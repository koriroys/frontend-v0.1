import React, { Component } from "react";
import BtnAdd from "../Tools/BtnAdd";
import "./ProjectsAttachedListBar.scss";

export default class ProjectsAttachedListBar extends Component {
  static get defaultProps() {
    return {
      actualList: [],
      itemId: 0,
      itemType: "",
      refreshList: () => console.log("props is missing"),
    };
  }

  render() {
    const { actualList, itemId, itemType, refreshList } = this.props;
    if (itemId > 0 && itemType !== "") {
      return (
        <div className="row justify-content-end projectsAttachedListBar">
          <div className="col-12">
            <BtnAdd
              type="project"
              actualList={actualList}
              itemType={itemType}
              itemId={itemId}
              refreshList={refreshList}
            />
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
}
