import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { Redirect } from "react-router-dom";
import { UserContext } from "UserProvider";
import Api from "Api";
import DocumentsManager from "../Tools/Documents/DocumentsManager";
import Feed from "../Feed/Feed";
import InfoHtmlComponent from "Components/Tools/Info/InfoHtmlComponent";
import Loading from "Components/Tools/Loading";
import Needs from "Components/Needs/Needs";
import ProjectHeader from "./ProjectHeader";
import { Helmet } from "react-helmet";
import Alert from "Components/Tools/Alert";
import $ from "jquery";
import "Components/Main/Similar.scss";
import { stickyTabNav, scrollToActiveTab } from "Components/Tools/utils/utils.js";

export default class ProjectDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      project: this.props.project,
      failedLoad: false,
      loading: true,
    };
  }

  componentWillMount() {
    if (this.props.project) {
      this.setState({ loading: false });
    } else {
      this.setState({ loading: true });
      Api.get("/api/projects/" + this.props.match.params.id)
        .then((res) => {
          // console.log("Project got : ", res.data);
          if (res.data) {
            this.setState({ project: res.data, loading: false });
          } else {
            this.setState({ failedLoad: true, loading: false });
          }
        })
        .catch((error) => {
          // console.log("ERR : ", error);
          this.setState({ failedLoad: true, loading: false });
        });
    }
  }

  componentWillUnmount() {
    window.removeEventListener("load", this.handleLoad);
  }

  // onLoad() {
  // 	if(urlSuccessParam === "1") { // if url success param is 1, show "saved changes" success alert
  // 		$(".alert-success").show() // display success message
  // 		setTimeout(function(){ $(".alert-success").hide(300) }, 2000); // hide it after 2sec
  // 	};
  // }

  componentDidMount() {
    // window.addEventListener('load', () => {
    // 	var url = new URL(window.location.href);
    // 	var urlSuccessParam = url.searchParams.get("success");
    // 	// var alertSuccess = document.querySelector(".alert-success")
    // 	// if(urlSuccessParam === "1") { // if url success param is 1, show "saved changes" success alert
    // 	// 	alertSuccess.classList.add("show")
    // 	// 	setTimeout(function(){ alertSuccess.classList.remove("show") }, 2000); // hide it after 2sec
    // 	// };
    // 	if(urlSuccessParam === "1") { // if url success param is 1, show "saved changes" success alert
    // 			$(".alert-success").show() // display success message
    // 			setTimeout(function(){ $(".alert-success").hide(300) }, 2000); // hide it after 2sec
    // 		};
    // 	stickyTabNav() // make the tab navigation bar sticky on top when we reach its scroll position
    // 	scrollToActiveTab() // if there is a hash in the url and the tab exists, click and scroll to the tab
    // });
    var url = new URL(window.location.href);
    var urlSuccessParam = url.searchParams.get("success");
    setTimeout(function () {
      stickyTabNav(); // make the tab navigation bar sticky on top when we reach its scroll position
      scrollToActiveTab(); // if there is a hash in the url and the tab exists, click and scroll to the tab
      if (urlSuccessParam === "1") {
        // if url success param is 1, show "saved changes" success alert
        $("#editSuccess").show(); // display success message
        setTimeout(function () {
          $("#editSuccess").hide(300);
        }, 2000); // hide it after 2sec
      }
    }, 1200); // had to add setTimeout to make it work
  }

  render() {
    const { project, failedLoad, loading } = this.state;
    var urlHash = new URLSearchParams(window.location.search);
    let userContext = this.context;
    if (failedLoad) {
      return <Redirect to="/projects" />;
    }
    // make different tab active depending if user is member or not
    var newsTabClasses, aboutTabClasses, newsPaneClasses, aboutPaneClasses;
    if (project) {
      if (project.is_member || urlHash.get("tab") === "feed") {
        newsTabClasses = "nav-item nav-link active";
        aboutTabClasses = "nav-item nav-link";
        newsPaneClasses = "tab-pane active";
        aboutPaneClasses = "tab-pane";
      } else {
        newsTabClasses = "nav-item nav-link";
        aboutTabClasses = "nav-item nav-link active";
        newsPaneClasses = "tab-pane";
        aboutPaneClasses = "tab-pane active";
      }
      // Dynamic SEO meta tags
      var SEO_title = project.title + " | JOGL";
      var SEO_desc = project.short_description;
      var SEO_image = project.banner_url;
    }

    return (
      <Loading active={loading}>
        <Helmet>
          <title>{SEO_title}</title>
          <meta name="title" content={SEO_title} data-react-helmet="true" />
          <meta name="description" content={SEO_desc} data-react-helmet="true" />
          <meta property="og:title" content={SEO_title} data-react-helmet="true" />
          <meta property="og:image" content={SEO_image} data-react-helmet="true" />
        </Helmet>
        {project && (
          <div className="projectDetails container-fluid">
            <ProjectHeader project={project} />
            <nav className="nav nav-tabs container-fluid">
              <a className={newsTabClasses} href="#news" data-toggle="tab">
                <FormattedMessage id="entity.tab.news" defaultMessage="News & Update" />
              </a>
              <a className={aboutTabClasses} href="#about" data-toggle="tab">
                <FormattedMessage id="general.about" defaultMessage="About the Project" />
              </a>
              <a className="nav-item nav-link" href="#needs" data-toggle="tab">
                <FormattedMessage id="entity.card.needs" defaultMessage="Needs" />
              </a>
              {((project.documents.length !== 0 && (!project.is_admin || !userContext.user)) || project.is_admin) && ( // hide documents tab is user is not admin and they are no docs
                <a className="nav-item nav-link" href="#documents" data-toggle="tab">
                  <FormattedMessage id="entity.tab.documents" defaultMessage="Documents" />
                </a>
              )}
            </nav>
            <div className="tabContainer">
              <div className="tab-content justify-content-center container-fluid">
                <div className={newsPaneClasses} id="news">
                  {project.feed_id && (
                    <Feed
                      feedId={project.feed_id}
                      // display post create component to everyone on public projects, and only to members on private projects (connected users only)
                      displayCreate={
                        this.context.connected ? (!project.is_private ? true : project.is_member ? true : false) : false
                      }
                      isAdmin={project.is_admin}
                    />
                  )}
                </div>
                <div className={aboutPaneClasses} id="about">
                  <InfoHtmlComponent title="" content={project.description} />
                  {project.desc_elevator_pitch && (
                    <h3>
                      <FormattedMessage id="entity.info.desc_elevator_pitch" />
                    </h3>
                  )}
                  <InfoHtmlComponent title="" content={project.desc_elevator_pitch} />
                  {project.desc_contributing && (
                    <h3>
                      <FormattedMessage id="entity.info.desc_contributing" />
                    </h3>
                  )}
                  <InfoHtmlComponent title="" content={project.desc_contributing} />
                  {project.desc_problem_statement && (
                    <h3>
                      <FormattedMessage id="entity.info.desc_problem_statement" />
                    </h3>
                  )}
                  <InfoHtmlComponent title="" content={project.desc_problem_statement} />
                  {project.desc_objectives && (
                    <h3>
                      <FormattedMessage id="entity.info.desc_objectives" />
                    </h3>
                  )}
                  <InfoHtmlComponent title="" content={project.desc_objectives} />
                  {project.desc_state_art && (
                    <h3>
                      <FormattedMessage id="entity.info.desc_state_art" />
                    </h3>
                  )}
                  <InfoHtmlComponent title="" content={project.desc_state_art} />
                  {project.desc_progress && (
                    <h3>
                      <FormattedMessage id="entity.info.desc_progress" />
                    </h3>
                  )}
                  <InfoHtmlComponent title="" content={project.desc_progress} />
                  {project.desc_stakeholder && (
                    <h3>
                      <FormattedMessage id="entity.info.desc_stakeholder" />
                    </h3>
                  )}
                  <InfoHtmlComponent title="" content={project.desc_stakeholder} />
                  {project.desc_impact_strat && (
                    <h3>
                      <FormattedMessage id="entity.info.desc_impact_strat" />
                    </h3>
                  )}
                  <InfoHtmlComponent title="" content={project.desc_impact_strat} />
                  {project.desc_ethical_statement && (
                    <h3>
                      <FormattedMessage id="entity.info.desc_ethical_statement" />
                    </h3>
                  )}
                  <InfoHtmlComponent title="" content={project.desc_ethical_statement} />
                  {project.desc_sustainability_scalability && (
                    <h3>
                      <FormattedMessage id="entity.info.desc_sustainability_scalability" />
                    </h3>
                  )}
                  <InfoHtmlComponent title="" content={project.desc_sustainability_scalability} />
                  {project.desc_communication_strat && (
                    <h3>
                      <FormattedMessage id="entity.info.desc_communication_strat" />
                    </h3>
                  )}
                  <InfoHtmlComponent title="" content={project.desc_communication_strat} />
                  {project.desc_funding && (
                    <h3>
                      <FormattedMessage id="entity.info.desc_funding" />
                    </h3>
                  )}
                  <InfoHtmlComponent title="" content={project.desc_funding} />
                </div>
                <div className="tab-pane" id="needs">
                  <Needs displayCreate={project.is_admin ? true : false} itemId={project.id} itemType="projects" />
                </div>
                <div className="tab-pane" id="documents">
                  <DocumentsManager item={project} itemId={project.id} itemType="projects" />
                </div>
              </div>
            </div>
            <Alert
              type="success"
              id="editSuccess"
              message={
                <FormattedMessage
                  id="general.editSuccessMsg"
                  defaultMessage="The changes have been saved successfully."
                />
              }
            />
          </div>
        )}
      </Loading>
    );
  }
}
ProjectDetails.contextType = UserContext;
