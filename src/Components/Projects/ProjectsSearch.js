import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { Link } from "react-router-dom";
import { UserContext } from "UserProvider";

export default class ProjectsSearch extends Component {
  render() {
    let userContext = this.context;
    return (
      <div className="row justify-content-around projectsSearch">
        {/* <div className="col-6">
          <button className="btn btn-sm btn-secondary btn-action">
            <FormattedMessage id="searchBar.mainBtn" defaultMessage="Search & Filter & Sort" />
					</button>
        </div> */}
        {/* <div className="col-6 text-right"> */}
        <div className="col-12 text-right">
          {userContext.connected && (
            <Link to="/project/create">
              <button className="btn btn-sm btn-primary btn-action">
                <FormattedMessage id="projects.searchBar.btnAddProject" defaultMessage="Add a project" />
              </button>
            </Link>
          )}
        </div>
      </div>
    );
  }
}
ProjectsSearch.contextType = UserContext;
