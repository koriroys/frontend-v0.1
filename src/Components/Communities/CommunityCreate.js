import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { Redirect } from "react-router-dom";
import CommunityForm from "./CommunityForm";
import Api from "Api";

export default class CommunityCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCreated: null,
      newCommunity: {
        creator_id: Number(localStorage.getItem("userId")),
        interests: [],
        short_description: "",
        short_title: "",
        public: true,
        skills: [],
        ressources: [],
        title: "",
        is_private: true,
      },
      sending: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(key, content) {
    var updateCommunity = this.state.newCommunity;
    updateCommunity[key] = content;
    this.setState({ newCommunity: updateCommunity });
  }

  handleSubmit() {
    const newCommunity = this.state.newCommunity;
    this.setState({ sending: true });
    Api.post("/api/communities/", { community: newCommunity })
      .then((res) => {
        this.setState({ isCreated: res.data.id, sending: false });
      })
      .catch((error) => {
        // console.log(error);
        this.setState({ sending: false });
      });
  }

  render() {
    var { isCreated, newCommunity, sending } = this.state;

    if (isCreated) {
      return <Redirect push to={"/community/" + isCreated + "/edit"} />;
    } else {
      return (
        <div className="communityCreate container-fluid">
          <h1>
            <FormattedMessage id="community.create.title" defaultMessage="Create a new community" />
          </h1>
          <CommunityForm
            community={newCommunity}
            handleChange={this.handleChange}
            handleSubmit={this.handleSubmit}
            mode="create"
            sending={sending}
          />
        </div>
      );
    }
  }
}
