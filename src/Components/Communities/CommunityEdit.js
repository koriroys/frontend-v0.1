import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { Redirect } from "react-router-dom";
import Alert from "Components/Tools/Alert";
import Api from "Api";
import Loading from "Components/Tools/Loading";
import CommunityForm from "./CommunityForm";
import MembersList from "../Members/MembersList";
import $ from "jquery";
import { UserContext } from "UserProvider";
import { stickyTabNav, scrollToActiveTab } from "Components/Tools/utils/utils.js";

export default class CommunityEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      community: this.props.community,
      communityUpdated: false,
      failedLoad: false,
      loading: true,
      sending: false,
      objectDelete: false,
      errors: "",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    if (this.props.community) {
      this.setState({ loading: false });
    } else {
      const idCommunity = this.props.match.params.id;
      Api.get("/api/communities/" + idCommunity)
        .then((res) => {
          // console.log("Community got : ", res.data);
          this.setState({ community: res.data, loading: false });
        })
        .catch((error) => {
          // console.log(error);
          this.setState({ failedLoad: true, loading: false });
        });
    }
  }

  componentWillReceiveProps(nextProps) {
    const community = nextProps.community;
    this.setState({ community });
  }

  handleChange(key, content) {
    // console.log("Edit handleChange : " + key + " " + content);
    var newCommunity = this.state.community;
    newCommunity[key] = content;
    // console.log(newCommunity);
    this.setState({ community: newCommunity });
  }

  handleSubmit() {
    const community = this.state.community;
    this.setState({ sending: true });
    Api.patch("/api/communities/" + this.state.community.id, { community })
      .then((res) => {
        // console.log(res);
        this.setState({ communityUpdated: true, sending: false });
      })
      .catch((error) => {
        // console.log(error);
        this.setState({ sending: false });
      });
  }

  deleteGroup() {
    var communityid = this.state.community.id;
    Api.delete("api/communities/" + communityid + "/")
      .then((res) => {
        // console.log("community deleted", res);
        $("#closeDeleteModal").click();
        this.setState({ objectDelete: true });
        // console.log(res.data);
      })
      .catch((error) => {
        // console.log(error);
        this.setState({ errors: error.toString() });
      });
  }

  renderDeleteModal(delBtnTitle, delBtnText) {
    const { errors } = this.state;
    var errorMessage = errors.includes("err-") ? (
      <FormattedMessage id={errors} defaultMessage="An error has occured" />
    ) : (
      errors
    );
    return (
      // <ModalDeleteObject errors={errors}/>
      <div
        className="modal fade"
        id="deleteObjectModal"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="deleteObjectModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="deleteObjectModalLabel">
                <FormattedMessage id={delBtnTitle} defaultMessage="Delete group" />
              </h5>
              <button type="button" id="closeDeleteModal" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              {errors !== "" && <Alert type="danger" message={errorMessage} />}
              <p className="deleteModalMessage">
                <FormattedMessage id={delBtnText} />
              </p>
              <div className="btnGroup">
                <button type="button" className="btn btn-outline-danger" onClick={this.deleteGroup.bind(this)}>
                  {/* <button type="button" className="btn btn-outline-danger" onClick={this.deleteGroup}> */}
                  <FormattedMessage id="general.yes" defaultMessage="Yes" />
                </button>
                <button type="button" className="btn btn-success" data-dismiss="modal">
                  <FormattedMessage id="general.no" defaultMessage="No" />
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    const { community, communityUpdated, failedLoad, loading, sending } = this.state;
    let userContext = this.context;
    if (community || (!community && failedLoad)) {
      // check if group exist, or if it doesn't
      var urlBack = `/community/${community.id}`;
      if (failedLoad || !userContext.user || !community.is_admin || this.state.objectDelete) {
        // redirect to groups page if not connected, not admin, if group doesn't exist, or was just deleted
        return <Redirect to="/communities" />;
      }
      setTimeout(function () {
        stickyTabNav("isEdit");
        scrollToActiveTab(); // if there is a hash in the url and the tab exists, click and scroll to the tab
      }, 700); // had to add setTimeout for the function to work
    }
    if (communityUpdated) {
      return <Redirect push to={urlBack + "?success=1"} />;
    }
    if (community) {
      var delBtnTitle = community.members_count > 1 ? "community.archive.title" : "community.delete.title";
      var delBtnText = community.members_count > 1 ? "community.archive.text" : "community.delete.text";
    }

    return (
      <Loading active={loading}>
        <div className="communityEdit container-fluid">
          <h1>
            <FormattedMessage id="community.edit.title" defaultMessage="Edit my Community" />
          </h1>
          <a href={urlBack}>
            {" "}
            {/* go back link*/}
            <i className="fa fa-arrow-left" />
            <FormattedMessage id="community.edit.back" defaultMessage="Go back" />
          </a>
          <nav className="nav nav-tabs container-fluid">
            <a className="nav-item nav-link active" href="#basic_info" data-toggle="tab">
              <FormattedMessage id="entity.tab.basic_info" defaultMessage="Information" />
            </a>
            <a className="nav-item nav-link" href="#members" data-toggle="tab">
              <FormattedMessage id="general.members" defaultMessage="Members" />
            </a>
            <a className="nav-item nav-link" href="#advanced" data-toggle="tab">
              <FormattedMessage id="entity.tab.advanced" defaultMessage="Advanced" />
            </a>
          </nav>
          <div className="tabContainer">
            <div className="tab-content justify-content-center container-fluid">
              <div className="tab-pane active" id="basic_info">
                <CommunityForm
                  community={community}
                  mode="edit"
                  sending={sending}
                  handleChange={this.handleChange}
                  handleSubmit={this.handleSubmit}
                />
              </div>
              <div className="tab-pane" id="members">
                <MembersList itemType="communities" itemId={this.props.match.params.id} />
              </div>
              <div className="tab-pane" id="advanced">
                <div className="deleteBtns">
                  {community && community.members_count > 1 && (
                    <p>
                      <FormattedMessage id="community.delete.explain" />
                    </p>
                  )}
                  <button type="button" className="btn btn-danger" data-toggle="modal" data-target="#deleteObjectModal">
                    <FormattedMessage id={delBtnTitle} />
                  </button>
                  {community && community.members_count > 1 && (
                    <button type="button" className="btn btn-danger" disabled>
                      <FormattedMessage id="community.delete.title" />
                    </button>
                  )}
                  {this.renderDeleteModal(delBtnTitle, delBtnText)}
                </div>
              </div>
            </div>
          </div>
        </div>
      </Loading>
    );
  }
}
CommunityEdit.contextType = UserContext;
