import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import Api from "Api";
import Loading from "Components/Tools/Loading";
import ListComponent from "Components/Tools/ListComponent";
import MembersListBar from "Components/Members/MembersListBar";
import "./MembersList.scss";

export default class MembersList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listMembers: [],
      failedLoad: false,
      loading: true,
    };
    this.getMembers = this.getMembers.bind(this);
  }

  static get defaultProps() {
    return {
      actionBar: true,
      itemId: 0,
      itemType: "",
    };
  }

  // componentWillReceiveProps(nextProps){
  // 	const itemId = nextProps.itemId;
  //   this.setState({itemId});
  //   if (this.state.failedLoad){
  //     this.getMembers();
  //   }
  // }

  componentWillMount() {
    this.getMembers();
  }

  getMembers() {
    const { itemType, itemId } = this.props;
    if (itemId) {
      Api.get("/api/" + itemType + "/" + itemId + "/members")
        .then((res) => {
          // filter members by type
          var owners = res.data.users.filter((member) => member.owner);
          var admins = res.data.users.filter((member) => member.admin && !member.owner);
          var members = res.data.users.filter((member) => !member.owner && !member.admin && member.member);
          var pending = res.data.users.filter((member) => !member.owner && !member.admin && !member.member);
          // then group/order them, displaying owners first, admins second, and members last
          var listMembers = [...owners, ...admins, ...members, ...pending];
          this.setState({ listMembers, loading: false });
        })
        .catch((error) => {
          // console.log("ERR : ", error);
          this.setState({ failedLoad: true, loading: false });
        });
    } else {
      this.setState({ failedLoad: true, loading: false });
    }
  }

  render() {
    const { loading, listMembers } = this.state;
    const { actionBar, itemType, itemId } = this.props;
    return (
      <Loading active={loading} height="150px">
        {listMembers ? (
          <div className="membersList">
            <h3 className="title">
              <FormattedMessage id="general.members" defaultMessage="Members" />
            </h3>
            <div className="col-12">
              {actionBar && <MembersListBar itemType={itemType} itemId={itemId} refreshList={this.getMembers} />}
              <ListComponent
                data={listMembers}
                itemParentId={itemId}
                itemParentType={itemType}
                itemType="member"
                callBack={this.getMembers}
                colMax={1}
              />
            </div>
          </div>
        ) : (
          <div className="errorMessage text-center">Unable to load component</div>
        )}
      </Loading>
    );
  }
}
