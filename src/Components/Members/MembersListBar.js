import React, { Component } from "react";
import BtnAdd from "../Tools/BtnAdd";
import "./MembersListBar.scss";

export default class MembersListBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listPeople: [],
    };
  }

  render() {
    const { itemId, itemType } = this.props;
    if (itemId && itemType) {
      return (
        <div className="row justify-content-end membersListBar">
          <div className="col-12">
            <BtnAdd type="member" itemType={itemType} itemId={itemId} />
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
}
