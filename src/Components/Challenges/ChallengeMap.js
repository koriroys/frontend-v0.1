import React, { Component } from "react";
import square from "assets/img/square.png";
import "./ChallengeMap.scss";

export default class ChallengeMap extends Component {
  render() {
    return (
      <div className="row challengeMap">
        <div className="col-12">
          <div className="map">
            <img src={square} alt="mapZone" />
            <p className="devInfo">MAP ZONE</p>
          </div>
        </div>
      </div>
    );
  }
}
