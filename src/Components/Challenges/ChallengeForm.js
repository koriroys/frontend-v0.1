import React, { Component, Fragment } from "react";
import { FormattedMessage, injectIntl } from "react-intl";
import { Link } from "react-router-dom";
/*** Form objects ***/
import FormChipsComponent from "Components/Tools/Forms/FormChipsComponent";
import FormDefaultComponent from "Components/Tools/Forms/FormDefaultComponent";
import FormDropdownComponent from "Components/Tools/Forms/FormDropdownComponent";
import FormImgComponent from "Components/Tools/Forms/FormImgComponent";
import FormInterestsComponent from "Components/Tools/Forms/FormInterestsComponent";
import FormSkillsComponent from "Components/Tools/Forms/FormSkillsComponent";
import FormTextAreaComponent from "Components/Tools/Forms/FormTextAreaComponent";
import FormWysiwygComponent from "Components/Tools/Forms/FormWysiwygComponent";
/*** Validators ***/
import FormValidator from "Components/Tools/Forms/FormValidator";
import challengeFormRules from "./challengeFormRules";
/*** Images/Style ***/
import DefaultImg from "assets/img/default/default-challenge.jpg";

class ChallengeForm extends Component {
  validator = new FormValidator(challengeFormRules);

  static get defaultProps() {
    return {
      mode: "create",
      challenge: {
        banner_url: "",
        description: "",
        faq: "",
        followers: 0,
        interests: [],
        public: true,
        rules: "",
        short_title: "",
        short_description: "",
        short_description_fr: "",
        skills: [],
        status: "draft",
        short_name: "",
        title: "",
        title_fr: "",
      },
      sending: false,
    };
  }

  handleChange(key, content) {
    // console.log(content);

    /* Validators start */
    const state = {};
    state[key] = content;
    const validation = this.validator.validate(state);
    if (validation[key] !== undefined) {
      const stateValidation = {};
      stateValidation["valid_" + key] = validation[key];
      this.setState(stateValidation);
    }
    /* Validators end */
    this.props.handleChange(key, content);
  }

  handleSubmit(event) {
    event.preventDefault();
    /* Validators control before submit */
    const validation = this.validator.validate(this.props.challenge);
    if (validation.isValid) {
      this.props.handleSubmit();
    } else {
      const stateValidation = {};
      var firsterror = true;
      Object.keys(validation).forEach((key) => {
        if (key !== "isValid") {
          // console.log(key);
          if (validation[key].isInvalid && firsterror) {
            // if field is invalid and it's the first field that has error
            var element = document.querySelector(`#${key}`); // get element that is not valid
            const y = element.getBoundingClientRect().top + window.pageYOffset - 130; // calculate it's top value and remove 25 of offset
            window.scrollTo({ top: y, behavior: "smooth" }); // scroll to element to show error
            firsterror = false; // set to false so that it won't scroll to second invalid field and further
          }
          stateValidation["valid_" + key] = validation[key];
        }
      });
      this.setState(stateValidation);
    }
  }

  renderBtnsForm() {
    const { challenge, mode, sending } = this.props;
    var urlBack = "/challenges";
    var textAction = "Create";
    if (mode === "edit") {
      urlBack = "/challenge/" + challenge.short_title;
      textAction = "Update";
    }

    return (
      <div className="row challengeFormBtns">
        <Link to={urlBack}>
          <button type="button" className="btn btn-outline-primary">
            <FormattedMessage id="entity.form.btnCancel" defaultMessage="Cancel" />
          </button>
        </Link>
        <button type="submit" className="btn btn-primary" disabled={sending} style={{ marginRight: "10px" }}>
          {sending && (
            <Fragment>
              <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true"></span>
              &nbsp;
            </Fragment>
          )}
          <FormattedMessage id={"entity.form.btn" + textAction} defaultMessage={textAction} />
        </button>
      </div>
    );
  }

  render() {
    const {
      valid_title,
      valid_title_fr,
      valid_short_title,
      valid_short_description,
      valid_short_description_fr,
      valid_interests,
      valid_skills,
    } = this.state ? this.state : "";
    const { challenge, mode, intl } = this.props;

    return (
      <form onSubmit={this.handleSubmit.bind(this)} className="challengeForm">
        <FormDefaultComponent
          content={challenge.title}
          errorCodeMessage={valid_title ? valid_title.message : ""}
          id="title"
          isValid={valid_title ? !valid_title.isInvalid : undefined}
          mandatory={true}
          onChange={this.handleChange.bind(this)}
          placeholder={intl.formatMessage({ id: "challenge.form.title.placeholder" })}
          title={intl.formatMessage({ id: "entity.info.title" })}
        />
        <FormDefaultComponent
          content={challenge.title_fr}
          errorCodeMessage={valid_title_fr ? valid_title_fr.message : ""}
          id="title_fr"
          isValid={valid_title_fr ? !valid_title_fr.isInvalid : undefined}
          mandatory={true}
          onChange={this.handleChange.bind(this)}
          placeholder={intl.formatMessage({ id: "challenge.form.title.placeholder" })}
          title={intl.formatMessage({ id: "entity.info.title_fr" })}
        />
        <FormDefaultComponent
          content={challenge.short_title}
          errorCodeMessage={valid_short_title ? valid_short_title.message : ""}
          id="short_title"
          isValid={valid_short_title ? !valid_short_title.isInvalid : undefined}
          onChange={this.handleChange.bind(this)}
          mandatory={true}
          pattern={/[A-Za-z0-9]/g}
          placeholder={intl.formatMessage({ id: "challenge.form.short_title.placeholder" })}
          prepend="#"
          title={intl.formatMessage({ id: "entity.info.short_name" })}
        />
        <FormTextAreaComponent
          content={challenge.short_description}
          errorCodeMessage={valid_short_description ? valid_short_description.message : ""}
          id="short_description"
          isValid={valid_short_description ? !valid_short_description.isInvalid : undefined}
          mandatory={true}
          maxChar={500}
          onChange={this.handleChange.bind(this)}
          row={3}
          placeholder={intl.formatMessage({ id: "challenge.form.short_description.placeholder" })}
          title={intl.formatMessage({ id: "entity.info.short_description" })}
        />
        <FormTextAreaComponent
          content={challenge.short_description_fr}
          errorCodeMessage={valid_short_description_fr ? valid_short_description_fr.message : ""}
          id="short_description_fr"
          isValid={valid_short_description_fr ? !valid_short_description_fr.isInvalid : undefined}
          mandatory={true}
          maxChar={500}
          onChange={this.handleChange.bind(this)}
          row={3}
          placeholder={intl.formatMessage({ id: "challenge.form.short_description.placeholder" })}
          title={intl.formatMessage({ id: "entity.info.short_description_fr" })}
        />
        {mode === "edit" && (
          <Fragment>
            <FormDropdownComponent
              id="status"
              warningMsgId="challenge.info.dropDownMsg"
              title={intl.formatMessage({ id: "entity.info.status" })}
              content={challenge.status}
              list={["draft", "soon", "active", "completed"]}
              onChange={this.handleChange.bind(this)}
            />
            <FormDefaultComponent
              id="launch_date"
              content={challenge.launch_date ? challenge.launch_date.split("T")[0] : null}
              onChange={this.handleChange.bind(this)}
              type="date"
              // placeholder={intl.formatMessage({id:'challenge.form.launch_date.placeholder'})}
              title={intl.formatMessage({ id: "entity.info.launch_date" })}
            />
            <FormDefaultComponent
              id="final_date"
              content={challenge.final_date ? challenge.final_date.split("T")[0] : null}
              onChange={this.handleChange.bind(this)}
              type="date"
              // placeholder={intl.formatMessage({id:'challenge.form.final_date.placeholder'})}
              title={intl.formatMessage({ id: "entity.info.final_date" })}
            />
            <FormDefaultComponent
              id="end_date"
              content={challenge.end_date ? challenge.end_date.split("T")[0] : null}
              onChange={this.handleChange.bind(this)}
              type="date"
              // placeholder={intl.formatMessage({id:'challenge.form.end_date.placeholder'})}
              title={intl.formatMessage({ id: "entity.info.end_date" })}
            />
            <FormWysiwygComponent
              id="description"
              placeholder={intl.formatMessage({ id: "challenge.form.description.placeholder" })}
              title={intl.formatMessage({ id: "entity.info.description" })}
              content={challenge.description}
              onChange={this.handleChange.bind(this)}
            />
            <FormWysiwygComponent
              id="description_fr"
              placeholder={intl.formatMessage({ id: "challenge.form.description.placeholder" })}
              title={intl.formatMessage({ id: "entity.info.description_fr" })}
              content={challenge.description_fr}
              onChange={this.handleChange.bind(this)}
            />
            <FormWysiwygComponent
              id="rules"
              placeholder={intl.formatMessage({ id: "challenge.form.rules.placeholder" })}
              title={intl.formatMessage({ id: "entity.info.rules" })}
              content={challenge.rules}
              onChange={this.handleChange.bind(this)}
            />
            <FormWysiwygComponent
              id="rules_fr"
              placeholder={intl.formatMessage({ id: "challenge.form.rules.placeholder" })}
              title={intl.formatMessage({ id: "entity.info.rules_fr" })}
              content={challenge.rules_fr}
              onChange={this.handleChange.bind(this)}
            />
            <FormWysiwygComponent
              id="faq"
              placeholder={intl.formatMessage({ id: "challenge.form.faq.placeholder" })}
              title={intl.formatMessage({ id: "entity.info.faq" })}
              content={challenge.faq}
              onChange={this.handleChange.bind(this)}
            />
            <FormWysiwygComponent
              id="faq_fr"
              placeholder={intl.formatMessage({ id: "challenge.form.faq.placeholder" })}
              title={intl.formatMessage({ id: "entity.info.faq_fr" })}
              content={challenge.faq_fr}
              onChange={this.handleChange.bind(this)}
            />
            <FormImgComponent
              type="banner"
              id="banner_url"
              imageUrl={challenge.banner_url}
              itemId={challenge.id}
              itemType="challenges"
              title={intl.formatMessage({ id: "challenge.info.banner_url" })}
              content={challenge.banner_url}
              defaultImg={DefaultImg}
              onChange={this.handleChange.bind(this)}
            />
            <FormChipsComponent
              id="hashtags"
              placeholder={intl.formatMessage({ id: "general.hashtags.placeholder" })}
              title={intl.formatMessage({ id: "entity.info.hashtags" })}
              content={challenge.hashtags}
              onChange={this.handleChange.bind(this)}
              maxChips={5}
              prepend="#"
            />
          </Fragment>
        )}
        <FormInterestsComponent
          content={challenge.interests}
          errorCodeMessage={valid_interests ? valid_interests.message : ""}
          id="interests"
          isValid={valid_interests ? !valid_interests.isInvalid : undefined}
          mandatory={true}
          onChange={this.handleChange.bind(this)}
          title={intl.formatMessage({ id: "entity.info.interests" })}
        />
        <FormSkillsComponent
          content={challenge.skills}
          errorCodeMessage={valid_skills ? valid_skills.message : ""}
          id="skills"
          type="challenge"
          isValid={valid_skills ? !valid_skills.isInvalid : undefined}
          mandatory={true}
          onChange={this.handleChange.bind(this)}
          placeholder={intl.formatMessage({ id: "general.skills.placeholder" })}
          title={intl.formatMessage({ id: "entity.info.skills" })}
        />
        {mode === "edit" && (
          <Fragment>
            <FormDefaultComponent
              id="address"
              placeholder={intl.formatMessage({ id: "general.address.placeholder" })}
              title={intl.formatMessage({ id: "general.address" })}
              content={challenge.address}
              onChange={this.handleChange.bind(this)}
            />
            <FormDefaultComponent
              id="city"
              placeholder={intl.formatMessage({ id: "general.city.placeholder" })}
              title={intl.formatMessage({ id: "general.city" })}
              content={challenge.city}
              onChange={this.handleChange.bind(this)}
            />
            <FormDefaultComponent
              id="country"
              placeholder={intl.formatMessage({ id: "general.country.placeholder" })}
              title={intl.formatMessage({ id: "general.country" })}
              content={challenge.country}
              onChange={this.handleChange.bind(this)}
            />
          </Fragment>
        )}
        {this.renderBtnsForm()}
      </form>
    );
  }
}
export default injectIntl(ChallengeForm);
