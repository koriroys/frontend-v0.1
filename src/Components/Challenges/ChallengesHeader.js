import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import "../Main/Similar.scss";

export default class ChallengesHeader extends Component {
  render() {
    return (
      <div className="challengesHeader">
        <h1>
          <FormattedMessage id="challenges.header.title" defaultMessage="Explore challenges" />
        </h1>
        <p>
          <FormattedMessage
            id="challenges.header.description"
            defaultMessage="Create, join teams and contribute to cracking challenges."
          />
        </p>
        <hr />
      </div>
    );
  }
}
