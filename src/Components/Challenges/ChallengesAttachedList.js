import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import Api from "Api";
import ListComponent from "Components/Tools/ListComponent";
import Loading from "Components/Tools/Loading";
import ChallengesAttachedListBar from "./ChallengesAttachedListBar";
import "./ChallengesAttachedList.scss";

export default class ChallengesAttachedList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listChallenges: [],
      failedLoad: false,
      loading: true,
    };
    this.refreshList = this.refreshList.bind(this);
  }

  static get defaultProps() {
    return {
      actionBar: true,
      itemType: "",
    };
  }

  componentDidMount() {
    this.getChallenges();
  }

  getChallenges() {
    if (this.props.itemId) {
      Api.get("/api/" + this.props.itemType + "/" + this.props.itemId + "/challenges")
        .then((res) => {
          // console.log("List challenges receive : ", res.data.challenges);
          this.setState({
            listChallenges: res.data.challenges,
            failedLoad: false,
            loading: false,
            gotChallenges: true,
          });
        })
        .catch((error) => {
          // console.log("ERR : ", error);
          this.setState({ failedLoad: true, loading: false });
        });
    } else {
      this.setState({ failedLoad: true, loading: false });
    }
  }

  refreshList() {
    this.getChallenges();
  }

  render() {
    const { loading, listChallenges, gotChallenges } = this.state;
    if (!gotChallenges) {
      return null;
    }
    return (
      <Loading active={loading} height="150px">
        {listChallenges ? (
          <div className="challengesAttachedList">
            <h3 className="title">
              <FormattedMessage id="challenges" defaultMessage="Challenges" />
            </h3>
            <div className="col-12">
              <ChallengesAttachedListBar
                actualList={listChallenges}
                itemType={this.props.itemType}
                itemId={this.state.itemId}
                refreshList={this.refreshList}
              />
              <ListComponent
                itemType="challengeAttached"
                data={listChallenges}
                callBack={this.refreshList}
                colMax={1}
              />
            </div>
          </div>
        ) : (
          <div className="errorMessage text-center">Unable to load component</div>
        )}
      </Loading>
    );
  }
}
