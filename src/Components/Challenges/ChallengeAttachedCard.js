import React, { Component, Fragment } from "react";
import { FormattedMessage } from "react-intl";
import Api from "Api";
import Alert from "Components/Tools/Alert";
import { Link } from "react-router-dom";
import defaultImg from "assets/img/default/default-challenge.jpg";
import "./ChallengeAttachedCard.scss";

export default class ChallengeAttachedCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sending: "",
      error: "",
      itemType: "challenges",
      challenge: this.props.challenge,
    };
  }

  componentWillReceiveProps(nextProps) {
    const { challenge } = nextProps;
    this.setState({ challenge });
    // console.log("CHALLENGE ATTACHED", challenge)
  }

  static get defaultProps() {
    return {
      challenge: undefined,
    };
  }

  removeChallenge() {
    const { itemType, challenge } = this.state;
    this.setState({ sending: "remove", error: "" });

    Api.delete("/api/" + itemType + "/" + challenge.challenge_id + "/challenges/" + challenge.id)
      .then((res) => {
        // console.log(res);
        this.setState({ sending: "" });
        this.props.callBack();
      })
      .catch((error) => {
        console.log(error);
        this.setState({ sending: "", error: <FormattedMessage id="err-" defaultMessage="An error has occured" /> });
      });
  }

  render() {
    const { error, challenge, sending } = this.state;

    if (challenge !== undefined) {
      var imgTodisplay = defaultImg;
      if (challenge.banner_url_sm !== "" && challenge.banner_url_sm !== null && challenge.banner_url_sm !== undefined) {
        imgTodisplay = challenge.banner_url_sm;
      }

      const bgBanner = {
        backgroundImage: "url(" + imgTodisplay + ")",
        backgroundSize: "cover",
        backgroundPosition: "center",
      };

      return (
        <div>
          <div className="row challengeCard" key={challenge.id}>
            <div className="col-3 col-sm-2 col-lg-1 zoneLogo">
              <Link to={"/challenge/" + challenge.short_title}>
                <div className="logoChallenge" style={bgBanner}></div>
              </Link>
            </div>
            <div className="col-9 col-sm-6 zoneInfo">
              <Link to={"/challenge/" + challenge.short_title}>
                {challenge.title}
                <br />
              </Link>
              <span className="role">
                <FormattedMessage id="attach.status" defaultMessage="Status : " />
                <FormattedMessage id={`entity.info.status.${challenge.status}`} defaultMessage="Active" />
              </span>
            </div>
            <div className="col">
              <FormattedMessage id="attach.members" defaultMessage="Members : " />
              {challenge.members_count}
            </div>
            <div className="col col-right">
              <button
                type="button"
                className="delete"
                disabled={sending === "remove" ? true : false}
                onClick={() => this.removeChallenge()}
              >
                {sending === "remove" ? (
                  <Fragment>
                    <span
                      className="spinner-border spinner-border-sm text-center"
                      role="status"
                      aria-hidden="true"
                    ></span>
                    &nbsp;
                  </Fragment>
                ) : (
                  "X "
                )}
                <FormattedMessage id="attach.remove" defaultMessage="remove" />
              </button>
              {error !== "" && <Alert type="danger" message={error} />}
            </div>
          </div>
          <hr />
        </div>
      );
    } else {
      return null;
    }
  }
}
