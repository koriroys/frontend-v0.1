import React, { Component } from "react";
import Api from "Api";
import { FormattedMessage } from "react-intl";

export default class NeedDelete extends Component {
  static get defaultProps() {
    return {
      need: undefined,
      refresh: () => console.log("Missing function"),
    };
  }

  deleteNeed() {
    const { need, refresh } = this.props;
    if (need === undefined) {
      // console.log("undefined need");
    } else {
      Api.delete("api/needs/" + need.id)
        .then((res) => {
          // console.log(res);
          refresh();
        })
        .catch((error) => {
          // console.log(error);
        });
    }
  }

  render() {
    return (
      <div onClick={this.deleteNeed.bind(this)}>
        <i className="fa fa-trash postDelete" /> <FormattedMessage id="feed.object.delete" defaultMessage="Delete" />
      </div>
    );
  }
}
