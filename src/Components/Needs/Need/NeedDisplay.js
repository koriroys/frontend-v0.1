import React, { Component } from "react";
import NeedCard from "./NeedCard";
import NeedUpdate from "./NeedUpdate";

export default class NeedDisplay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //showComments: false,
      mode: this.props.mode,
    };
  }

  static get defaultProps() {
    return {
      need: undefined,
      mode: "card",
      refresh: () => console.log("Missing function"),
    };
  }

  changeMode(newMode) {
    // console.log("newMode : " + newMode);
    if (newMode) {
      this.setState({ mode: newMode });
      this.props.refresh();
    }
  }

  render() {
    const { need, refresh } = this.props;
    const { mode } = this.state;
    if (need === undefined) {
      return "no need";
    } else if (mode === "details") {
      return <NeedCard changeMode={this.changeMode.bind(this)} need={need} mode="details" refresh={refresh} />;
    } else if (mode === "update") {
      return <NeedUpdate changeMode={this.changeMode.bind(this)} need={need} cancelMode={mode} refresh={refresh} />;
    } else {
      return <NeedCard changeMode={this.changeMode.bind(this)} need={need} mode="card" refresh={refresh} />;
    }
  }
}
