import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import Api from "Api";
import BtnUploadFile from "Components/Tools/BtnUploadFile";
import mimetype2fa from "Components/Tools/Documents/mimetypes.js";
// import DocumentsList from "Components/Tools/Documents/DocumentsList.js";
import "./NeedDocsManagement.scss";

export default class NeedDocsManagement extends Component {
  static get defaultProps() {
    return {
      need: undefined,
      uploadNow: false,
    };
  }

  attachDocuments(documents) {
    // console.log("Documents reçus : ", documents);
    // console.log(documents);
    var arrayDocuments = this.props.need.documents;
    for (var i = 0; i < documents.length; i++) {
      arrayDocuments.push(documents[i]);
    }
    this.props.handleChange(arrayDocuments);
    // console.log(documents, arrayDocuments);
  }

  deleteDocument(document) {
    const { need, mode, handleChange } = this.props;
    if (mode === "create") {
      var documents = need.documents;
      documents.forEach((documentToInspect, index) => {
        if (documentToInspect === document) {
          documents.splice(index, 1);
        }
      });
      handleChange(documents);
    } else if (mode === "update") {
      Api.delete("api/needs/" + need.id + "/documents/" + document.id)
        .then((res) => {
          // console.log(res);
          var documents = need.documents;
          documents.forEach((documentToInspect, index) => {
            if (documentToInspect === document) {
              documents.splice(index, 1);
            }
          });
          handleChange(documents);
        })
        .catch((error) => {
          // console.log(error);
        });
    }
  }

  renderPreviewDocuments(documents) {
    if (documents.length !== 0) {
      return (
        // <DocumentsList documents={documents} cardtype="feed" />
        <div className="preview">
          <div className="listDocuments">
            {documents.map((document, index) => {
              return (
                <div className="documentCardFeed" key={index}>
                  <div className="card-body">
                    <i className={"fa fa-" + mimetype2fa(document.content_type)}></i>
                    {document.content_type === "image/jpeg" ? (
                      // if document is a jpg image, make it hoverable with the image appearing in a popup
                      <div className="hover_img">
                        <a href={document.url} target="_blank" rel="noopener noreferrer">
                          {document.filename}
                          <span>
                            <img src={document.url} alt={document.filename} width="280" />
                          </span>
                        </a>
                      </div>
                    ) : (
                      // else just display the document
                      <a href={document.url} target="_blank" rel="noopener noreferrer">
                        {document.filename}
                      </a>
                    )}
                    {this.props.mode === "update" && this.props.need.is_owner && (
                      <button
                        type="button"
                        className="close justify-content-right"
                        aria-label="Close"
                        onClick={() => this.deleteDocument(document)}
                      >
                        <span aria-hidden="true">&times;</span>
                      </button>
                    )}
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      );
    } else {
      return null;
    }
  }

  render() {
    const { need, handleChange, mode } = this.props;
    if (need === undefined) {
      return null;
    } else {
      return (
        <div className="needDocsManagement">
          <h6>
            <FormattedMessage id="need.docs.title" defaultMessage="Attached documents:" />
          </h6>
          {need.documents.length > 0 ? (
            this.renderPreviewDocuments(need.documents)
          ) : (
            <div className="noDoc">
              <FormattedMessage id="need.docs.noDoc" defaultMessage="No documents" />
            </div>
          )}
          {handleChange && (
            <BtnUploadFile
              itemId={need.id}
              itemType="needs"
              setListFiles={this.attachDocuments.bind(this)}
              type="documents"
              uploadNow={mode === "update" ? true : false}
            />
          )}
        </div>
      );
    }
  }
}
