import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { UserContext } from "UserProvider";
import Api from "Api";
import NeedDocsManagement from "./NeedDocsManagement";
import NeedForm from "./NeedForm";
import "../Needs.scss";

export default class NeedCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      description: this.props.description,
      error: "",
      isCreating: false,
      need: undefined,
      uploading: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  static get defaultProps() {
    return {
      project_id: undefined,
      refresh: () => console.log("Missing function"),
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.need) {
      this.setState({ need: nextProps.need });
    }
  }

  componentDidMount() {
    this.setState({
      need: {
        content: "",
        creator: this.context.user,
        documents: [],
        // end_date: "",
        skills: [],
        ressources: [],
        title: "",
        users: [],
      },
    });
  }

  handleChange(key, content) {
    var updateNeed = this.state.need;
    updateNeed[key] = content;
    this.setState({ need: updateNeed, error: "" });
  }

  handleChangeDoc(documents) {
    this.handleChange("documents", documents);
  }

  handleSubmit() {
    const { project_id } = this.props;
    const { need } = this.state;
    if (!project_id) {
      // console.log("project_id is missing");
    } else {
      this.setState({ uploading: true });
      need["project_id"] = project_id;
      Api.post("/api/needs", { need: need })
        .then((res) => {
          // console.log(res);
          const itemId = res.data.id;
          if (need.documents.length > 0) {
            // console.log(res.data)
            // console.log("NEEDID", itemId);
            const itemType = "needs";
            const type = "documents";
            if (itemId) {
              var bodyFormData = new FormData();
              Array.from(need.documents).forEach((file) => {
                // console.log("file : ", file);
                bodyFormData.append(type + "[]", file);
              });
              var config = {
                headers: { "Content-Type": "multipart/form-data" },
              };

              Api.post("/api/" + itemType + "/" + itemId + "/documents", bodyFormData, config)
                .then((res) => {
                  // console.log(res);
                  // console.log(res.data);
                  if (res.status === 200) {
                    // console.log("Documents uploaded !");
                    this.refresh();
                  } else {
                    // console.log("An error has occured");
                    this.setState({
                      uploading: false,
                      error: <FormattedMessage id="err-" defaultMessage="An error has occured" />,
                    });
                  }
                })
                .catch((error) => {
                  // console.log(error);
                  // console.log(error.response.data);
                  this.setState({
                    uploading: false,
                    error: error.response.data.status + " : " + error.response.data.error,
                  });
                });
            } else {
              // console.log("Unable to upload files (No Id defined)");
              this.refresh();
            }
          } else {
            this.refresh();
          }
          Api.put("/api/needs/" + itemId + "/follow"); // follow the need when creating it
        })
        .catch((error) => {
          // console.log(error);
          // console.log("An error has occured");
          this.setState({
            uploading: false,
            error: <FormattedMessage id="err-" defaultMessage="An error has occured" />,
          });
        });
    }
  }

  changeDisplay() {
    // console.log("change display");
    this.setState({ isCreating: !this.state.isCreating });
  }

  refresh() {
    this.setState({
      documents: [],
      isCreating: false,
      need: {
        content: "",
        end_date: "",
        documents: [],
        skills: [],
        ressources: [],
        title: "",
      },
      valid_content: undefined,
      valid_end_date: undefined,
      // valid_skills: undefined,
      valid_title: undefined,
      uploading: false,
    });
    this.props.refresh();
  }

  render() {
    const { error, isCreating, need, uploading } = this.state;
    if (need === undefined) {
      return "OK";
    } else {
      if (isCreating) {
        return (
          <div className="needCreate isCreating row">
            <div className="col-12 col-md-10">
              {error && (
                <div className="alert alert-danger" role="alert">
                  <FormattedMessage id={"err-"} defaultMessage="An error has occurred" />
                </div>
              )}
              <NeedForm
                action="create"
                cancel={this.changeDisplay.bind(this)}
                need={need}
                handleChange={this.handleChange}
                handleSubmit={this.handleSubmit}
                uploading={uploading}
              />
            </div>
            <div className="col-12 col-md-2">
              <NeedDocsManagement need={need} handleChange={this.handleChangeDoc.bind(this)} mode="create" />
            </div>
          </div>
        );
      } else {
        return (
          <div className="needCreate justButton">
            <button className="btn btn-primary" onClick={() => this.changeDisplay()}>
              <FormattedMessage id="need.addNeed" defaultMessage="Add a need" />
            </button>
          </div>
        );
      }
    }
  }
}
NeedCreate.contextType = UserContext;
