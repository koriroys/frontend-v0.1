import React, { Component, Fragment } from "react";
import { FormattedMessage } from "react-intl";
import defaultLogoImg from "assets/img/default/default-user.png";
import "./NeedWorkers.scss";
import Api from "Api";
import { Link } from "react-router-dom";
import Loading from "Components/Tools/Loading";
import NeedWorkersDelete from "./NeedWorkersDelete";

export default class NeedWorkers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gotUsers: false,
    };
  }

  static get defaultProps() {
    return {
      need: undefined,
      refresh: () => console.log("Missing function"),
    };
  }

  componentDidMount() {
    this.getMembers();
  }

  getDaysLeft(end_date) {
    const now = new Date();
    const end = new Date(end_date);
    var daysLeft = Math.round((end - now) / 1000 / 60 / 60 / 24);
    if (daysLeft < 0) {
      daysLeft = 0;
    }
    return daysLeft;
  }

  getMembers() {
    this.setState({ loading: true });
    Api.get(`/api/needs/${this.props.need.id}/members`)
      .then((res) => {
        var needMembers = [];
        res.data.users.map((user) => {
          return needMembers.push(user);
        });
        // console.log(needMembers);
        this.setState({ needMembers, loading: false, gotUsers: true });
      })
      .catch((error) => {
        this.setState({ loading: false });
      });
  }

  render() {
    if (!this.state.gotUsers) {
      return <Loading active={this.state.loading} height="150px"></Loading>;
    }
    const { need, mode, refresh } = this.props;
    const { needMembers } = this.state;
    console.log(needMembers);
    if (need === undefined || need.creator === undefined) {
      return null;
    } else {
      var CreatorLogoUrl = !need.creator.logo_url ? defaultLogoImg : need.creator.logo_url;
      return (
        <div className="needWorkers">
          <FormattedMessage id="need.card.working" defaultMessage="Working on it: " />
          <span className="workers">
            <span className="imgList">
              <Link to={"/user/" + need.creator.id}>
                <img
                  className="peopleImg"
                  src={CreatorLogoUrl}
                  alt={need.creator.first_name + " " + need.creator.last_name}
                />
              </Link>
              {needMembers.map((user, index) => {
                // map
                var logoUrl = !user.logo_url ? defaultLogoImg : user.logo_url;
                if (index < 6) {
                  return (
                    <>
                      <Link to={"/user/" + user.id} key={index}>
                        <img className="peopleImg" src={logoUrl} alt={user.first_name + " " + user.last_name} />
                      </Link>
                      {mode === "update" && need.is_owner && (
                        <NeedWorkersDelete worker={user} itemId={need.id} itemType="needs" refresh={refresh} />
                      )}
                    </>
                  );
                }
                return "";
              })}
              {need.members_count > 6 && <div className="moreMembers">+{need.members_count - 7}</div>}
            </span>
          </span>
        </div>
      );
    }
  }
}
