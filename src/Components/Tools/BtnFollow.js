import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import Api from "Api";

export default class BtnFollow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      followState: this.props.followState,
    };
  }

  static get defaultProps() {
    return {
      classBtn: "btn btn-md btn-outline-primary",
      followState: false,
      itemType: undefined,
      itemId: undefined,
      sending: false,
      textFollow: <FormattedMessage id="general.follow" defaultMessage="Follow" />,
      textUnfollow: <FormattedMessage id="general.unfollow" defaultMessage="Unfollow" />,
      source: "api",
    };
  }

  componentWillReceiveProps(nextProps) {
    const { followState, itemId } = nextProps;
    this.setState({ followState, itemId });
  }

  getApiFollowState() {
    const { itemId, itemType } = this.props;
    Api.get(`/api/${itemType}/${itemId}`)
      .then((res) => {
        this.setState({ followState: res.data.has_followed });
      })
      .catch((error) => {});
  }

  componentDidMount() {
    // if data also comes from algolia (and not only api), get follow state from api (because it's not available in algolia)
    if (this.props.source === "algolia") this.getApiFollowState();
  }

  changeStateFollow(action) {
    const { itemId, itemType } = this.props;
    if (!itemId || !itemType) {
    } else {
      this.setState({ sending: true });
      if (action == "follow") {
        // if action is follow
        Api.put("/api/" + itemType + "/" + itemId + "/follow")
          .then((res) => {
            this.setState({ followState: !this.state.followState, sending: false });
          })
          .catch((error) => {
            this.setState({ sending: false });
          });
      } else {
        // else delete follow
        Api.delete("/api/" + itemType + "/" + itemId + "/follow")
          .then((res) => {
            this.setState({ followState: !this.state.followState, sending: false });
          })
          .catch((error) => {
            this.setState({ sending: false });
          });
      }
    }
  }

  render() {
    const { followState, sending } = this.state;
    const { classBtn, style, textFollow, textUnfollow } = this.props;
    var text = followState ? textUnfollow : textFollow;
    var action = followState ? "unfollow" : "follow";
    return (
      <button onClick={() => this.changeStateFollow(action)} className={classBtn} style={style} disabled={sending}>
        {sending ? (
          <span>
            <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true"></span>
            &nbsp; {text}
          </span>
        ) : (
          text
        )}
      </button>
    );
  }
}
