import React, { Component, Fragment } from "react";
import { FormattedMessage } from "react-intl";
import "./InfoInterestsComponent.scss";
import TitleInfo from "Components/Tools/TitleInfo";
import $ from "jquery";

export default class InfoInterestsComponent extends Component {
  static get defaultProps() {
    return {
      title: "Title",
      content: [],
      colSizeTitle: "col-12 col-sm-3 col-md-2",
      colSizeContent: "col-12 col-sm-9 col-md-10",
      place: "",
      displayCount: "4",
    };
  }

  showMore(items) {
    $(".less").show();
    $(".interest:lt(" + items + ")").show(300);
    $(".more").hide();
  }

  showLess() {
    $(".interest").not(":lt(4)").hide(300);
    $(".more").show();
    $(".less").hide();
  }

  render() {
    const { title, content, colSizeTitle, colSizeContent, place, displayCount } = this.props;
    let titleinfo = "";

    if (title) {
      titleinfo = <TitleInfo title={title} classSize={colSizeTitle} />;
    }
    var ContentClassNames = place === "entity_header" ? "col-12 interests" : colSizeContent + " interests";
    var userLang = navigator.language || navigator.userLanguage;
    var path =
      (localStorage.getItem("language") && localStorage.getItem("language") === "fr") ||
      (!localStorage.getItem("language") && (userLang === "fr-FR" || userLang === "fr"))
        ? "interests/fr" // if website or browser is in french
        : "interests"; // if english
    var interestClass = "";
    if (content.length > 0) {
      return (
        <div className="row infoInterests">
          {titleinfo}
          <div className={ContentClassNames}>
            {content.map((interest, index) => {
              interestClass = index < 4 ? "interest" : "interest toggle";
              return (
                <div className={interestClass} key={index}>
                  <img
                    src={require("assets/img/" + path + "/Interest-" + interest + ".png")}
                    className="imgInterest"
                    alt={"Interest-" + interest}
                  />
                </div>
              );
            })}
            {content.length > 4 && (
              <Fragment>
                <div className="more" onClick={() => this.showMore(content.length)}>
                  <FormattedMessage id="general.showmore" defaultMessage="Show More" />
                </div>
                <div className="less" onClick={() => this.showLess()}>
                  <FormattedMessage id="general.showless" defaultMessage="Show Less" />
                </div>
              </Fragment>
            )}
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
}
