import React, { Component, Fragment } from "react";
import { FormattedMessage } from "react-intl";
import "./InfoRessourcesComponent.scss";
import TitleInfo from "Components/Tools/TitleInfo";
import $ from "jquery";
import { Link } from "react-router-dom";

export default class InfoRessourcesComponent extends Component {
  static get defaultProps() {
    return {
      title: "",
      content: "",
      limit: "99",
      place: "",
      colSizeTitle: "col-12 col-sm-3 col-md-2",
      colSizeContent: "col-12 col-sm-9 col-md-10",
      displayCount: "4",
    };
  }

  showMore(items) {
    $(".lessRessources").show();
    $(".ressource:lt(" + items + ")").removeClass("toggle");
    $(".moreRessources").hide();
  }

  showLess() {
    $(".ressource").not(":lt(6)").addClass("toggle");
    $(".moreRessources").show();
    $(".lessRessources").hide();
  }

  render() {
    const { title, content, colSizeTitle, colSizeContent, limit, place, displayCount } = this.props;

    let titleinfo = "";

    if (title) {
      titleinfo = <TitleInfo title={title} classSize={colSizeTitle} />;
    }

    var ressourceClass = "ressource";
    var ContentClassNames =
      place === "entity_header" ? "col-12" : "userProfile" ? colSizeContent : "entity_card" ? "" : colSizeContent;
    var ContainerClassName = place === "entity_card" ? "infoRessources" : "row infoRessources";
    if (content.length > 0) {
      return (
        <div className={ContainerClassName}>
          {titleinfo}
          <div className={ContentClassNames}>
            {content.slice(0, limit).map((ressource, index) => {
              {
                /* check if ressource has id, because ressources from needs have different structure if it comes from a project or challenge/program for ex. If has id, access ressource with ressource_name*/
              }
              var theRessource = ressource.id ? ressource.ressource_name : ressource;
              if (place != "userProfile") ressourceClass = index < 6 ? "ressource" : "ressource toggle"; // don't do this on user Profile
              return (
                <Link key={index} to={"/search/?refinementList[ressources][0]=" + theRessource}>
                  <span key={index} className={ressourceClass}>
                    {theRessource}
                  </span>
                </Link>
              );
            })}
            {// display more/less ressources links when more than 6 ressources, and we are not in a header or user profile
            content.length > 6 && limit != 3 && place != "userProfile" ? (
              <Fragment>
                <div className="moreRessources" onClick={() => this.showMore(content.length)}>
                  <FormattedMessage id="general.showmore" defaultMessage="Show More" />
                </div>
                <div className="lessRessources" onClick={() => this.showLess()}>
                  <FormattedMessage id="general.showless" defaultMessage="Show Less" />
                </div>
              </Fragment>
            ) : (
              ""
            )}
            {limit == 3 && content.length > 3 ? <span className="ressource more">+{content.length - 3}</span> : ""}
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
}
