import React, { Component } from "react";
import TitleInfo from "Components/Tools/TitleInfo";
import "./InfoAddressComponent.scss";

export default class InfoAddressComponent extends Component {
  static get defaultProps() {
    return {
      title: "Title",
      address: "",
      city: "",
      country: "",
      colSizeTitle: "col-12 col-sm-3 col-md-2",
      colSizeContent: "col-12 col-sm-9 col-md-10",
    };
  }

  render() {
    const { title, address, city, country, colSizeTitle, colSizeContent } = this.props;
    let titleinfo = "";

    if (title) {
      titleinfo = <TitleInfo title={title} classSize={colSizeTitle} />;
    }

    if (address || city || country) {
      return (
        <div className="row infoAddress">
          {titleinfo}
          <div className={colSizeContent + " content"}>
            {/* <p id="address">{address}</p>
            <p id="city">{city}</p>
            <p id="country">{country}</p> */}
            <span id="full_address">
              {/* {address ? address+", " : ""} */}
              {city ? city + ", " : ""}
              {country}
            </span>
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
}
