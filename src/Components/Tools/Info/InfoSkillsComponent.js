import React, { Component, Fragment } from "react";
import { FormattedMessage } from "react-intl";
import "./InfoSkillsComponent.scss";
import TitleInfo from "Components/Tools/TitleInfo";
import { Link } from "react-router-dom";
import $ from "jquery";

export default class InfoSkillsComponent extends Component {
  static get defaultProps() {
    return {
      title: "",
      content: "",
      limit: "99",
      place: "",
      colSizeTitle: "col-12 col-sm-3 col-md-2",
      colSizeContent: "col-12 col-sm-9 col-md-10",
      displayCount: "4",
    };
  }

  showMore(items) {
    $(".lessSkills").show();
    $(".skill:lt(" + items + ")").removeClass("toggle");
    $(".moreSkills").hide();
  }

  showLess() {
    $(".skill").not(":lt(6)").addClass("toggle");
    $(".moreSkills").show();
    $(".lessSkills").hide();
  }

  // goToAbout(){
  // var els = document.querySelectorAll("a[href^='#about']");
  // for (var i = 0, l = els.length; i < l; i++) {
  // 	var el = els[i];
  // 	el.className = "nav-item nav-link active";
  // }
  // }

  render() {
    const { title, content, colSizeTitle, colSizeContent, limit, place, displayCount } = this.props;

    let titleinfo = "";

    if (title) {
      titleinfo = <TitleInfo title={title} classSize={colSizeTitle} />;
    }

    var skillClass = "skill";
    var ContentClassNames =
      place === "entity_header" ? "col-12" : "userProfile" ? colSizeContent : "entity_card" ? "" : colSizeContent;
    var ContainerClassName = place === "entity_card" ? "infoSkills" : "row infoSkills";
    if (content.length > 0) {
      return (
        <div className={ContainerClassName}>
          {titleinfo}
          <div className={ContentClassNames}>
            {content.slice(0, limit).map((skill, index) => {
              {
                /* check if skill has id, because skills from needs have different structure if it comes from a project or challenge/program for ex. If has id, access skill with skill_name*/
              }
              var theSkill = skill.id ? skill.skill_name : skill;
              if (place != "userProfile") skillClass = index < 6 ? "skill" : "skill toggle"; // don't do this on user Profile
              return (
                <Link key={index} to={"/search/?refinementList[skills][0]=" + theSkill}>
                  <span key={index} className={skillClass}>
                    {theSkill}
                  </span>
                </Link>
              );
            })}
            {// display more/less skills links when more than 6 skills, and we are not in a header or user profile
            content.length > 6 && limit != 3 && place != "userProfile" ? (
              <Fragment>
                <div className="moreSkills" onClick={() => this.showMore(content.length)}>
                  <FormattedMessage id="general.showmore" defaultMessage="Show More" />
                </div>
                <div className="lessSkills" onClick={() => this.showLess()}>
                  <FormattedMessage id="general.showless" defaultMessage="Show Less" />
                </div>
              </Fragment>
            ) : (
              ""
            )}
            {/* {limit<=3 ? <span><a href="#about" data-toggle="tab" onClick={() => this.goToAbout()}>Show more</a></span> : ""} */}
            {/* {limit == 3 && content.length > 3 ? <span>...</span> : ""} */}
            {limit == 3 && content.length > 3 ? <span className="skill more">+{content.length - 3}</span> : ""}
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
}
