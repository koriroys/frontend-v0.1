import React, { Component } from "react";
import $ from "jquery";
import { linkifyQuill } from "Components/Tools/utils/utils.js";
import "react-quill/dist/quill.snow.css";
import "./InfoHtmlComponent.scss";

export default class InfoHtmlComponent extends Component {
  static get defaultProps() {
    return {
      title: "Title",
      content: "",
      // colSizeTitle: "col-3",
      // colSizeContent: "col-12"
    };
  }

  componentDidMount() {
    $(".tab-pane#bout .infoHtml a[href]").attr("target", "_blank");
  }

  render() {
    var {
      // title,
      content,
      // colSizeTitle,
      // colSizeContent
    } = this.props;
    // let titleinfo = "";

    // if (title) {
    //   titleinfo = <TitleInfo title={title} classSize={colSizeTitle} />;
    //   colSizeContent = "col-9"
    // }

    if (content) {
      content = linkifyQuill(content);
      return (
        <div className="infoHtml">
          {/* {titleinfo} */}
          <div className="content">
            {/* <div className={(colSizeContent) + " content"}> */}
            <div dangerouslySetInnerHTML={{ __html: content }} className="rendered-quill-content ql-editor" />
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
}
