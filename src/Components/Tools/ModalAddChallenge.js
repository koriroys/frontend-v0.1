import React, { Component, Fragment } from "react";
import { FormattedMessage } from "react-intl";
import Api from "Api";
import $ from "jquery";
import Loading from "Components/Tools/Loading";
import Alert from "Components/Tools/Alert";

export default class ModalAddChallenge extends Component {
  constructor(props) {
    super(props);

    this.state = {
      actualList: this.props.actualList,
      listMine: [],
      loading: true,
      challengeSelected: undefined,
      disabledBtn: false,
      requestSent: false,
      error: "",
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  static get defaultProps() {
    return {
      itemId: undefined,
      itemType: "",
      actualList: [],
      refreshList: () => console.log("props is missing"),
    };
  }

  componentDidMount() {
    this.getMyChallenges();
  }

  componentWillReceiveProps(nextProps) {
    const actualList = nextProps.actualList;
    // console.log("nEW PROPS RECEIVE : ", actualList);
    this.setState({ actualList });
    this.getMyChallenges();
  }

  handleChange(event) {
    // console.log(event.target.value);
    const challengeSelected = this.state.listMine.find(function (challenge) {
      return Number(challenge.id) === Number(event.target.value);
    });
    // console.log(challengeSelected);
    this.setState({ challengeSelected: challengeSelected, disabledBtn: false });
  }

  handleSubmit(event) {
    const { listMine, challengeSelected } = this.state;
    const { itemId, itemType } = this.props;

    event.preventDefault();
    if (itemType !== "programs") {
      // console.log("itemType not compatible");
    } else if (!itemId) {
      // console.log("itemId is missing");
    } else if (challengeSelected === undefined || challengeSelected === null) {
      // console.log("A challenge can be selected");
      this.setState({ error: "A challenge can be selected" });
      setTimeout(() => {
        this.setState({ error: "" });
      }, 4000);
    } else {
      this.setState({ disabledBtn: true, error: "" });
      Api.put("/api/" + itemType + "/" + itemId + "/challenges/" + challengeSelected.id)
        .then((res) => {
          // console.log("Succes : ", res);
          listMine.splice(
            listMine.findIndex((challenge) => {
              return challenge === challengeSelected;
            }),
            1
          );
          this.props.refreshList();
          this.setState({ requestSent: true, listMine: listMine });
          setTimeout(() => {
            //close
            $("#closeModal").click();
            this.resetState();
          }, 6000);
        })
        .catch((error) => {
          // console.log("Echec : ", error);
          this.setState({ error: "An error has occured" });
          setTimeout(() => {
            this.setState({ disabledBtn: false, error: "" });
          }, 8000);
        });
    }
  }

  /*   getActualChallenges(){
    return new Promise((resolve, reject) => {
      Api.get("/api/" + this.props.itemType + "/" + this.props.itemId + "/challenges")
      .then(res=>{
        console.log("Modal List challenges receive : ", res.data.challenges);
        this.setState({actualList: res.data.challenges});
        resolve(res.data.challenges);
      })
      .catch(error=>{
        console.log("ERR : ", error);
        reject();
      });
    });
  } */

  getMyChallenges() {
    this.setState({ loading: true });
    Api.get("/api/challenges/mine")
      .then((res) => {
        /* this.getActualChallenges()
      .then((actualList) => { */
        var listMine = [];
        res.data.map((myChallenge) => {
          // console.log("My challenge : ", myChallenge);
          var alreadyHere = false;
          // console.log("ActualChallenges : ", this.state.actualList)
          this.state.actualList.forEach((attachChallenge) => {
            // console.log("Actual challenge : ", attachChallenge);
            if (attachChallenge.id === myChallenge.id) {
              // console.log("Already Here");
              alreadyHere = true;
            }
          });
          return alreadyHere ? null : listMine.push(myChallenge);
        });
        this.setState({ listMine: listMine, loading: false });
        /* })
      .catch((error) => {
        console.log(error);
        this.setState({loading: false});
      }); */
      })
      .catch((error) => {
        // console.log(error);
        this.setState({ loading: false });
      });
  }

  resetState() {
    // console.log("resetState applied");
    this.setState({
      challengeSelected: undefined,
      disabledBtn: false,
      requestSent: false,
      error: "",
    });
    this.getMyChallenges();
  }

  render() {
    const { disabledBtn, error, listMine, loading, requestSent } = this.state;

    return (
      <div className="modal fade" id="modalAddChallenge" tabIndex="-1" role="dialog" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="membersModalLabel">
                <FormattedMessage id="attach.challenge.button.title" defaultMessage="Add a challenge" />
              </h5>
              <button type="button" id="closeModal" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <Loading active={loading} height="30vh">
                {listMine.length > 0 ? (
                  <form onSubmit={this.handleSubmit} style={{ textAlign: "left" }}>
                    {listMine.map((challenge, index) => (
                      <div className="form-check" key={index} style={{ height: "50px" }}>
                        <input
                          type="radio"
                          className="form-check-input"
                          name="exampleRadios"
                          id={"challenge-" + index}
                          value={challenge.id}
                          onChange={this.handleChange}
                        />
                        <label className="form-check-label" htmlFor={"challenge-" + index}>
                          {challenge.title}
                        </label>
                      </div>
                    ))}
                    <div className="text-center btnZone">
                      <button type="submit" className="btn btn-primary btn-block" disabled={disabledBtn}>
                        {requestSent ? (
                          <FormattedMessage id="attach.challenge.btnSendEnded" defaultMessage="challenge sent" />
                        ) : (
                          <Fragment>
                            {disabledBtn && (
                              <Fragment>
                                <span
                                  className="spinner-border spinner-border-sm text-center"
                                  role="status"
                                  aria-hidden="true"
                                ></span>
                                &nbsp;
                              </Fragment>
                            )}
                            <FormattedMessage id="attach.challenge.btnSend" defaultMessage="Submit this challenge" />
                          </Fragment>
                        )}
                      </button>
                    </div>
                    {error !== "" && <Alert type="danger" message={error} />}
                  </form>
                ) : (
                  <div className="noChallenge" style={{ textAlign: "center" }}>
                    <FormattedMessage
                      id="attach.challenge.noChallenge"
                      defaultMessage="You do not have a challenge to add"
                    />
                  </div>
                )}
                {requestSent && (
                  <Alert
                    type="success"
                    message={
                      <FormattedMessage
                        id="attach.challenge.success"
                        defaultMessage="The challenge has been sent. He will be examined by the challenge team to validate his commitment to the challenge."
                      />
                    }
                  />
                )}
              </Loading>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
