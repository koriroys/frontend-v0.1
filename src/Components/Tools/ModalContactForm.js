import React, { Component, Fragment } from "react";
import { injectIntl } from "react-intl";
import Api from "Api";
import Alert from "Components/Tools/Alert";
import "./ModalContactForm.scss";

class ModalContactForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      disabledBtn: true,
      object: "",
      message: "",
      error: "",
      sent: false,
      sending: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  static get defaultProps() {
    return {
      itemId: undefined,
      itemType: "",
      refreshList: () => console.log("props is missing"),
    };
  }

  handleChange(event) {
    const { itemId } = this.props;
    this.setState({ [event.target.name]: event.target.value });
    let form = document.querySelector(`#ModalContactForm${itemId} form`);
    // change button disablbe state depending on form validity (in all field are not empty)
    this.setState({ disabledBtn: form.checkValidity() ? false : true });
  }

  handleSubmit(event) {
    const { itemId } = this.props;
    event.preventDefault();
    this.setState({ sending: true, disabledBtn: true, error: "" });
    var param = {
      object: this.state.object,
      content: this.state.message,
    };
    Api.post("/api/users/" + itemId + "/send_email", param)
      .then((res) => {
        this.setState({ sent: true, sending: false });
        setTimeout(() => {
          document.querySelector(`#ModalContactForm${itemId} .modal-header .close`).click(); //close modal
          this.resetState();
        }, 3500);
      })
      .catch((error) => {
        const errorMessage = error.response;
        // console.log("errorMessage", errorMessage);
        this.setState({ error: errorMessage.data });
      });
  }

  resetState() {
    this.setState({
      error: "",
      object: "",
      content: "",
    });
  }

  render() {
    var { intl, itemId } = this.props;
    const { disabledBtn, sent, sending } = this.state;
    return (
      <Fragment>
        <button
          className="btn btn-md btn-outline-primary"
          data-toggle="modal"
          data-target={`#ModalContactForm${itemId}`}
        >
          {intl.formatMessage({ id: "user.btn.contact" })}
        </button>
        <div className="modal fade" id={`ModalContactForm${itemId}`} tabIndex="-1" role="dialog" aria-hidden="true">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  {intl.formatMessage({ id: "user.contactModal.title" })}
                </h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <form>
                  {/* <form onSubmit={this.handleSubmit}> */}
                  <div className="form-group">
                    <label htmlFor="recipient-name" className="col-form-label">
                      {intl.formatMessage({ id: "user.contactModal.subject" })}
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      id="recipient-name"
                      name="object"
                      onChange={this.handleChange.bind(this)}
                      required
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="message-text" className="col-form-label">
                      {intl.formatMessage({ id: "user.contactModal.message" })}
                    </label>
                    <textarea
                      className="form-control"
                      id="message-text"
                      name="message"
                      onChange={this.handleChange.bind(this)}
                      required
                    ></textarea>
                  </div>
                  {sent && <Alert type="success" message={intl.formatMessage({ id: "user.contactModal.success" })} />}
                  <button type="button" className="btn btn-secondary" data-dismiss="modal">
                    {intl.formatMessage({ id: "user.contactModal.close" })}
                  </button>
                  <button type="submit" className="btn btn-primary" disabled={disabledBtn} onClick={this.handleSubmit}>
                    <Fragment>
                      {sending && (
                        <Fragment>
                          <span
                            className="spinner-border spinner-border-sm text-center"
                            role="status"
                            aria-hidden="true"
                          ></span>
                          &nbsp;
                        </Fragment>
                      )}
                      {intl.formatMessage({ id: "user.contactModal.send" })}
                    </Fragment>
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}
export default injectIntl(ModalContactForm);
