import React, { Component, Fragment } from "react";
import { FormattedMessage } from "react-intl";
import Api from "Api";
import Loading from "Components/Tools/Loading";
import Alert from "Components/Tools/Alert";
import { UserContext } from "UserProvider";

export default class ModalAddProject extends Component {
  constructor(props) {
    super(props);

    this.state = {
      actualList: this.props.actualList,
      listMine: [],
      listChallenges: [],
      loading: true,
      projectSelected: undefined,
      challengeSelected: undefined,
      disabledBtn: true,
      sending: false,
      requestSent: false,
      error: "",
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  static get defaultProps() {
    return {
      itemId: undefined,
      itemType: "",
      actualList: [],
      refreshList: () => console.log("props is missing"),
    };
  }

  componentDidMount() {
    this.props.itemType == "challenges" ? this.getMyProjects() : this.getChallenges();
  }

  componentWillReceiveProps(nextProps) {
    const actualList = nextProps.actualList;
    // console.log("nEW PROPS RECEIVE : ", actualList);
    this.setState({ actualList });
    this.props.itemType == "challenges" ? this.getMyProjects() : this.getChallenges();
  }

  handleChange(event) {
    if (this.props.itemType == "challenges") {
      const projectSelected = this.state.listMine.find(function (project) {
        return Number(project.id) === Number(event.target.value);
      });
      this.setState({ projectSelected, disabledBtn: false });
    } else {
      const challengeSelected = this.state.listChallenges.find(function (challenge) {
        return Number(challenge.id) === Number(event.target.value);
      });
      this.setState({ challengeSelected, disabledBtn: false });
    }
  }

  handleSubmit(event) {
    const { listMine, listChallenges, challengeSelected, projectSelected } = this.state;
    const { itemId, itemType } = this.props;
    var list = this.props.itemType == "challenges" ? listMine : listChallenges;
    var selectedItem = this.props.itemType == "challenges" ? projectSelected : challengeSelected;

    event.preventDefault();
    this.setState({ disabledBtn: true, sending: true, error: "" });
    var apiCall =
      this.props.itemType == "challenges"
        ? "/api/" + itemType + "/" + itemId + "/projects/" + selectedItem.id
        : "/api/challenges/" + selectedItem.id + "/projects/" + itemId;
    Api.put(apiCall)
      .then((res) => {
        list.splice(
          list.findIndex((project) => {
            return project === selectedItem;
          }),
          1
        );
        this.props.refreshList();
        this.setState({ requestSent: true, sending: false, listMine: list, listChallenges: list });
        setTimeout(() => {
          document.querySelector("#closeModal").click(); //close
          this.resetState();
        }, 4000);
      })
      .catch((error) => {
        // console.log("Echec : ", error);
        this.setState({ error: "An error has occured" });
        setTimeout(() => {
          this.setState({ disabledBtn: false, error: "" });
        }, 8000);
      });
  }

  getActualProjects() {
    return new Promise((resolve, reject) => {
      Api.get("/api/" + this.props.itemType + "/" + this.props.itemId + "/projects")
        .then((res) => {
          // console.log("Modal List projects receive : ", res.data.projects);
          this.setState({ actualList: res.data.projects });
          resolve(res.data.projects);
        })
        .catch((error) => {
          // console.log("ERR : ", error);
          reject();
        });
    });
  }

  getMyProjects() {
    let userContext = this.context;
    if (userContext.connected) {
      // only if user is connected
      this.getActualProjects();
      this.setState({ loading: true });
      Api.get("/api/projects/mine")
        .then((res) => {
          /* this.getActualProjects()
	      .then((actualList) => { */
          var listMine = [];
          res.data.map((myProject) => {
            // console.log("My project : ", myProject);
            var alreadyHere = false;
            // console.log("ActualProjects : ", this.state.actualList)
            // console.log(this.state.actualList);
            this.state.actualList.forEach((attachProject) => {
              // console.log("Actual project : ", attachProject);
              if (attachProject.id === myProject.id) {
                // console.log("Already Here");
                alreadyHere = true;
              }
            });
            return alreadyHere ? null : listMine.push(myProject);
          });
          this.setState({ listMine: listMine, loading: false });
          /* })
	      .catch((error) => {
	        console.log(error);
	        this.setState({loading: false});
	      }); */
        })
        .catch((error) => {
          // console.log(error);
          this.setState({ loading: false });
        });
    }
  }

  getChallenges() {
    this.setState({ loading: true });
    Api.get("/api/challenges")
      .then((res) => {
        var listChallenges = res.data;
        this.setState({ listChallenges, loading: false });
      })
      .catch((error) => {
        // console.log(error);
        this.setState({ loading: false });
      });
  }

  resetState() {
    // console.log("resetState applied");
    this.setState({
      projectSelected: undefined,
      challengeSelected: undefined,
      disabledBtn: false,
      requestSent: false,
      error: "",
    });
    this.props.itemType == "challenges" ? this.getMyProjects() : this.getChallenges();
  }

  render() {
    const { disabledBtn, error, listMine, listChallenges, loading, requestSent, sending } = this.state;
    var list = this.props.itemType == "challenges" ? listMine : listChallenges;
    var attachProjectTitle = this.props.itemType == "challenges" ? "attach.project.title" : "attach.myproject.title";
    return (
      <div className="modal fade" id="modalAddProject" tabIndex="-1" role="dialog" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="ModalAddProject">
                <FormattedMessage id={attachProjectTitle} defaultMessage="Add a project" />
              </h5>
              <button type="button" id="closeModal" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <Loading active={loading} height="30vh">
                {this.props.itemType == "challenges" && (
                  <a href="/project/create" target="_blank">
                    <FormattedMessage id="projects.searchBar.btnAddProject" defaultMessage="Add a project" />
                  </a>
                )}
                {list.length > 0 ? (
                  <form onSubmit={this.handleSubmit} style={{ textAlign: "left" }}>
                    {list.map((project, index) => (
                      <div className="form-check" key={index} style={{ height: "50px" }}>
                        <input
                          type="radio"
                          className="form-check-input"
                          name="exampleRadios"
                          id={"project-" + index}
                          value={project.id}
                          onChange={this.handleChange}
                        />
                        <label className="form-check-label" htmlFor={"project-" + index}>
                          {project.title}
                        </label>
                      </div>
                    ))}
                    {/* <p className="readRules">
											<FormattedMessage id="attach.project.readRules" />
											<a href="http://bit.ly/participation-rules-coimmune" target="_blank">
				                <FormattedMessage id="attach.project.readRulesLink"/>
				            	</a>
										</p> */}
                    <div className="text-center btnZone">
                      <button type="submit" className="btn btn-primary btn-block" disabled={disabledBtn}>
                        {requestSent ? (
                          <FormattedMessage id="attach.project.btnSendEnded" defaultMessage="project was submitted" />
                        ) : (
                          <Fragment>
                            {sending && (
                              <Fragment>
                                <span
                                  className="spinner-border spinner-border-sm text-center"
                                  role="status"
                                  aria-hidden="true"
                                ></span>
                                &nbsp;
                              </Fragment>
                            )}
                            <FormattedMessage id="attach.project.btnSend" defaultMessage="Submit this project" />
                          </Fragment>
                        )}
                      </button>
                    </div>
                    {error !== "" && <Alert type="danger" message={error} />}
                  </form>
                ) : (
                  <div className="noProject" style={{ textAlign: "center" }}>
                    <FormattedMessage id="attach.project.noProject" defaultMessage="You do not have a project to add" />
                  </div>
                )}
                {requestSent && (
                  <Alert
                    type="success"
                    message={
                      <FormattedMessage
                        id="attach.project.success"
                        defaultMessage="The project has been sent. He will be examined by the challenge team to validate his commitment to the challenge."
                      />
                    }
                  />
                )}
              </Loading>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
ModalAddProject.contextType = UserContext;
