import React from "react";

const TitleInfo = ({ title, classSize, mandatory }) => (
  <div className={classSize + " titleInfo"}>
    {title}
    {mandatory && "*"}:
  </div>
);

export default TitleInfo;
