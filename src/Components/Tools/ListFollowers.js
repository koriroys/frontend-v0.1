import React, { Component } from "react";
// import { FormattedMessage } from "react-intl";
import Api from "Api";
import Loading from "Components/Tools/Loading";
import PeopleList from "Components/People/PeopleList";

export default class ListFollowers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listFollowers: [],
      loading: true,
    };
  }

  static get defaultProps() {
    return {
      itemId: undefined,
      itemType: undefined,
    };
  }

  componentDidMount() {
    const { itemId, itemType } = this.props;
    this.getFollowers(itemId, itemType);
  }

  componentWillReceiveProps(nextProps) {
    const { itemId, itemType } = nextProps;
    this.getFollowers(itemId, itemType);
  }

  getFollowers(itemId, itemType) {
    this.setState({ loading: true });
    Api.get("/api/" + itemType + "/" + itemId + "/followers")
      .then((res) => {
        var listUser = [];
        // console.log(res);
        res.data.followers.map((user) => {
          return listUser.push(user);
        });
        this.setState({ listFollowers: listUser, loading: false });
      })
      .catch((error) => {
        // console.log(error);
        this.setState({ loading: false });
      });
  }

  render() {
    const { listFollowers, loading } = this.state;

    return (
      <div className="listFollowers">
        <Loading active={loading}>
          <PeopleList searchBar={false} listPeople={listFollowers} />
        </Loading>
      </div>
    );
  }
}
