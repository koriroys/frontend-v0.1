import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import ChallengeCard from "Components/Challenges/ChallengeCard";
import ChallengeAttachedCard from "Components/Challenges/ChallengeAttachedCard";
import CommunityCard from "Components/Communities/CommunityCard";
import MemberCard from "Components/Members/MemberCard";
import PeopleCard from "Components/People/PeopleCard";
import ProjectAttachedCard from "Components/Projects/ProjectAttachedCard";
import ProjectCard from "Components/Projects/ProjectCard";
import ProgramCard from "Components/Programs/ProgramCard";
import "./ListComponent.scss";

export default class ListComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.data,
      itemType: this.props.itemType,
    };
  }

  static get defaultProps() {
    return {
      callBack: () => {
        console.log("Missing callback");
      },
      data: [],
      itemParentId: undefined,
      itemParentType: undefined,
      itemType: undefined,
      colMax: 3,
      cardFormat: undefined,
      filter: undefined,
    };
  }

  renderItemType(itemType, item) {
    const { itemParentId, itemParentType, callBack, cardFormat } = this.props;
    switch (itemType) {
      case "challenge":
        return <ChallengeCard challenge={item} cardFormat={cardFormat} />;
      case "challengeAttached":
        return <ChallengeAttachedCard challenge={item} />;
      case "community":
        return <CommunityCard community={item} cardFormat={cardFormat} />;
      case "member":
        return <MemberCard itemId={itemParentId} itemType={itemParentType} member={item} callBack={callBack} />;
      case "people":
        return <PeopleCard user={item} />;
      case "project":
        return <ProjectCard project={item} cardFormat={cardFormat} />;
      case "program":
        return <ProgramCard program={item} />;
      case "projectAttached":
        return <ProjectAttachedCard project={item} callBack={callBack} />;
      default:
        // console.log("itemType not available to the ListComponent");
        return null;
    }
  }

  renderList() {
    var classes = "col-md-4 col-sm-6";
    var listItems = this.props.data;
    var { itemType, filter } = this.props;
    switch (this.props.colMax) {
      case 1:
        classes = "col-12";
        break;
      case 2:
        classes = "col-md-12 col-lg-6";
        break;
      case 3:
        classes = "col-12 col-md-6 col-lg-4";
        break;
      case 4:
        classes = "col-lg-3 col-md-4 col-sm-6";
        break;
      default:
        classes = "col-md-4 col-sm-6";
    }
    if (this.props.data.length === 0) {
      return (
        <div className="col-12 noResult">
          <p>
            <FormattedMessage id={`list.noResult.${itemType}`} defaultMessage="No Result" />
          </p>
        </div>
      );
    } else {
      var sortedUsers = listItems; // if no filter, then it's the whole website members list
      // else create array of users depending of their status in the project (received from props)
      if (filter)
        sortedUsers =
          filter === "creators"
            ? listItems.filter((item) => item.owner)
            : filter === "admins"
            ? listItems.filter((item) => item.admin && !item.owner)
            : listItems.filter((item) => !item.owner && !item.admin && item.member);

      if (itemType === "people") {
        return (
          <div className="row">
            {sortedUsers.map((item, index) => {
              if (item !== undefined && item !== null) {
                return (
                  <div className={classes} key={index}>
                    {this.renderItemType(itemType, item)}
                  </div>
                );
              } else {
                return null;
              }
            })}
          </div>
        );
      } else {
        return (
          <div className="row">
            {listItems.map((item, index) => {
              if (item !== undefined && item !== null) {
                return (
                  <div className={classes} key={index}>
                    {this.renderItemType(itemType, item)}
                  </div>
                );
              } else {
                return null;
              }
            })}
          </div>
        );
      }
    }
  }

  render() {
    if (this.props.itemType !== undefined) {
      return <div className="listComponent">{this.renderList()}</div>;
    } else {
      return null;
    }
  }
}
