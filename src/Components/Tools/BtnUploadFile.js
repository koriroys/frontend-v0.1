import React, { Component, Fragment } from "react";
import { FormattedMessage, injectIntl } from "react-intl";
import Api from "Api";
import Alert from "Components/Tools/Alert";
import $ from "jquery";
import "./BtnUploadFile.scss";

class BtnUploadFile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      uploading: false,
    };
    this.handleChange = this.handleChange.bind(this);
  }

  static get defaultProps() {
    return {
      fileTypes: [""],
      itemId: "",
      itemType: "",
      maxSizeFile: 31457280,
      multiple: true,
      onChange: (result) => console.log("Oops, the function onChange is missing !", result),
      setListFiles: (listFiles) => console.log("Oops, the function setListFiles is missing !", listFiles),
      type: "",
      imageUrl: "",
      text: <FormattedMessage id="info-1000" defaultMessage="Attach file" />,
      textUploading: <FormattedMessage id="info-1001" defaultMessage="Uploading..." />,
      uploadNow: false,
    };
  }

  handleChange(event) {
    event.preventDefault();
    const { intl, itemType } = this.props;
    if (event.target.files) {
      if (!this.validFilesSize(event.target.files)) {
        this.props.onChange({
          error: (
            <Fragment>
              <FormattedMessage id="err-1001" defaultMessage="File's size too large" />
              &nbsp;(max : {this.props.maxSizeFile / 1024 / 1024}Mo)
            </Fragment>
          ),
          url: "",
        });
      } else if (!this.props.uploadNow) {
        this.props.setListFiles(event.target.files);
        if (itemType === "needs") alert(intl.formatMessage({ id: "need.docs.uploaded.onCreate" }));
        document.getElementById("btnUpload").value = "";
      } else {
        this.uploadFiles(event.target.files);
      }
    }
  }

  uploadFiles(files) {
    const { intl, itemId, itemType, multiple, type } = this.props;
    if (!itemId || !itemType || !type || !files) {
      this.props.onChange({ error: <FormattedMessage id="err-" defaultMessage="An error has occured" />, url: "" });
    } else {
      this.setState({ uploading: true }); // setting to true will render a loading wheel + disable the button

      var bodyFormData = new FormData();
      if (multiple) {
        Array.from(files).forEach((file) => {
          bodyFormData.append(type + "[]", file);
        });
      } else {
        bodyFormData.append(type, files[0]);
      }

      var config = {
        headers: { "Content-Type": "multipart/form-data" },
      };

      Api.post("/api/" + itemType + "/" + itemId + "/" + type, bodyFormData, config)
        .then((res) => {
          if (res.status === 200) {
            this.setState({ uploading: false }); // set to false to show user upload has been successful
            if (itemType === "needs") alert(intl.formatMessage({ id: "need.docs.uploaded.onUpdate" }));
            if (res.data.url) {
              this.props.onChange({ error: "", url: res.data.url }); // on success, show new uploading image in the front
            } else if (this.props.refresh !== undefined) {
              this.props.refresh();
            }
          } else {
            this.setState({ uploading: false }); // stop uploading if error
            this.props.onChange({
              error: <FormattedMessage id="err-" defaultMessage="An error has occured" />,
              url: "",
            });
          }
        })
        .catch((error) => {
          this.setState({ uploading: false }); // stop uploading if error
          this.props.onChange({ error: error.response.data.status + " : " + error.response.data.error, url: "" });
        });
    }
  }

  removeImg() {
    const { itemId, itemType, type } = this.props;
    var randomNumber = Math.floor(Math.random() * 12) + 1; // with 12 = max, and 1 = min
    // each static media has a code at the end of url, so making an array of those codes for images 1 to 12
    var imagesCodes = [
      "2e46be09",
      "639bc585",
      "ee2eb97c",
      "71d8e3fb",
      "008eb222",
      "ec11ec0a",
      "81c9a54a",
      "284d4d91",
      "ce49c345",
      "7692ba90",
      "748860b8",
      "0271ffd0",
    ];
    // make logo_url randomly equals to one of our default image (1 to 12)
    const imgUrl =
      type === "avatar" // set imgUrl depending if it's user profile or a banner
        ? `/static/media/default-user-${randomNumber}.${imagesCodes[randomNumber - 1]}.png` // if type is avatar, reset image to a random default user avatar
        : ""; // else make it empty so it shows object default image
    this.setState({ uploading: true }); // setting to true will render a loading wheel + disable the button
    Api.delete("/api/" + itemType + "/" + itemId + "/" + type).then((res) => {
      if (res) {
        this.setState({ uploading: false }); // set to false to show user upload has been successful
        this.props.onChange({ error: "", url: imgUrl }); // set new imgUrl
        $("#imageRemoved").show(); // display success message alert
        setTimeout(function () {
          $("#imageRemoved").hide(300);
        }, 6000); // hide it after 2sec
      }
    });
    // .catch(error=>{});
  }

  validFilesSize(files) {
    const maxSizeFile = this.props.maxSizeFile;
    var nbFilesValid = 0;
    // console.log("Size file : ", files);
    if (files) {
      for (var index = 0; index < files.length; index++) {
        // console.log("Size file : " + files[index].size + " " + maxSizeFile);
        if (files[index].size <= maxSizeFile) {
          nbFilesValid++;
        }
      }
    }
    // console.log(nbFilesValid, files.length);
    return nbFilesValid === files.length ? true : false; // return true or false depending on condition
  }

  // validFilesType(files) {
  //   const fileTypes = this.props.fileTypes;
  //   var nbFilesValid = 0;
  //   if(files){
  //     for(var index = 0; index < files.length; index++){
  //       for(var i = 0; i < fileTypes.length; i++) {
  //         // console.log("Type : " + files[index].type + " ; list type check : " + fileTypes[i]);
  //         if(files[index].type === fileTypes[i]) {
  //           nbFilesValid++;
  //           break;
  //         }
  //       }
  //     }
  //   }
  // 	return (nbFilesValid === files.length) ? true : false // return true or false depending on condition
  // }

  render() {
    const { multiple, text, textUploading, fileTypes, itemType, imageUrl, intl } = this.props;
    const { uploading } = this.state;
    // console.log(imageUrl);
    if (uploading) {
      // if "uploading" true, make button and input disabled (unclickable)
      document.querySelector(".upload-btn-wrapper .btn").disabled = true;
      document.querySelector(".upload-btn-wrapper input").disabled = true;
    }
    return (
      <>
        <div className="upload-btn-wrapper">
          <div className="btn">
            {uploading ? ( // render differently depending on uploading state
              <>
                <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true"></span>
                &nbsp;
                {textUploading}
              </>
            ) : (
              <>
                <i className="far fa-file"></i>
                {text}
              </>
            )}
          </div>
          <input
            id={!uploading ? "btnUpload" : ""}
            type="file"
            accept={fileTypes.join()}
            multiple={multiple ? true : false}
            onChange={this.handleChange}
          />
        </div>
        {/* {(
					(itemType !== "users" && imageUrl !== "") // if it's not a user (so all other object) and has a stored image (not empty)
					|| (itemType === "users" && !imageUrl.includes('/static/')) // OR if it's a user and he has a real stored image (not a default one)
				) && // then show buttn to remove image
					<>
						<div onClick={() => this.removeImg()} id="btnRemove">{intl.formatMessage({ id: "general.removeImage.btn" })}</div>
					</>
				} */}
        {/* <Alert id="imageRemoved" type="success" message={<FormattedMessage id="general.removeImage.success" defaultMessage="Image has been removed. Please save to see changes." />}/> */}
      </>
    );
  }
}
export default injectIntl(BtnUploadFile);
