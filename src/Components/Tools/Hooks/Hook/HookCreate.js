import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { UserContext } from "UserProvider";
import Api from "Api";
import HookForm from "./HookForm";

export default class HookCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: "",
      isCreating: false,
      hook: {
        hook_type: "Slack",
        trigger_need: true,
        trigger_post: true,
        trigger_member: true,
        trigger_project: true,
        hook_params: {
          hook_url: "",
          channel: "",
          username: "",
        },
      },
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  static get defaultProps() {
    return {
      refresh: () => console.log("Missing function"),
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.hook) {
      this.setState({ hook: nextProps.hook });
    }
  }

  handleChange(key, content) {
    var updateHook = this.state.hook;
    updateHook[key] = content;
    // var newHook = {
    // 	"hook_type": updateHook.hook_type,
    // 	"hook_type": updateHook.hook_type,
    // 	"trigger_need": updateHook.trigger_need,
    // 	"trigger_post": updateHook.trigger_post,
    // 	"trigger_member": updateHook.trigger_member,
    // 	"trigger_project": true,
    // 	"hook_params": {
    // 		"hook_url": key === "hook_url" ? content : updateHook.hook_url,
    // 		"channel": key === "hook_channel" ? content : updateHook.hook_channel,
    // 		"username": key === "hook_username" ? content : updateHook.hook_username,
    // 	}
    // }
    // updateHook["hook_params"] = hook_params;

    if (key === "trigger_post") updateHook.trigger_post = !content;
    if (key === "trigger_need") updateHook.trigger_need = !content;
    if (key === "trigger_member") updateHook.trigger_member = !content;
    var hook_params = {
      hook_url: key === "hook_url" ? content : this.state.hook.hook_url,
      channel: key === "channel" ? content : this.state.hook.channel,
      username: key === "username" ? content : this.state.hook.username,
    };
    updateHook["hook_params"] = hook_params;
    // console.log("new hook", newHook);
    this.setState({ hook: updateHook, error: "" });
  }

  handleSubmit() {
    const { projectId } = this.props;
    const { hook } = this.state;
    if (!projectId) {
      // console.log("project_id is missing");
    } else {
      // hook["project_id"] = projectId;
      Api.post(`/api/projects/${projectId}/hooks`, { hook: hook })
        .then((res) => {
          this.refresh();
        })
        .catch((error) => {
          // console.log(error);
        });
    }
  }

  changeDisplay() {
    this.setState({ isCreating: !this.state.isCreating });
  }

  refresh() {
    this.setState({
      isCreating: false,
      hook: {
        content: "",
        end_date: "",
        title: "",
      },
      valid_content: undefined,
      valid_end_date: undefined,
      valid_title: undefined,
    });
    this.props.refresh();
  }

  render() {
    const { error, isCreating, hook } = this.state;
    if (hook === undefined) {
      return "OK";
    } else {
      if (isCreating) {
        return (
          <div className="hookCard isCreating">
            {/* {error &&
              <div className="alert alert-danger" role="alert">
                <FormattedMessage id={"err-"} defaultMessage="An error has occurred" />
              </div>
            } */}
            <HookForm
              action="create"
              cancel={this.changeDisplay.bind(this)}
              hook={hook}
              handleChange={this.handleChange}
              handleSubmit={this.handleSubmit}
            />
          </div>
        );
      } else {
        return (
          <div className="hookCreate justButton">
            <button className="btn btn-primary" onClick={() => this.changeDisplay()}>
              <FormattedMessage id="hook.addHook" defaultMessage="Add a hook" />
            </button>
          </div>
        );
      }
    }
  }
}
HookCreate.contextType = UserContext;
