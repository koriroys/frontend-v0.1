import React, { Component, Fragment } from "react";
import { FormattedMessage } from "react-intl";
import { UserContext } from "UserProvider";
import Api from "Api";
import HookForm from "./HookForm";
import HookDelete from "./HookDelete";
import "./HookCard.scss";

export default class HookCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mode: this.props.mode,
      hook: this.props.hook,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  static get defaultProps() {
    return {
      hook: undefined,
      refresh: () => console.log("Missing function"),
    };
  }

  changeMode(newMode) {
    if (newMode) {
      this.setState({ mode: newMode });
      this.props.refresh();
    }
  }

  cancel() {
    this.setState({ hook: this.props.hook });
    this.changeMode("display");
  }

  handleChange(key, content) {
    var updateHook = this.state.hook;
    updateHook[key] = content;
    var hook_params = {
      hook_url: key === "hook_url" ? content : this.state.hook.hook_params.hook_url,
      channel: key === "channel" ? content : this.state.hook.hook_params.channel,
      username: key === "username" ? content : this.state.hook.hook_params.username,
    };
    updateHook["hook_params"] = hook_params;
    if (key === "trigger_post") updateHook.trigger_post = !content;
    if (key === "trigger_need") updateHook.trigger_need = !content;
    if (key === "trigger_member") updateHook.trigger_member = !content;
    this.setState({ hook: updateHook, error: "" });
  }

  handleSubmit() {
    const { hookProjectId } = this.props;
    const { hook } = this.state;
    this.setState({ uploading: true });
    Api.patch(`/api/projects/${hookProjectId}/hooks/${hook.id}`, { hook: hook }).then((res) => {
      this.changeMode("display");
      this.props.refresh();
    });
  }

  render() {
    const { hookProjectId, refresh } = this.props;
    const { mode, hook } = this.state;
    if (hook === undefined) {
      return null;
    } else {
      return (
        <div className="hookCard">
          {mode === "display" && (
            <>
              <div className="hookContent">
                <div className="hookContent--header">
                  <h6>
                    <FormattedMessage id="hook.parameters.channel" defaultMessage="Channel" />:
                    <span className="channel">{hook.hook_params.channel}</span>
                  </h6>
                  <span className="hookAction">
                    <i className="fa fa-edit" onClick={() => this.changeMode("update")} />
                    <HookDelete hook={hook} hookProjectId={hookProjectId} refresh={refresh} />
                  </span>
                </div>
                <p className="hookContent--triggers">
                  <FormattedMessage id="hook.triggerOn" defaultMessage="Trigger on" />:
                  {hook.trigger_post && <FormattedMessage id="post.posts" defaultMessage="Post" />}
                  {hook.trigger_need && <FormattedMessage id="need.uppercase" defaultMessage="Need" />}
                  {hook.trigger_member && <FormattedMessage id="entity.info.members" defaultMessage="Member" />}
                </p>
              </div>
            </>
          )}
          {mode === "update" && (
            <HookForm
              action="update"
              cancel={() => this.cancel()}
              hook={hook}
              handleChange={this.handleChange}
              handleSubmit={this.handleSubmit}
            />
          )}
        </div>
      );
    }
  }
}
HookCard.contextType = UserContext;
