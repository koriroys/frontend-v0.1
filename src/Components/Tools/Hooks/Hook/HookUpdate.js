import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import Api from "Api";
import HookForm from "./HookForm";

export default class HookUpdate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      description: this.props.description,
      error: "",
      isCreating: false,
      hook: this.props.hook,
      uploading: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  static get defaultProps() {
    return {
      changeMode: () => console.log("Missing function"),
      hook: undefined,
      mode: "",
      refresh: () => console.log("Missing function"),
    };
  }

  componentDidMount() {
    this.setState({ documents: this.props.hook.documents });
  }

  handleChange(key, content) {
    var updateHook = this.state.hook;
    updateHook[key] = content;
    this.setState({ hook: updateHook, error: "" });
  }

  handleChangeDoc(documents) {
    this.handleChange("documents", documents);
  }

  handleSubmit() {
    const { hook } = this.state;
    this.setState({ uploading: true });
    Api.patch("/api/hooks/" + hook.id, { hook: hook })
      .then((res) => {
        this.refresh();
      })
      .catch((error) => {
        this.setState({
          uploading: false,
          error: <FormattedMessage id="err-" defaultMessage="An error has occured" />,
        });
      });
  }

  refresh() {
    this.setState({
      isCreating: false,
      hook: undefined,
      valid_content: undefined,
      valid_skills: undefined,
      valid_ressources: undefined,
      valid_title: undefined,
    });
    this.props.refresh();
    this.props.changeMode("card");
  }

  render() {
    const { hook, refresh } = this.state;
    // put hook card back to different state depending if it's in an object, or as a single hook page
    const cancelMode = this.props.cancelMode ? "details" : "card";
    if (hook === undefined) {
      return null;
    } else {
      return (
        <div className="hookUpdate">
          {this.state.error && (
            <div className="alert alert-danger" role="alert">
              <FormattedMessage id={"err-"} defaultMessage="An error has occurred" />
            </div>
          )}
          <HookForm
            action="update"
            cancel={() => this.props.changeMode(cancelMode)}
            hook={hook}
            handleChange={this.handleChange}
            handleSubmit={this.handleSubmit}
            uploading={this.state.uploading}
          />
        </div>
      );
    }
  }
}
