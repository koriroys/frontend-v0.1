import React, { Component, useLayoutEffect } from "react";
import { FormattedMessage } from "react-intl";
import TitleInfo from "Components/Tools/TitleInfo";
import ReactQuill, { Quill } from "react-quill";
import { ImageDrop } from "quill-image-drop-module";
import ImageResize from "quill-image-resize-module-react";
import quillEmoji from "quill-emoji";
import BlotFormatter from "quill-blot-formatter";
import "quill-emoji/dist/quill-emoji.css";
import "react-quill/dist/quill.snow.css";
import "./FormTextAreaComponent.scss";
import "quill-mention";
import "quill-mention/dist/quill.mention.css";
import algoliasearch from "algoliasearch";
import defaultProjectImg from "assets/img/default/default-project.jpg";

Quill.register("modules/imageDrop", ImageDrop);
Quill.register("modules/imageResize", ImageResize);
Quill.register(
  {
    // emojis support
    "formats/emoji": quillEmoji.EmojiBlot,
    "modules/emoji-shortname": quillEmoji.ShortNameEmoji,
    "modules/emoji-toolbar": quillEmoji.ToolbarEmoji,
    "modules/emoji-textarea": quillEmoji.TextAreaEmoji,
  },
  true
);
Quill.register("modules/blotFormatter", BlotFormatter);

export default class FormWysiwygComponent extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  modules = {
    toolbar: [
      [{ header: 1 }, { header: 2 }],
      ["bold", "italic", "underline", "strike"],
      [{ color: [] }, { background: [] }],
      [{ align: [] }],
      [{ script: "sub" }, { script: "super" }],
      ["code-block", "blockquote"],
      [{ list: "ordered" }, { list: "bullet" }, { indent: "-1" }, { indent: "+1" }],
      ["link", "image", "video"],
      ["clean"],
      ["emoji"], // emoji support
    ],
    "emoji-toolbar": true,
    "emoji-textarea": true,
    "emoji-shortname": true, // emoji support
    imageDrop: true, // ability to drop image directly in the editor
    blotFormatter: {},
    mention: {
      // quill-mention features
      allowedChars: /^[A-Za-z\sÅÄÖåäö]*$/, // allowed characters
      mentionDenotationChars: ["@", "#"], // characters that triggers source function
      linkTarget: "_blank",
      source: function (searchTerm, renderList, mentionChar) {
        // function triggered when typing one of mentionDenotationChars
        var appId = process.env.REACT_APP_ALGOLIA_APP_ID; // algolia app id
        var token = process.env.REACT_APP_ALGOLIA_TOKEN; // algolia token
        var client = algoliasearch(appId, token); // initialize algolia client
        var index = mentionChar === "@" ? client.initIndex("User") : client.initIndex("Project"); // set client index depending on mention character
        index
          .search(searchTerm, {
            hitsPerPage: 10,
          })
          .then((content) => {
            var objectsList =
              mentionChar === "@"
                ? // if mentionChar is @ (user), render a user specific object
                  content.hits.map((obj) => ({
                    value: obj.first_name + " " + obj.last_name, // "value" is needed by quill-mention, it's the text that will be displayed after selecting the mention. Here it's user full name
                    name: obj.nickname, // not required by quill-mention but we set "name" so it can be displayed in the mention list (renderItem function). Here it's user nickname
                    link: `/user/${obj.objectID}`, // "link" is required by quill-mention to englobe "value" around a link automatically. Here it's relative link to the user
                    imageUrl: obj.logo_url_sm, // not required by quill-mention but we set "imageUrl" so it can be displayed in the mention list (renderItem function). Here it's user profile image
                  }))
                : // else render project specific object
                  content.hits.map((obj) => ({
                    value: obj.title, // project title
                    name: obj.short_title, // project short yiyle
                    link: `/project/${obj.objectID}`, // relative link to the project
                    imageUrl: obj.banner_url ? obj.banner_url : defaultProjectImg, // user profile image // project banner image
                  }));
            renderList(objectsList); // quill-mention function that renders the list of matching mentions
          });
      },
      renderItem: function (item) {
        // quill-mention function that gives control over how matching mentions from source are displayed within the list
        return `
					<div class="cql-list-item-inner">
						<img src="${item.imageUrl}"/>
						${item.value} <span>${item.name}</span>
					</div>`;
      },
    },
  };

  formats = [
    "header",
    "align",
    "color",
    "background",
    "script",
    "bold",
    "italic",
    "underline",
    "strike",
    "blockquote",
    "list",
    "bullet",
    "indent",
    "link",
    "image",
    "code-block",
    "span",
    "video",
    "emoji",
    "mention",
    "alt",
    "width",
    "height",
    "style",
  ];

  static get defaultProps() {
    return {
      id: "default",
      title: "Title",
      content: "",
      show: false,
      onChange: (value) => console.log("onChange doesn't exist to update " + value),
      placeholder: "",
      colSizeTitle: "col-12 col-sm-3",
      colSizeContent: "col-12 col-sm-9",
      row: 3,
      maxChar: undefined,
      mandatory: false,
      beHide: false,
    };
  }

  handleChange(event) {
    // console.log("EVENT : ", event);
    this.props.onChange(this.props.id, event);
  }
  renderBeHide(id, beHide) {
    if (beHide) {
      return (
        <div className="form-group form-check">
          <input type="checkbox" className="form-check-input" id={"show" + id} />
          <label className="form-check-label" htmlFor={"show" + id}>
            <FormattedMessage id="user.profile.edit.public" defaultMessage="Show to public" />
          </label>
        </div>
      );
    } else {
      return null;
    }
  }

  renderPrepend(prepend) {
    if (prepend) {
      return (
        <div className="input-group-prepend">
          <div className="input-group-text">{prepend}</div>
        </div>
      );
    } else {
      return null;
    }
  }

  render() {
    const { title, id, content, show, placeholder, colSizeTitle, colSizeContent, mandatory } = this.props;

    return (
      <div className="form-row formTextArea">
        <TitleInfo title={title} classSize={colSizeTitle} mandatory={mandatory} />
        <div className={colSizeContent}>
          {!show && ( // only show button if prop is true
            <a
              className="btn btn-primary"
              data-toggle="collapse"
              href={`#${id}`}
              role="button"
              aria-expanded="false"
              aria-controls="collapseExample"
            >
              <FormattedMessage id="entity.form.btnAdmin" defaultMessage="Edit" />
            </a>
          )}
          <div className={show ? "" : "collapse"} id={id}>
            <ReactQuill
              formats={this.formats}
              modules={this.modules}
              onChange={this.handleChange}
              placeholder={placeholder}
              style={{ width: "100%", paddingBottom: "20px" }}
              theme="snow"
              value={content === null ? "" : content}
            />
          </div>
        </div>
      </div>
    );
  }
}
