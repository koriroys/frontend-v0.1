import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import Alert from "Components/Tools/Alert";
import BtnUploadFile from "Components/Tools/BtnUploadFile";
import TitleInfo from "Components/Tools/TitleInfo";
import "./FormImgComponent.scss";

export default class FormImgComponent extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      error: "",
      imageUrl: this.props.imageUrl,
      uploading: false,
    };
  }

  static get defaultProps() {
    return {
      colSizeBtn: "col-12 col-sm-2",
      colSizePreview: "col-12 col-sm-7",
      colSizeTitle: "col-12 col-sm-3",
      defaultImg: "",
      id: "",
      imageUrl: "",
      itemId: "",
      itemType: "",
      mandatory: false,
      maxSizeFile: 3145728,
      onChange: (value) => console.log("onChange doesn't exist to update " + value),
      placeholder: "",
      title: "Title",
      type: "",
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ imageUrl: nextProps.imageUrl });
  }

  handleChange(result) {
    if (this.props.id !== "") {
      if (result.error !== "") {
        this.setState({ error: result.error });
      } else {
        this.setState({ error: "" });
        if (result.url !== "") {
          this.props.onChange(this.props.id, result.url);
        }
      }
    } else {
      console.log("id is missing (key to set)");
    }
  }

  render() {
    const {
      colSizeTitle,
      colSizePreview,
      colSizeBtn,
      defaultImg,
      itemId,
      itemType,
      mandatory,
      title,
      type,
    } = this.props;
    const { error, imageUrl, uploading } = this.state;

    var imgTodisplay = defaultImg;
    if (imageUrl !== "" && imageUrl !== null) {
      imgTodisplay = imageUrl;
    }

    const bgImg = {
      backgroundImage: "url(" + imgTodisplay + ")",
      backgroundSize: "cover",
      backgroundPosition: "center",
    };

    return (
      <div className="row formImg">
        {/* <div className="form-row formImg"> */}
        <TitleInfo title={title} classSize={colSizeTitle} mandatory={mandatory} />
        <div className={colSizePreview + " preview"}>
          <div className={type + " center"} style={bgImg}></div>
        </div>
        <div className={colSizeBtn + " align-self-center btnUploadZone"}>
          {error !== "" && <Alert type="danger" message={error} />}
          {uploading ? (
            <div className="spinner-border text-secondary" role="status">
              <span className="sr-only">
                <FormattedMessage id="general.loading" />
              </span>
            </div>
          ) : (
            <BtnUploadFile
              uploadNow={true}
              fileTypes={["image/jpeg", "image/png", "image/jpg"]}
              itemId={itemId}
              itemType={itemType}
              type={type}
              maxSizeFile={4194304}
              imageUrl={imageUrl}
              multiple={false}
              text={<FormattedMessage id="info-1002" defaultMessage="Choose a file" />}
              onChange={this.handleChange}
            />
          )}
        </div>
      </div>
    );
  }
}
