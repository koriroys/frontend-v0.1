import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import TitleInfo from "Components/Tools/TitleInfo";
import "./FormInterestsComponent.scss";
import $ from "jquery";

export default class FormInterestsComponent extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  static get defaultProps() {
    return {
      beHide: false,
      colSizeTitle: "col-12 col-sm-3",
      colSizeContent: "col-12 col-sm-9",
      content: [],
      errorCodeMessage: "",
      id: "default",
      isValid: undefined,
      mandatory: false,
      onChange: (value) => console.log("onChange doesn't exist to update " + value),
      title: "Title",
      showAll: false,
    };
  }

  handleChange(event) {
    var actualContent = this.props.content;
    var interestSelected = Number(event.target.id.split("-")[1]);
    const elementClicked = actualContent.indexOf(interestSelected);
    if (elementClicked >= 0) {
      actualContent.splice(elementClicked, 1);
    } else {
      actualContent.push(interestSelected);
      actualContent.sort((a, b) => {
        return a - b;
      });
    }
    this.props.onChange("interests", actualContent);
  }

  renderBeHide(id, beHide) {
    if (beHide) {
      return (
        <div className="form-group form-check">
          <input type="checkbox" className="form-check-input" id={"show" + id} />
          <label className="form-check-label" htmlFor={"show" + id}>
            <FormattedMessage id="user.profile.edit.public" defaultMessage="Show to public" />
          </label>
        </div>
      );
    } else {
      return null;
    }
  }

  renderImgInterest(content, interest) {
    var userLang = navigator.language || navigator.userLanguage;
    var path =
      (localStorage.getItem("language") && localStorage.getItem("language") === "fr") ||
      (!localStorage.getItem("language") && (userLang === "fr-FR" || userLang === "fr"))
        ? "interests/fr" // if website or browser is in french
        : "interests"; // if english
    if (
      content.find((element) => {
        return element === interest;
      })
    ) {
      return (
        <img
          src={require("assets/img/" + path + "/Interest-" + interest + ".png")}
          className={"imgInterest"}
          alt={"Interest-" + interest}
        />
      );
    } else {
      return (
        <img
          src={require("assets/img/" + path + "/Interest-invert-" + interest + ".png")}
          className={"imgInterest"}
          alt={"Interest-" + interest}
        />
      );
    }
  }

  showMore(items) {
    $(".less").show();
    $(".interest:lt(" + items + ")").show(300);
    $(".more").hide();
  }

  showLess() {
    $(".interest").not(":lt(8)").hide(300);
    $(".more").show();
    $(".less").hide();
  }

  render() {
    const {
      beHide,
      colSizeContent,
      colSizeTitle,
      errorCodeMessage,
      id,
      isValid,
      mandatory,
      title,
      showAll,
    } = this.props;

    var content = this.props.content.map((interest) => {
      if (interest.id) {
        return interest.id;
      } else {
        return interest;
      }
    });

    const defaultInterests = [];
    for (var i = 1; i < 18; i++) {
      defaultInterests.push(i);
    }
    var interestClass = "";

    return (
      <div className="form-row formInterests">
        <TitleInfo title={title} classSize={colSizeTitle} mandatory={mandatory} />
        <div className={colSizeContent + " content"} id="interests">
          <div
            className={`form-row ${showAll && "showAll"}`}
            style={isValid !== undefined ? (isValid ? {} : { border: "1px solid red" }) : {}}
          >
            {defaultInterests.map((interest, index) => {
              interestClass = index < 8 || showAll ? "interest" : "interest toggle";
              return (
                <div className={`col-4 col-sm-4 col-lg-3 col-xl-2 ${interestClass}`} key={index}>
                  <input
                    id={"interest-" + interest}
                    className="interestElement"
                    type="checkbox"
                    checked={
                      content.find((element) => {
                        return element === interest;
                      })
                        ? true
                        : false
                    }
                    onChange={this.handleChange}
                  />
                  <label htmlFor={"interest-" + interest}>{this.renderImgInterest(content, interest)}</label>
                </div>
              );
            })}
            <div className="more" onClick={() => this.showMore(defaultInterests.length)}>
              <FormattedMessage id="general.showmore" defaultMessage="Show More" />
            </div>
            <div className="less" onClick={() => this.showLess()}>
              <FormattedMessage id="general.showless" defaultMessage="Show Less" />
            </div>
          </div>
          {errorCodeMessage && (
            <div className="invalid-feedback" style={{ display: "inline" }}>
              <FormattedMessage id={errorCodeMessage} defaultMessage="Value is not valid" />
            </div>
          )}
          {this.renderBeHide(id, beHide)}
        </div>
      </div>
    );
  }
}
