import React, { Component } from "react";
import { FormattedMessage, injectIntl } from "react-intl";
import TitleInfo from "Components/Tools/TitleInfo";
import { applyPattern } from "Components/Tools/Forms/FormChecker";
import "./FormSelectComponent.scss";
import $ from "jquery";

class FormSelectComponent extends Component {
  static get defaultProps() {
    return {
      beHide: false,
      colSizeContent: "col-12 col-sm-9",
      colSizeTitle: "col-12 col-sm-3",
      content: "",
      errorCodeMessage: "",
      id: "default",
      isValid: undefined,
      mandatory: false,
      maxChar: undefined,
      onChange: (value) => console.log("onChange doesn't exist to update " + value),
      pattern: undefined,
      placeholder: "",
      title: "Title",
      type: "text",
    };
  }

  handleChange(event) {
    // console.log(event);
    var { id, value } = event.target;
    const { content, pattern } = this.props;
    value = applyPattern(value, content, pattern);
    this.props.onChange(id, value);
  }

  componentDidMount() {
    if (this.props.content) {
      // select current category of user
      $("#category").val(this.props.content);
    }
  }

  renderBeHide(id, beHide) {
    if (beHide) {
      return (
        <div className="form-group form-check">
          <input type="checkbox" className="form-check-input" id={"show" + id} pattern="" />
          <label className="form-check-label" htmlFor={"show" + id}>
            <FormattedMessage id="user.profile.edit.public" defaultMessage="Show to public" />
          </label>
        </div>
      );
    } else {
      return null;
    }
  }

  renderPrepend(prepend) {
    if (prepend) {
      return (
        <div className="input-group-prepend">
          <div className="input-group-text">{prepend}</div>
        </div>
      );
    } else {
      return null;
    }
  }

  render() {
    const {
      colSizeTitle,
      colSizeContent,
      // content,
      errorCodeMessage,
      // id,
      // isValid,
      mandatory,
      // placeholder,
      prepend,
      // type,
      title,
      intl,
    } = this.props;

    return (
      <div className="form-row formSelect">
        <TitleInfo title={title} classSize={colSizeTitle} mandatory={mandatory} />
        <div className={colSizeContent + " content"}>
          <div className="input-group">
            {this.renderPrepend(prepend)}
            <div className="form-group">
              <select className="form-control" id="category" onChange={this.handleChange.bind(this)}>
                <option value="default" key="default-index">
                  {intl.formatMessage({ id: "user.profile.edit.select.default" })}
                </option>
                }
                <option value="fulltime_worker" key="fulltime_worker-index">
                  {intl.formatMessage({ id: "user.profile.edit.select.fulltime_worker" })}
                </option>
                }
                <option value="parttime_worker" key="parttime_worker-index">
                  {intl.formatMessage({ id: "user.profile.edit.select.parttime_worker" })}
                </option>
                }
                <option value="fulltime_student" key="fulltime_student-index">
                  {intl.formatMessage({ id: "user.profile.edit.select.fulltime_student" })}
                </option>
                }
                <option value="parttime_student" key="parttime_student-index">
                  {intl.formatMessage({ id: "user.profile.edit.select.parttime_student" })}
                </option>
                }
                <option value="retired" key="retired-index">
                  {intl.formatMessage({ id: "user.profile.edit.select.retired" })}
                </option>
                }
                <option value="between_jobs" key="between_jobs-index">
                  {intl.formatMessage({ id: "user.profile.edit.select.between_jobs" })}
                </option>
                }
                {/* <option value="forprofit" key="forprofit-index">{intl.formatMessage({id:'user.profile.edit.select.forprofit'})}</option>}
							  <option value="nonprofit" key="nonprofit-index">{intl.formatMessage({id:'user.profile.edit.select.nonprofit'})}</option>} */}
              </select>
            </div>
            {errorCodeMessage && (
              <div className="invalid-feedback">
                <FormattedMessage id={errorCodeMessage} defaultMessage="Value is not valid" />
              </div>
            )}
          </div>
          {/* this.renderBeHide(id, beHide) */}
        </div>
      </div>
    );
  }
}
export default injectIntl(FormSelectComponent);
