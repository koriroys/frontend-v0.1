import React, { Component } from "react";
import { FormattedMessage, injectIntl } from "react-intl";
import { MentionsInput, Mention } from "react-mentions";
import defaultProjectImg from "assets/img/default/default-project.jpg";
import algoliasearch from "algoliasearch";
import "./PostInputMentions.scss";

class PostInputMentions extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.placeholder = [["post"], ["What's on your mind?"]];
  }

  static get defaultProps() {
    return {
      onChange: (value) => console.log("onChange doesn't exist to update " + value),
      content: "",
    };
  }

  handleChange(event) {
    this.props.onChange(event.target.value);
  }

  searchAlgoliaObjects(objType, query, callback) {
    var appId = process.env.REACT_APP_ALGOLIA_APP_ID; // algolia app id
    var token = process.env.REACT_APP_ALGOLIA_TOKEN; // algolia token
    var client = algoliasearch(appId, token); // initialize algolia client
    var index = objType === "user" ? client.initIndex("User") : client.initIndex("Project"); // set client index depending on objType
    index
      .search(query, {
        hitsPerPage: 10,
      })
      .then((content) => {
        const resultFormat =
          objType === "user"
            ? // if objType is user, render a user specific object
              content.hits.map((obj) => ({
                id: obj.objectID, // user id
                display: obj.first_name + " " + obj.last_name, // user full name
                name: obj.nickname, // user nickname
                imageUrl: obj.logo_url_sm, // user profile image
              }))
            : // else render project specific object
              content.hits.map((obj) => ({
                id: obj.objectID, // project id
                display: obj.title, // project title
                name: obj.short_title, // project short title
                imageUrl: obj.banner_url_sm ? obj.banner_url_sm : defaultProjectImg, // project banner image
              }));
        return resultFormat;
      })
      .then(callback)
      .catch((err) => {
        // console.log(err);
      });
  }

  renderSuggestion(objType, suggestion) {
    var type = objType === "user" ? "@" : "#";
    return (
      <div className="suggestion-item">
        <img src={suggestion.imageUrl} />
        {suggestion.display}
        <span>{type + suggestion.name}</span>
      </div>
    );
  }

  render() {
    const inputId = this.props.placeholder[0] + ".placeholder";
    var { intl } = this.props;
    return (
      <div className="postInputMentions">
        <MentionsInput
          value={this.props.content}
          onChange={this.handleChange}
          className="suggestionsContainer"
          displayTransform={(id, display) => {
            return display; // return user full name, or entity title
          }}
          markup="((__type__:__id__:__display__))"
          placeholder={intl.formatMessage({ id: inputId })}
        >
          <Mention
            type="@"
            trigger="@"
            renderSuggestion={this.renderSuggestion.bind(this, "user")}
            data={this.searchAlgoliaObjects.bind(this, "user")}
          />
          <Mention
            type="#"
            trigger="#"
            renderSuggestion={this.renderSuggestion.bind(this, "project")}
            data={this.searchAlgoliaObjects.bind(this, "project")}
          />
        </MentionsInput>
      </div>
    );
  }
}
export default injectIntl(PostInputMentions);
