import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import Chips from "react-chips";
import "./FormRessourcesComponent.scss";
import TitleInfo from "Components/Tools/TitleInfo";
import algoliasearch from "algoliasearch/lite";
// import { InstantSearch, SearchBox, Hits } from 'react-instantsearch-dom';

export default class FormRessourcesComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      placeholder: this.props.placeholder,
    };
  }

  static get defaultProps() {
    return {
      beHide: false,
      colSizeTitle: "col-12 col-sm-3",
      colSizeContent: "col-12 col-sm-9",
      content: [],
      errorCodeMessage: "",
      id: "ressources",
      isValid: undefined,
      mandatory: false,
      type: "default",
      onChange: (value) => console.log("onChange doesn't exist to update " + value),
      placeholder: "",
      title: "Title",
    };
  }

  handleChange(event) {
    this.props.onChange(this.props.id, event);
    this.setState({ placeholder: "" });
  }

  renderBeHide(id, beHide) {
    if (beHide) {
      return (
        <div className="form-group form-check">
          <label className="form-check-label" htmlFor={"show" + id}>
            <FormattedMessage
              id="help.ressources"
              defaultMessage="Type your ressources in the box bellow, to validate use Tab or Enter."
            />
          </label>
          <input type="checkbox" className="form-check-input" id={"show" + id} />
          <label className="form-check-label" htmlFor={"show" + id}>
            <FormattedMessage id="user.profile.edit.public" defaultMessage="Show to public" />
          </label>
        </div>
      );
    } else {
      return null;
    }
  }

  render() {
    var appId = process.env.REACT_APP_ALGOLIA_APP_ID;
    var token = process.env.REACT_APP_ALGOLIA_TOKEN;

    var client = algoliasearch(appId, token);
    var index = client.initIndex("Ressource"); // Ressource
    var algoliaRessources = [];

    function fetchAlgolia(resolve, value) {
      index
        .search(value, {
          attributesToRetrieve: ["ressource_name"], // ressource_name
          hitsPerPage: 5,
        })
        .then((content) => {
          if (content) {
            algoliaRessources = content.hits;
          } else {
            algoliaRessources = [""];
          }
          algoliaRessources = algoliaRessources.map((ressource) => ressource["ressource_name"]); // ressource_name
          algoliaRessources.unshift(value);
          resolve(algoliaRessources);
        });
    }

    const { beHide, colSizeContent, colSizeTitle, errorCodeMessage, id, mandatory, title, type } = this.props;

    var content = this.props.content.map((ressource) => {
      if (ressource.ressource_name) {
        return ressource.ressource_name;
      } else {
        return ressource;
      }
    });

    var placeholder = content.length === 0 ? this.state.placeholder : "";

    return (
      <div className="form-row formRessources">
        <TitleInfo title={title} classSize={colSizeTitle} mandatory={mandatory} />

        <div className={colSizeContent + " content ressource-container"} id="ressources">
          <label className="form-check-label" htmlFor={"show" + id}>
            <FormattedMessage
              id={"help.ressources." + type}
              defaultMessage="Type your ressources in the box bellow, to validate use Tab, Enter or comma."
            />
          </label>
          <Chips
            id={id}
            value={content}
            onChange={this.handleChange.bind(this)}
            placeholder={placeholder}
            createChipKeys={["Enter", "Tab", ","]}
            fetchSuggestions={(value) => {
              return new Promise((resolve) => {
                fetchAlgolia(resolve, value);
              });
            }}
          />
          {errorCodeMessage && (
            <div className="invalid-feedback" style={{ display: "inline", fontSize: "80%" }}>
              <FormattedMessage id={errorCodeMessage} defaultMessage="Value is not valid" />
            </div>
          )}
          {this.renderBeHide(id, beHide)}
        </div>
      </div>
    );
  }
}
