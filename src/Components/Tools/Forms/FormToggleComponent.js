import React, { Component } from "react";
import TitleInfo from "Components/Tools/TitleInfo";
import { injectIntl } from "react-intl";
import "./FormToggleComponent.scss";

class FormToggleComponent extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  static get defaultProps() {
    return {
      id: "default",
      title: "Title",
      onChange: (value) => console.log("onChange doesn't exist to update " + value),
      colSizeTitle: "col-12 col-sm-3",
      colSizeContent: "col-12 col-sm-9",
      isChecked: false,
      choice1: "Choice 1",
      choice2: "Choice 2",
      colorChoice1: "#ccc",
      colorChoice2: "#2196F3",
    };
  }

  handleChange(event) {
    this.props.onChange(event.target.id, event.target.checked);
  }

  render() {
    const {
      id,
      title,
      colSizeTitle,
      colSizeContent,
      isChecked,
      choice1,
      choice2,
      colorChoice1,
      colorChoice2,
      intl,
      warningMsgId,
    } = this.props;

    return (
      <div className="form-row formToggle">
        <TitleInfo title={title} classSize={colSizeTitle} />
        <div className={colSizeContent + " content"}>
          {warningMsgId && <p>{intl.formatMessage({ id: warningMsgId })}</p>}
          <div className="toggle">
            <span className={"choice left " + (isChecked ? " selected" : "")}>{choice1}</span>
            <label className="switch">
              <input id={id} type="checkbox" checked={isChecked} onChange={this.handleChange} />
              <span
                className="slider round"
                style={{ backgroundColor: isChecked ? colorChoice2 : colorChoice1 }}
              ></span>
            </label>
            <span className={"choice" + (!isChecked ? " selected" : "")}>{choice2}</span>
          </div>
        </div>
      </div>
    );
  }
}
export default injectIntl(FormToggleComponent);
