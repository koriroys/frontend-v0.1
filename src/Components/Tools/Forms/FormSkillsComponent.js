import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import Chips from "react-chips";
import "./FormSkillsComponent.scss";
import TitleInfo from "Components/Tools/TitleInfo";
import algoliasearch from "algoliasearch/lite";
// import { InstantSearch, SearchBox, Hits } from 'react-instantsearch-dom';

export default class FormSkillsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      placeholder: this.props.placeholder,
    };
  }

  static get defaultProps() {
    return {
      beHide: false,
      colSizeTitle: "col-12 col-sm-3",
      colSizeContent: "col-12 col-sm-9",
      content: [],
      errorCodeMessage: "",
      id: "skills",
      isValid: undefined,
      mandatory: false,
      type: "default",
      onChange: (value) => console.log("onChange doesn't exist to update " + value),
      placeholder: "",
      title: "Title",
    };
  }

  handleChange(event) {
    this.props.onChange(this.props.id, event);
    this.setState({ placeholder: "" });
  }

  renderBeHide(id, beHide) {
    if (beHide) {
      return (
        <div className="form-group form-check">
          <label className="form-check-label" htmlFor={"show" + id}>
            <FormattedMessage
              id="help.skills"
              defaultMessage="Type your skills in the box bellow, to validate use Tab or Enter."
            />
          </label>
          <input type="checkbox" className="form-check-input" id={"show" + id} />
          <label className="form-check-label" htmlFor={"show" + id}>
            <FormattedMessage id="user.profile.edit.public" defaultMessage="Show to public" />
          </label>
        </div>
      );
    } else {
      return null;
    }
  }

  render() {
    // const language = getLanguage();

    // function getLanguage() {
    //     let lang;
    //     console.log("Local Language stored:", localStorage.getItem("language"));
    //     if(localStorage.getItem("language") !== null) {
    //         lang = localStorage.getItem("language");
    //     } else {
    //         lang = navigator.language.split(/[-_]/)[0];
    //     }
    //     return lang;
    // }

    // var addText = language === "fr" ? "Ajouter" : "Add"

    var appId = process.env.REACT_APP_ALGOLIA_APP_ID;
    var token = process.env.REACT_APP_ALGOLIA_TOKEN;

    var client = algoliasearch(appId, token);
    var index = client.initIndex("Skill"); // Skill
    var algoliaSkills = [];

    function fetchAlgolia(resolve, value) {
      index
        .search(value, {
          attributesToRetrieve: ["skill_name"], // skill_name
          hitsPerPage: 5,
        })
        .then((content) => {
          if (content) {
            algoliaSkills = content.hits;
          } else {
            algoliaSkills = [""];
          }
          algoliaSkills = algoliaSkills.map((skill) => skill["skill_name"]); // skill_name
          algoliaSkills.unshift(value);
          resolve(algoliaSkills);
        });
    }

    const { beHide, colSizeContent, colSizeTitle, errorCodeMessage, id, mandatory, title, type } = this.props;

    var content = this.props.content.map((skill) => {
      if (skill.skill_name) {
        return skill.skill_name;
      } else {
        return skill;
      }
    });

    // function renderChips () {
    // 	console.log("poporipo");
    // 	return this.props.value.map((chip, idx) => {
    // 		return (
    // 			React.cloneElement(this.props.renderChip(chip, this.props.chipTheme), {
    // 				selected: this.state.chipSelected && idx === this.props.value.length - 1,
    // 				onRemove: this.removeChip(idx),
    // 				index: idx,
    // 				key: `chip${idx}`,
    // 			})
    // 		);
    // 	});
    // }

    // show placeholder only if field is empty
    var placeholder = content.length === 0 ? this.state.placeholder : "";

    return (
      <div className="form-row formSkills">
        <TitleInfo title={title} classSize={colSizeTitle} mandatory={mandatory} />

        <div className={colSizeContent + " content skill-container"} id="skills">
          <label className="form-check-label" htmlFor={"show" + id}>
            <FormattedMessage
              id={"help.skills." + type}
              defaultMessage="Type your skills in the box bellow, to validate use Tab, Enter or comma."
            />
          </label>
          <Chips
            id={id}
            value={content}
            // renderChips={this.renderChips}
            onChange={this.handleChange.bind(this)}
            placeholder={placeholder}
            createChipKeys={["Enter", "Tab", ","]}
            fetchSuggestions={(value) => {
              return new Promise((resolve) => {
                fetchAlgolia(resolve, value);
              });
            }}
          />
          {errorCodeMessage && (
            <div className="invalid-feedback" style={{ display: "inline", fontSize: "80%" }}>
              <FormattedMessage id={errorCodeMessage} defaultMessage="Value is not valid" />
            </div>
          )}
          {this.renderBeHide(id, beHide)}
        </div>
      </div>
    );
  }
}
