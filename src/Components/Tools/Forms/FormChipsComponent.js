import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import Chips from "react-chips";
import CustomChip from "Components/Tools/CustomChip";
import TitleInfo from "Components/Tools/TitleInfo";
import "./FormChipsComponent.scss";

export default class FormChipsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: this.props.content,
      placeholder: this.props.placeholder,
    };
    this.handleChange = this.handleChange.bind(this);
  }

  static get defaultProps() {
    return {
      id: "Chips",
      title: "Title",
      content: [],
      suggests: [],
      onChange: (value) => console.log("onChange doesn't exist to update " + value),
      placeholder: "",
      colSizeTitle: "col-12 col-sm-3",
      colSizeContent: "col-12 col-sm-9",
      maxChips: null,
      prepend: "",
      mandatory: false,
      beHide: false,
    };
  }

  handleChange(event) {
    const nbChips = event.length;
    if (this.props.maxChips !== null && nbChips <= this.props.maxChips) {
      this.setState({ content: event });
      this.props.onChange(this.props.id, event);
    }
    this.setState({ placeholder: "" });
  }

  renderBeHide(id, beHide) {
    if (beHide) {
      return (
        <div className="form-group form-check">
          <input type="checkbox" className="form-check-input" id={"show" + id} />
          <label className="form-check-label" htmlFor={"show" + id}>
            <FormattedMessage id="user.profile.edit.public" defaultMessage="Show to public" />
          </label>
        </div>
      );
    } else {
      return null;
    }
  }

  render() {
    const { id, title, colSizeTitle, colSizeContent, prepend, mandatory, beHide } = this.props;

    const { content } = this.state;

    var placeholder = content.length === 0 ? this.state.placeholder : "";

    return (
      <div className="row formChipsComponent">
        <TitleInfo title={title} classSize={colSizeTitle} mandatory={mandatory} />
        <div className={colSizeContent + " content hashtags-container"}>
          <label className="form-check-label" htmlFor={"show" + id}>
            <FormattedMessage id="help.hashtags" />
          </label>
          <Chips
            id={id}
            value={content}
            onChange={this.handleChange}
            placeholder={placeholder}
            createChipKeys={["Enter", "Tab", ",", 32]}
            renderChip={(value) => <CustomChip prepend={prepend} value={value} />}
          />
          {this.renderBeHide(id, beHide)}
        </div>
      </div>
    );
  }
}
