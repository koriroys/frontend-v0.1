import React, { Component, Fragment } from "react";
import { FormattedMessage } from "react-intl";
import Api from "Api";
import $ from "jquery";
import "./DropdownRole.scss";

export default class DropdownRole extends Component {
  constructor(props) {
    super(props);
    this.state = {
      actualRole: this.props.actualRole,
      sending: false,
    };

    this.handleChange = this.handleChange.bind(this);
  }

  static get defaultProps() {
    return {
      actualRole: "member",
      callBack: () => {
        console.log("Missing callback");
      },
      itemId: undefined,
      itemType: undefined,
      listRole: [],
      member: undefined,
    };
  }

  componentWillReceiveProps(nextProps) {
    const actualRole = nextProps.actualRole;
    this.setState({ actualRole });
  }

  handleChange(role) {
    // console.log("Role : ", role);
    const { actualRole, itemId, itemType, listRole, member } = this.props;
    if (!itemId || !itemType || !member.id) {
      // console.log("Error : missed props");
      // console.log("- actualRole : ", actualRole);
      // console.log("- itemId : ", itemId);
      // console.log("- itemType : ", itemType);
      // console.log("- listRole : ", listRole);
      // console.log("- memberId : ", memberId);
    } else {
      // console.log("Role : ", role);
      const jsonToSend = {
        user_id: member.id,
        previous_role: actualRole,
        new_role: role.toLowerCase(),
      };
      // console.log(jsonToSend);
      this.setState({ sending: true });
      Api.post("/api/" + this.props.itemType + "/" + this.props.itemId + "/members", jsonToSend)
        .then((res) => {
          // console.log("ROLE CHANGED");
          this.setState({ sending: false });
          this.props.callBack();
          $(".roleChangedMessage.member" + member.id).show(); // display changed role confirm message
          setTimeout(function () {
            $(".roleChangedMessage.member" + member.id).hide(700);
          }, 3000);
        })
        .catch((error) => {
          this.setState({ sending: false });
          // console.log(error);
        });
    }
  }

  render() {
    const { actualRole, sending } = this.state;
    const { listRole } = this.props;
    var roleSelected = listRole.find((role) => role === actualRole);
    return (
      <div className="dropdownRole btn-group">
        <button
          type="button"
          className="btn btn-outline-primary dropdown-toggle"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="false"
        >
          {sending && (
            <Fragment>
              <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true"></span>
              &nbsp;
            </Fragment>
          )}
          <FormattedMessage id={"member.role." + roleSelected} defaultMessage={roleSelected} />
        </button>
        <div className="dropdown-menu">
          {listRole.map((role, index) => {
            return (
              <FormattedMessage id={"member.role." + role} key={index} defaultMessage={role}>
                {(roleText) => (
                  <button className="dropdown-item" onClick={() => this.handleChange(role)}>
                    {roleText}
                  </button>
                )}
              </FormattedMessage>
            );
          })}
        </div>
      </div>
    );
  }
}
