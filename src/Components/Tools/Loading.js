import React, { Component } from "react";
import { FormattedMessage } from "react-intl";

export default class Loading extends Component {
  static get defaultProps() {
    return {
      active: true,
      height: "88vh",
    };
  }

  render() {
    if (this.props.active) {
      return (
        <div style={{ height: this.props.height, display: "flex" }}>
          <div className="d-flex justify-content-center" style={{ margin: "auto" }}>
            <div className="spinner-border text-secondary text-center" role="status">
              <span className="sr-only">
                <FormattedMessage id="general.loading" />
              </span>
            </div>
          </div>
        </div>
      );
    } else {
      return this.props.children;
    }
  }
}
