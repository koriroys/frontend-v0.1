import React, { Component } from "react";
import { injectIntl, FormattedMessage } from "react-intl";
import Alert from "Components/Tools/Alert";
// import Api from 'Api';

class ModalContactForm extends Component {
  constructor(props) {
    super(props);
    // this.deleteProj = this.deleteProj.bind(this)
    this.state = {
      // actualList: this.props.actualList,
    };
  }

  static get defaultProps() {
    return {
      errors: "",
    };
  }

  render() {
    var { intl, errors } = this.props;
    var errorMessage = errors.includes("err-") ? (
      <FormattedMessage id={errors} defaultMessage="An error has occured" />
    ) : (
      errors
    );
    return (
      <div
        className="modal fade"
        id="deleteObjectModal"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="deleteObjectModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="deleteObjectModalLabel">
                <FormattedMessage id="project.delete.title" defaultMessage="Delete my JoGL account" />
              </h5>
              <button type="button" id="closeDeleteModal" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              {errors !== "" && <Alert type="danger" message={errorMessage} />}
              <p className="deleteModalMessage">
                <FormattedMessage id="project.delete.text" />
              </p>
              <div className="btnGroup">
                <button type="button" className="btn btn-outline-danger" onClick={this.deleteProj}>
                  <FormattedMessage id="general.yes" defaultMessage="Yes" />
                </button>
                <button type="button" className="btn btn-success" data-dismiss="modal">
                  <FormattedMessage id="general.no" defaultMessage="No" />
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default injectIntl(ModalContactForm);
