import React, { Component } from "react";
import DocumentCard from "./DocumentCard.js";
import DocumentCardFeed from "./DocumentCardFeed.js";

export default class DocumentsCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      documents: this.props.documents,
      cardtype: this.props.cardtype,
    };
  }

  static get defaultProps() {
    return {
      documents: [],
      cardtype: "feed",
      item: "",
      itemType: "",
      postObj: "",
      user: "",
      isEditing: "",
    };
  }

  componentWillReceiveProps(nextProps) {
    const { documents, type } = nextProps;
    this.setState({ documents, type });
  }

  makeCards() {
    const { refresh, item, postObj, user, isEditing, itemType } = this.props;
    const cards = this.state.documents.map((document, index) => {
      if (document !== undefined && document !== null) {
        if (this.state.cardtype === "feed") {
          return (
            <DocumentCardFeed
              document={document}
              key={index}
              refresh={refresh}
              item={item}
              postObj={postObj}
              user={user}
              isEditing={isEditing}
            />
          );
        } else {
          return <DocumentCard document={document} key={index} refresh={refresh} item={item} itemType={itemType} />;
        }
      }
      return "";
    });
    return cards;
  }

  render() {
    return (
      <div className="feedDocumentList containe-fluid">
        <div className="row">{this.makeCards()}</div>
      </div>
    );
  }
}
