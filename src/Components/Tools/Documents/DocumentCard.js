import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import Api from "Api";
import mimetype2fa from "./mimetypes.js";
import "./DocumentCard.scss";

export default class DocumentsCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      document: this.props.document,
      item: "",
    };
  }

  static get defaultProps() {
    return {
      document: {
        content_type: "image/jpg",
        filename: "MyImage.jpg",
        url: "https://something.com/image.jpg",
      },
      item: "",
    };
  }

  deleteDoc() {
    const { item, document, itemType } = this.props;
    Api.delete("/api/" + itemType + "/" + item.id + "/documents/" + document.id)
      .then((res) => {
        if (res.status === 200) {
          this.props.refresh();
        } else {
          console.log("An error has occured");
        }
      })
      .catch((error) => {
        // console.log(error);
      });
  }

  componentWillReceiveProps(nextProps) {
    const { document } = nextProps;
    this.setState({ document });
  }

  render() {
    return (
      <div className="card documentCard">
        <div className="card-body">
          <h6 className="card-title">{this.state.document.filename}</h6>
          <p>
            <i className={"fa fa-" + mimetype2fa(this.state.document.content_type)}></i>
          </p>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href={this.state.document.url}
            className="btn btn-secondary"
            download={this.state.document.url}
          >
            <FormattedMessage id="document.download" defaultMessage="Download" />
          </a>
          {this.props.item.is_admin && <i className="fa fa-times" onClick={this.deleteDoc.bind(this) // restrict deletion of documents only to admin
              }></i>}
        </div>
      </div>
    );
  }
}
