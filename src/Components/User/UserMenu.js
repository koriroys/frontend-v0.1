import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { Link } from "react-router-dom";
import { UserContext } from "UserProvider";
import DefaultImg from "assets/img/default/default-user.png";
import "./UserMenu.scss";

export default class UserMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
    };
  }

  render() {
    const nothingLink = "#";
    return (
      <UserContext.Consumer>
        {(userContext) => {
          if (userContext.connected) {
            var logo_url = userContext.user
              ? userContext.user.logo_url_sm
                ? userContext.user.logo_url_sm
                : userContext.user.logo_url
                ? userContext.user.logo_url
                : DefaultImg
              : DefaultImg;
            const logoStyle = {
              backgroundImage: "url(" + logo_url + ")",
            };
            return (
              <li className="nav-item userMenu">
                <div className="dropdown ">
                  <a
                    className="dropdown-toggle"
                    href={nothingLink}
                    role="button"
                    id="dropdownMenuLink"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <div className="btnUserMenu" style={logoStyle}></div>
                  </a>

                  <div className="dropdown-menu dropdown-menu-right text-right" aria-labelledby="dropdownMenuLink">
                    <Link to={"/user/" + localStorage.getItem("userId")} className="dropdown-item">
                      <i className="fa fa-user-circle" />
                      <FormattedMessage id="menu.profile.profile" defaultMessage="Profile" />
                    </Link>
                    <div className="dropdown-divider"></div>
                    <Link to={"/user/" + localStorage.getItem("userId") + "/settings"} className="dropdown-item">
                      <i className="fa fa-cog" />
                      <FormattedMessage id="menu.profile.settings" defaultMessage="Settings" />
                    </Link>
                    <div className="dropdown-divider"></div>
                    <a href="mailto:support@jogl.io" className="dropdown-item">
                      <i className="fa fa-question-circle" />
                      <FormattedMessage id="menu.profile.help" defaultMessage="Help" />
                    </a>
                    <div className="dropdown-divider"></div>
                    <Link to="/" className="dropdown-item" onClick={() => userContext.logout()}>
                      <i className="fa fa-sign-out-alt" aria-hidden="true" />
                      <FormattedMessage id="menu.profile.logout" defaultMessage="Logout" />
                    </Link>
                  </div>
                </div>
              </li>
            );
          } else {
            return (
              <li className="nav-item userMenu d-flex align-items-center">
                <div className="signup">
                  <Link to="/signup">
                    <FormattedMessage id="header.signUp" defaultMessage="Sign up" />
                  </Link>
                  <FormattedMessage id="general.or" defaultMessage=" or " />
                </div>
                <button type="button" className="btn btn-primary">
                  {/* Pass current URL to sign in, so that it redirects there after sign in */}
                  <Link to={{ pathname: "/signin", currentUrl: window.location.pathname }} className="nav-link">
                    <FormattedMessage id="general.signIn" defaultMessage="Sign in" />
                  </Link>
                </button>
              </li>
            );
          }
        }}
      </UserContext.Consumer>
    );
  }
}
