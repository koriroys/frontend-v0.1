import React, { Component, Fragment } from "react";
import { FormattedMessage } from "react-intl";
import Api from "Api";
import Loading from "Components/Tools/Loading";
import ChallengeList from "Components/Challenges/ChallengeList";
import CommunityList from "Components/Communities/CommunityList";
import PeopleList from "Components/People/PeopleList";
import ProjectList from "Components/Projects/ProjectList";
import "Components/User/UserProfile.scss";
import "Components/User/Tabs/UserFollowings.scss";

export default class UserFollowings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listChallenges: [],
      listCommunities: [],
      listProjects: [],
      listUsers: [],
      loading: true,
      noResult: false,
    };
  }

  static get defaultProps() {
    return {
      userId: "",
    };
  }

  componentDidMount() {
    this.setState({ loading: true, noResult: false });
    Api.get("/api/users/" + this.props.userId + "/following")
      .then((res) => {
        var listChallenges = [];
        var listCommunities = [];
        var listProjects = [];
        var listUsers = [];
        res.data.challenges.map((challenge) => {
          // console.log("Challenge : ", challenge);
          return listChallenges.push(challenge);
        });
        res.data.communities.map((community) => {
          // console.log("Community : ", community);
          return listCommunities.push(community);
        });
        res.data.projects.map((project) => {
          // console.log("Project : ", project);
          return listProjects.push(project);
        });
        res.data.users.map((user) => {
          // console.log("User : ", user);
          return listUsers.push(user);
        });
        this.setState({
          listChallenges: listChallenges,
          listCommunities: listCommunities,
          listProjects: listProjects,
          listUsers: listUsers,
          loading: false,
        });
        if (
          listChallenges.length === 0 &&
          listCommunities.length === 0 &&
          listProjects.length === 0 &&
          listUsers.length === 0
        ) {
          this.setState({ noResult: true });
        }
      })
      .catch((error) => {
        // console.log(error);
        this.setState({ loading: false, noResult: true });
      });
  }

  async addActiveClass(type) {
    while (!document.querySelector("#followingsModal .tab-content")) {
      // while element does not exist
      await new Promise((r) => setTimeout(r, 500)); // relaunch promise test
    }
    // do following when promise is fullfiled (element exists)
    document.querySelector(`.nav-item[href="#${type}"]`).classList.add("active");
    document.querySelector(`.tab-pane#${type}`).classList.add("active");
  }

  render() {
    const { listChallenges, listCommunities, listProjects, listUsers, loading, noResult } = this.state;
    listUsers.length > 0
      ? this.addActiveClass("users") // if user follows users, add active class to this tab
      : listProjects.length > 0
      ? this.addActiveClass("projects") // if don't follow objects from previous tab, but from projects, add active class to this tab
      : listCommunities.length > 0
      ? this.addActiveClass("communities") // same as up for communities
      : listChallenges.length > 0 && this.addActiveClass("challenges"); // else add active class to challenges tab
    return (
      <Loading active={loading} height="300px">
        {!noResult && (
          <nav className="nav modal-nav-tabs container-fluid">
            {listUsers.length > 0 && (
              <a className="nav-item nav-link" href="#users" data-toggle="tab">
                <FormattedMessage id="user.profile.tab.following.users" defaultMessage="Users" />
              </a>
            )}
            {listProjects.length > 0 && (
              <a className="nav-item nav-link" href="#projects" data-toggle="tab">
                <FormattedMessage id="general.projects" defaultMessage="Projects" />
              </a>
            )}
            {listCommunities.length > 0 && (
              <a className="nav-item nav-link" href="#communities" data-toggle="tab">
                <FormattedMessage id="communities" defaultMessage="Communities" />
              </a>
            )}
            {listChallenges.length > 0 && (
              <a className="nav-item nav-link" href="#challenges" data-toggle="tab">
                <FormattedMessage id="challenges" defaultMessage="Challenges" />
              </a>
            )}
          </nav>
        )}

        <div className="tabContainer">
          <div className="tab-content container-fluid">
            {listUsers.length > 0 && (
              <div className="tab-pane" id="users">
                <PeopleList searchBar={false} listPeople={listUsers} />
              </div>
            )}
            {listProjects.length > 0 && (
              <div className="tab-pane" id="projects">
                <ProjectList searchBar={false} listProjects={listProjects} />
              </div>
            )}
            {listCommunities.length > 0 && (
              <div className="tab-pane" id="communities">
                <CommunityList searchBar={false} listCommunities={listCommunities} />
              </div>
            )}
            {listChallenges.length > 0 && (
              <div className="tab-pane" id="challenges">
                <ChallengeList searchBar={false} listChallenges={listChallenges} />
              </div>
            )}
            {noResult && (
              <div className="col-12 noResult">
                <p>
                  <FormattedMessage id="list.noResult.followings" defaultMessage="No followings" />
                </p>
              </div>
            )}
          </div>
        </div>
      </Loading>
    );
  }
}
