import React, { Component, Fragment } from "react";
import { FormattedMessage, injectIntl } from "react-intl";
import { Redirect } from "react-router-dom";
import Api from "Api";
import Alert from "Components/Tools/Alert.js";
import Envelope from "assets/img/envelope.svg";

class ForgotPwd extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      error: "",
      fireRedirect: false,
      sending: false,
      success: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value, error: "" });
  }

  handleSubmit(event) {
    event.preventDefault();
    const { email } = this.state;

    if (email === "") {
      this.setState({
        error: <FormattedMessage id="err-4003" defaultMessage="This mail address is not valid" />,
      });
    } else {
      const redirect_url = process.env.REACT_APP_ADDRESS_FRONT + "/auth/new-password";
      this.setState({ error: "", sending: true });

      Api.post("/api/users/password", { email, redirect_url })
        .then(() => {
          this.setState({ sending: false, success: true });
          setTimeout(() => {
            this.setState({ fireRedirect: true });
          }, 10000);
        })
        .catch((error) => {
          // console.log(error);
          // console.log(error.response.data);
          const errors = error.response.data.errors[0];
          this.setState({ sending: false, error: errors });
        });
    }
  }

  render() {
    const { error, fireRedirect, sending, success } = this.state;
    const { intl } = this.props;

    if (fireRedirect) {
      return <Redirect to="/signin" />;
    }

    var formStyle, messageStyle, title, msg;
    if (!success) {
      formStyle = { display: "block" };
      messageStyle = { display: "none" };
      title = {
        text: "Reset Password",
        id: "forgotPwd.title",
      };
      msg = {
        text: "Password reset instruction will be sent to your email address.",
        id: "forgotPwd.description",
      };
    } else {
      formStyle = { display: "none" };
      messageStyle = { display: "block" };
      title = {
        text: "An email has been sent to you.",
        id: "forgotPwd.confTitle",
      };
      msg = {
        text: "Password reset instruction will be sent to your email address.",
        id: "forgotPwd.confMsg",
      };
    }

    return (
      <div className="form-content">
        <div className="form-header">
          <h2 className="form-title" id="signModalLabel">
            <FormattedMessage id={title.id} defaultMessage={title.text} />
          </h2>
          <p>
            <FormattedMessage id={msg.id} defaultMessage={msg.text} />
          </p>
        </div>
        <div className="form-body" style={formStyle}>
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label className="form-check-label" htmlFor="password">
                <FormattedMessage id="signIn.email" defaultMessage="Email" />
              </label>
              <input
                type="email"
                name="email"
                id="email"
                className="form-control"
                placeholder={intl.formatMessage({ id: "signIn.email.placeholder" })}
                onChange={this.handleChange}
              />
            </div>
            {error !== "" && <Alert type="danger" message={error} />}
            {success && (
              <Alert
                type="success"
                message={
                  <FormattedMessage id="info-4000" defaultMessage="An e-mail has been sent to reset your password" />
                }
              />
            )}

            <button type="submit" className="btn btn-primary btn-block" disabled={sending ? true : false}>
              {sending && (
                <Fragment>
                  <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                  <span className="sr-only">{intl.formatMessage({ id: "general.loading" })}</span>&nbsp;
                </Fragment>
              )}
              <FormattedMessage id="forgotPwd.btnSendMail" defaultMessage="Send me instructions" />
            </button>
          </form>
        </div>
        <div className="form-message" style={messageStyle}>
          <img src={Envelope} alt="Message sent envelope" />
        </div>
      </div>
    );
  }
}
export default injectIntl(ForgotPwd);
