import React, { Component } from "react";
import qs from "qs";

const updateAfter = 700;

const routeStateDefaultValues = {
  query: "",
  page: "1",
  skills: undefined,
  category: "",
  sortBy: "instant_search",
  hitsPerPage: "20",
};

const searchStateToURL = (searchState) => {
  const routeState = {
    query: searchState.query,
    page: String(searchState.page),
    skills: searchState.refinementList && searchState.refinementList.skill,
    category: searchState.hierarchicalMenu && searchState.hierarchicalMenu["interests.1"],
    sortBy: searchState.sortBy,
    hitsPerPage: (searchState.hitsPerPage && String(searchState.hitsPerPage)) || undefined,
  };

  const { protocol, hostname, port = "", pathname, hash } = window.location;
  const portWithPrefix = port === "" ? "" : `:${port}`;
  const urlParts = window.location.href.match(/^(.*?)\/search/);
  const baseUrl = (urlParts && urlParts[0]) || `${protocol}//${hostname}${portWithPrefix}${pathname}search`;
  const queryParameters = {};

  if (routeState.query && routeState.query !== routeStateDefaultValues.query) {
    queryParameters.query = encodeURIComponent(routeState.query);
  }
  if (routeState.page && routeState.page !== routeStateDefaultValues.page) {
    queryParameters.page = routeState.page;
  }
  if (routeState.skills && routeState.skills !== routeStateDefaultValues.skills) {
    queryParameters.skills = routeState.skills.map(encodeURIComponent);
  }
  if (routeState.sortBy && routeState.sortBy !== routeStateDefaultValues.sortBy) {
    queryParameters.sortBy = routeState.sortBy;
  }
  if (routeState.hitsPerPage && routeState.hitsPerPage !== routeStateDefaultValues.hitsPerPage) {
    queryParameters.hitsPerPage = routeState.hitsPerPage;
  }

  const queryString = qs.stringify(queryParameters, {
    addQueryPrefix: true,
    arrayFormat: "repeat",
  });

  return `${baseUrl}/${queryString}${hash}`;
};

const urlToSearchState = (location) => {
  const queryParameters = qs.parse(location.search.slice(1));
  const { query = "", page = 1, skills = [], hitsPerPage, sortBy } = queryParameters;
  // `qs` does not return an array when there's a single value.
  const allSkills = Array.isArray(skills) ? skills : [skills].filter(Boolean);

  const searchState = { range: {} };

  if (query) {
    searchState.query = decodeURIComponent(query);
  }
  if (page) {
    searchState.page = page;
  }
  if (allSkills.length) {
    searchState.refinementList = {
      skill: allSkills.map(decodeURIComponent),
    };
  }
  if (sortBy) {
    searchState.sortBy = sortBy;
  }
  if (hitsPerPage) {
    searchState.hitsPerPage = hitsPerPage;
  }

  return searchState;
};

const withURLSync = (App) =>
  class WithURLSync extends Component {
    state = {
      searchState: urlToSearchState(window.location),
    };

    componentDidMount() {
      window.addEventListener("popstate", this.onPopState);
    }

    componentWillUnmount() {
      clearTimeout(this.debouncedSetState);
      window.removeEventListener("popstate", this.onPopState);
    }

    onPopState = ({ state }) =>
      this.setState({
        searchState: state || {},
      });

    onSearchStateChange = (searchState) => {
      clearTimeout(this.debouncedSetState);

      this.debouncedSetState = setTimeout(() => {
        window.history.pushState(searchState, null, searchStateToURL(searchState));
      }, updateAfter);

      this.setState({ searchState });
    };

    render() {
      const { searchState } = this.state;

      return (
        <App
          {...this.props}
          searchState={searchState}
          onSearchStateChange={this.onSearchStateChange}
          createURL={searchStateToURL}
        />
      );
    }
  };

export default withURLSync;
