import React, { Component } from "react";
import { Link } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import footerLogo from "assets/img/logo_JOGL-02.png";
import $ from "jquery";
import "./Footer.scss";

class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      emailValue: "",
      fNameValue: "",
      lNameValue: "",
    };
  }

  changeLanguage(newLanguage) {
    localStorage.setItem("language", newLanguage);
    // console.log("New change : ", localStorage.getItem("language"));
    window.location.reload();
  }

  // showMailingPopUp() {
  // 	$(".popup").fadeToggle("fast"); // open popup
  // }

  hideModal() {
    $("#signInModal").modal("hide"); // hide modal when clicking on signin btn
  }

  render() {
    return (
      <footer>
        <div className="footer-top">
          <div className="container-fluid">
            <div className="row">
              {/* <div className="col-12 col-sm-6 col-md-4 col-lg-3 fadeInUp">
		            <img src={footerLogo}/>
	        			<p>
									<FormattedMessage id="footer.JOGLdescription" defaultMessage="JOGL is a non profit registered under the French law Loi 1901." />
	        			</p>
              </div> */}
              <div className="col-12 col-sm-4 col-md-4 col-lg-2 footCol">
                <img src={footerLogo} alt="jogl logo rocket" />
                {/* <p>
									<FormattedMessage id="footer.JOGLdescription" defaultMessage="JOGL is a non profit registered under the French law Loi 1901." />
	        			</p> */}
              </div>
              <div className="col-12 col-sm-8 col-lg-4 footCol">
                <h3>
                  <FormattedMessage id="footer.aboutJOGL" defaultMessage="About JOGL" />
                </h3>
                {/* <img src={footerLogo}/> */}
                <p>
                  <FormattedMessage
                    id="footer.JOGLdescription"
                    defaultMessage="JOGL is a non profit registered under the French law Loi 1901."
                  />
                </p>
              </div>
              <div className="col-12 col-sm-4 col-md-4 col-lg-3 footer-contact footCol">
                <h3>
                  <FormattedMessage id="footer.general" defaultMessage="General" />
                </h3>
                <p>
                  <a href="https://jogl.io" target="_blank" rel="noopener noreferrer">
                    <FormattedMessage id="footer.aboutJOGL" defaultMessage="About JOGL" />
                  </a>
                </p>
                <p>
                  <Link to="/ethics-pledge">
                    <FormattedMessage id="footer.ethicsPledge" defaultMessage="Awareness and Ethics Pledge" />
                  </Link>
                </p>
                <p>
                  <Link to="/data">
                    <FormattedMessage id="footer.data" defaultMessage="Politique sur les données d'utilisateur" />
                  </Link>
                </p>
                <p>
                  <a href="https://jogl.io/#team" target="_blank" rel="noopener noreferrer">
                    <FormattedMessage id="footer.theTeam" defaultMessage="The team" />
                  </a>
                </p>
                <p>
                  <a href="https://jogl.io/#jobs" target="_blank" rel="noopener noreferrer">
                    <FormattedMessage id="footer.jobs" defaultMessage="Jobs opportunities" />
                  </a>
                </p>
                <p>
                  <Link to="/terms">
                    <FormattedMessage id="footer.termsConditions" defaultMessage="Terms and Conditions" />
                  </Link>
                </p>
                <p>
                  <a href="https://gitlab.com/JOGL/JOGL" target="_blank" rel="noopener noreferrer">
                    <FormattedMessage id="footer.contribute" defaultMessage="Contribute to the code" />
                  </a>
                </p>
              </div>
              {/* <div className="col-12 col-sm-6 col-md-4 col-lg-3 footer-contact">
	        			<h3>Newsletter</h3>
								<div className="d-flex flex-row">
									<p>
										subscribe to our beautiful newsletter and stay informed of our active development !
									</p>
								</div>
								<p onClick={() => this.showMailingPopUp()}>Join our mailing list!</p>
								<div className="d-flex flex-row form-group">
									<input type="text" className="form-control" id="email"/>
									<button type="button" className="btn btn-primary">Subscribe</button>
								</div>
              </div> */}
              <div className="col-12 col-sm-8 col-lg-3 footer-contact footCol">
                <h3>Contact</h3>
                <div className="d-flex flex-row">
                  <p>
                    Centre de Recherches Interdisciplinaires (CRI)
                    <br />8 bis rue Charles V, 75004, Paris
                  </p>
                </div>
                <div className="contact">
                  <a href="mailto:hello@jogl.io">
                    <button type="button" className="btn btn-primary">
                      <FormattedMessage id="footer.contactUs" defaultMessage="Contact us" />
                    </button>
                  </a>
                  <p className="press">
                    <FormattedMessage id="footer.pressContact" defaultMessage="Press contact:" />
                    <a href="mailto:press@jogl.io">press[at]jogl.io</a>
                  </p>
                </div>
                <div className="social-networks">
                  <a
                    href="https://twitter.com/justonegiantlab"
                    target="_blank"
                    rel="noopener noreferrer"
                    className="twitter"
                  >
                    <i className="fab fa-twitter"></i>
                  </a>
                  <a
                    href="https://www.facebook.com/justonegiantlab/"
                    target="_blank"
                    rel="noopener noreferrer"
                    className="facebook"
                  >
                    <i className="fab fa-facebook-f"></i>
                  </a>
                  <a
                    href="https://www.linkedin.com/company/jogl/about/"
                    target="_blank"
                    rel="noopener noreferrer"
                    className="linkedin"
                  >
                    <i className="fab fa-linkedin"></i>
                  </a>
                  <a
                    href="https://www.instagram.com/justonegiantlab/"
                    target="_blank"
                    rel="noopener noreferrer"
                    className="instagram"
                  >
                    <i className="fab fa-instagram"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="footer-bottom">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12 col-sm-7 col-md-9 footer-bottom--copyright">
                <p>Just One Giant Lab - {new Date().getFullYear()}</p>
                <p>
                  <FormattedMessage
                    id="footer.CC"
                    defaultMessage="Content on this site is licensed under the Creative Commons Attribution 4.0 International licence (CC-BY 4.0)"
                  />
                </p>
              </div>
              <div className="col-12 col-sm-5 col-md-3 footer-bottom--right">
                <button type="button" className="btn btn-warning repport">
                  <a href="mailto:support@jogl.io" target="_blank" rel="noopener noreferrer">
                    <FormattedMessage id="footer.help" defaultMessage="Need help?" />
                  </a>
                </button>
                <a href="https://www.algolia.com/" target="_blank" rel="noopener noreferrer">
                  <img src={require("assets/img/search-by-algolia.svg")} alt="Search by algolia" />
                </a>
              </div>
            </div>
          </div>
        </div>
        {/* <div className="popup">
			    <div className="popup--content">
						<form action="https://jogl.us17.list-manage.com/subscribe/post?u=ffefc0d7fb31cc92dbb27c0da&amp;id=130b6193e7" method="POST" noValidate>
              <input type="hidden" name="u" value="eb05e4f830c2a04be30171b01"/>
            <input type="hidden" name="id" value="8281a64779"/>
            <label htmlFor='MERGE0'>
                Email
                <input 
                    type="email" 
                    name="EMAIL" 
                    id="MERGE0"
                    value={this.state.emailValue} 
                    onChange={ (e)=>{this.setState({emailValue: e.target.value});} } 
                    autoCapitalize="off" 
                    autoCorrect="off"
                 /> 
            </label>
            <label htmlFor='MERGE1'>
                First name
                <input 
                    type="text" 
                    name="FNAME" 
                    id="MERGE1" 
                    value={this.state.fNameValue} 
                    onChange={(e)=>{this.setState({fNameValue: e.target.value});}}
                />
            </label>
            <label htmlFor='MERGE2'>
                Last name
                <input 
                    type="text" 
                    name="LNAME" 
                    id="MERGE2" 
                    value={this.state.lNameValue} 
                    onChange={(e)=>{this.setState({lNameValue: e.target.value});}}
                />
            </label>
              <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" className="button"/>

            <div style={{position: 'absolute', left: '-5000px'}} aria-hidden='true' aria-label="Please leave the following three fields empty">
                <label htmlFor="b_name">Name: </label>
                <input type="text" name="b_name" tabIndex="-1" value="" placeholder="Freddie" id="b_name"/>

                <label htmlFor="b_email">Email: </label>
                <input type="email" name="b_email" tabIndex="-1" value="" placeholder="youremail@gmail.com" id="b_email"/>

                <label htmlFor="b_comment">Comment: </label>
                <textarea name="b_comment" tabIndex="-1" placeholder="Please comment" id="b_comment"></textarea>
            </div>
          </form>
				</div>
			</div> */}

        {/* <div className="popup">
          <div className="popup--content">
            <div className="form">
              <div id="mc_embed_signup">
                <form action="https://jogl.us17.list-manage.com/subscribe/post?u=ffefc0d7fb31cc92dbb27c0da&amp;id=130b6193e7" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" className="validate" target="_blank" noValidate>
                  <div id="mc_embed_signup_scroll">
                    <div className="mc-field-group">
                      <label htmlFor="mce-EMAIL">Email Address <span className="asterisk">*</span>
                      </label>
                      <input type="email" value="" name="EMAIL" className="required email" id="mce-EMAIL" onChange={this.handleChange.bind(this)}/>
                    </div>
                    <div className="mc-field-group">
                      <label htmlFor="mce-FNAME">First Name  <span className="asterisk">*</span></label>
                      <input type="text" value="" name="FNAME" className="required" id="mce-FNAME" onChange={this.handleChange.bind(this)}/>
                    </div>
                    <div className="mc-field-group">
                      <label htmlFor="mce-LNAME">Last Name  <span className="asterisk">*</span></label>
                      <input type="text" value="" name="LNAME" className="required" id="mce-LNAME" onChange={this.handleChange.bind(this)}/>
                    </div>
                    <div id="mce-responses" className="clear">
                      <div className="response" id="mce-error-response"></div>
                      <div className="response" id="mce-success-response"></div>
                    </div>
                    <div className="formAbsolute" aria-hidden="true"><input type="text" name="b_ffefc0d7fb31cc92dbb27c0da_130b6193e7" tabIndex="-1" value="" onChange={this.handleChange.bind(this)}/></div>
                    <div className="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" className="button" onChange={this.handleChange.bind(this)}/></div>
                  </div>
                </form>
              </div>
              <div id="mce-responses" className="clear">
                <div className="response" id="mce-error-response"></div>
                <div className="response" id="mce-success-response"></div>
              </div>
            </div>
          </div>
          <div className="close-popup"><i className="fa fa-times-circle"></i></div>
        </div> */}

        {/* Sign in Modal toggling on 401 error */}
        <div
          className="modal fade"
          id="signInModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="signModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="signModalLabel">
                  <FormattedMessage id="footer.modalSignIn.text" defaultMessage="You must be logged in" />
                </h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <p>
                  <FormattedMessage id="footer.modalSignIn.text" defaultMessage="Please log in" />
                </p>
                <button type="button" id="signInBtn" className="btn btn-primary" onClick={this.hideModal()}>
                  {/* Pass current URL to sign in, so that it redirects there after sign in */}
                  <Link to={{ pathname: "/signin", currentUrl: window.location.pathname }} className="nav-link">
                    <FormattedMessage id="general.signIn" defaultMessage="Sign in" />
                  </Link>
                </button>
              </div>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}

export default Footer;
