// find all mentions, that are inside double parenthesis (ex '((@:1:John Doe))'), and return their type+id+content in the mentions array
function findMentions(content) {
  if (content) {
    var regexMentions = /\(\(.*?:.*?:.*?\)\)/g;
    const match = content.match(regexMentions);
    if (match) {
      var mentions = match.map((mention) => {
        mention = mention.substring(2, mention.length - 2); // remove double parenthis englobing the mention
        var type = mention.split(":")[0] === "@" ? "user" : "project";
        const id = mention.split(":")[1];
        const content = mention.split(":")[2];
        return { obj_type: type, obj_id: id, obj_match: content };
      });
      return mentions;
    } else {
      return [];
    }
  } else {
    return [];
  }
}

// replace html (<a href="...) mention with the "((mention))" format that postInputMentions/react-mentions uses
function noHTMLMentions(content) {
  if (content) {
    var regexMentions = /<a href=.*?>.*?<\/a>/g; // match all links (html "a" tags)
    const match = content.match(regexMentions);
    if (match) {
      match.forEach((mention) => {
        // loop through all links
        var mentionObj = mention.substring(10, mention.length - 4); // take only the href value. ex: user/1"><span>@</span>John Doe
        var mentionId = mentionObj.split('"')[0].split("/")[1]; // get id by getting valur after the first "/"
        if (mentionObj.split(">")[1] === "<span") {
          // // posts/comments with new mention system
          var mentionType = mentionObj.split(">")[2].substring(0, 1);
          var mentionValue = mentionObj.split(">")[3].substring(0, mention.length);
        } else {
          // posts/comments with old mention system
          var mentionType = mentionObj.split(">")[1].substring(0, 1);
          var mentionValue = mentionObj.split(">")[1].substring(1, mention.length);
        }
        var formatReactMention = "((" + mentionType + ":" + mentionId + ":" + mentionValue + "))";
        content = content.replace(mention, formatReactMention);
      });
    }
  }
  return content;
}

// replace the "((mention))" format that postInputMentions/react-mentions uses, with the mention in html format (<a href="...)
function transformMentions(content) {
  if (content) {
    var regexMentions = /\(\(.*?:.*?:.*?\)\)/g;
    const match = content.match(regexMentions);
    if (match) {
      match.forEach((mention) => {
        var mention_obj = mention.substring(2, mention.length - 2); // remove double parenthis englobing the mention
        var mentionChar = mention_obj.split(":")[0];
        var mentionId = mention_obj.split(":")[1];
        var mentionName = mention_obj.split(":")[2];
        var mentionType = mentionChar === "@" ? "user" : "project";
        content = content.replace(
          mention,
          // "<a href=\"/" + mentionType + "/" + mentionId + "\">" + mentionChar+mentionName + "</a>"
          // "<a href=\"/" + mentionType + "/" + mentionId + "\">" + mentionName + "</a>"
          '<a href="/' + mentionType + "/" + mentionId + '">' + `<span>${mentionChar}</span>` + mentionName + "</a>"
        );
      });
    }
  }
  return content.replace(/\([()]*(\([^()]*\)[^()]*)*\)/g, ""); // remove content inside double parenthesis. ex: ((content))
}

module.exports = { findMentions, noHTMLMentions, transformMentions };
