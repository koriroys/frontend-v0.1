import React, { Component } from "react";
import Api from "Api";
import FormComment from "./FormComment";
import { findMentions, noHTMLMentions, transformMentions } from "Components/Feed/Mentions.js";

export default class CommentUpdate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: noHTMLMentions(this.props.content),
      mentions: [],
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  static get defaultProps() {
    return {
      postId: undefined,
      content: "",
      commentId: 1,
      user: "",
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ postId: nextProps.postId });
  }

  handleChange(content) {
    this.setState({ content });
  }

  handleSubmit(event) {
    var mentions = findMentions(this.state.content);
    var contentNoMentions = transformMentions(this.state.content);
    const postId = this.props.postId;
    const commentId = this.props.commentId;
    var updateJson = {
      comment: {
        content: contentNoMentions,
      },
    };
    if (mentions) {
      updateJson["comment"]["mentions"] = mentions;
    }
    // console.log("updateJson", updateJson);
    Api.patch(`/api/posts/${postId}/comment/${commentId}`, updateJson)
      .then((res) => {
        // console.log(res);
        this.props.isEdit();
        this.props.refresh();
      })
      .catch((error) => {
        // console.log(error);
      });
  }

  render() {
    return (
      <FormComment
        action="update"
        content={this.state.content}
        handleChange={this.handleChange}
        handleSubmit={this.handleSubmit}
        user={this.props.user}
      />
    );
  }
}
