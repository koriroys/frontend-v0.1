import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import Api from "Api";
import FormPost from "./FormPost";
import { findMentions, transformMentions } from "Components/Feed/Mentions.js";
import "./PostCreate.scss";

export default class PostCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: this.props.content,
      documents: [],
      mentions: [],
      uploading: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeDoc = this.handleChangeDoc.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  static get defaultProps() {
    return {
      feedId: undefined,
      content: "",
      user: "",
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ feedId: nextProps.feedId });
  }

  handleChange(content) {
    this.setState({ content });
  }

  handleChangeDoc(documents) {
    this.setState({ documents });
  }

  handleSubmit() {
    var mentions = findMentions(this.state.content);
    var documents = this.state.documents;
    var contentNoMentions = transformMentions(this.state.content);
    const user_id = this.props.user.id;
    var postJson = {
      post: {
        user_id: user_id,
        content: contentNoMentions,
        feed_id: this.props.feedId,
      },
    };
    if (mentions) {
      postJson["post"]["mentions"] = mentions;
    }
    if (documents) {
      postJson["post"]["documents"] = documents;
    }
    // console.log("postJson", postJson);
    if (this.props.feedId !== undefined) {
      this.setState({ uploading: true });
      Api.post("/api/posts", postJson)
        .then((res) => {
          // console.log(res);
          if (this.state.documents.length > 0) {
            // console.log(res.data)
            const itemId = res.data.id;
            // console.log("POSTID", itemId)
            const itemType = "posts";
            const type = "documents";
            if (itemId) {
              var bodyFormData = new FormData();
              Array.from(this.state.documents).forEach((file) => {
                // console.log("file : ", file);
                bodyFormData.append(type + "[]", file);
              });

              var config = {
                headers: { "Content-Type": "multipart/form-data" },
              };

              Api.post("/api/" + itemType + "/" + itemId + "/documents", bodyFormData, config)
                .then((res) => {
                  // console.log(res);
                  // console.log(res.data);
                  if (res.status === 200) {
                    // console.log("Documents uploaded !");
                    this.refresh();
                  } else {
                    // console.log("An error has occured");
                    this.setState({
                      uploading: false,
                      error: <FormattedMessage id="err-" defaultMessage="An error has occured" />,
                    });
                  }
                })
                .catch((error) => {
                  // console.log(error);
                  // console.log(error.response.data);
                  this.setState({
                    uploading: false,
                    error: error.response.data.status + " : " + error.response.data.error,
                  });
                });
            } else {
              // console.log("Unable to upload files (No PostId defined)");
              this.refresh();
            }
          } else {
            this.refresh();
          }
        })
        .catch((error) => {
          // console.log(error);
        });
    } else {
      // console.log("json was send");
    }
  }

  refresh() {
    this.props.refresh();
    this.setState({
      content: "",
      uploading: false,
      documents: [],
    });
  }

  render() {
    return (
      <FormPost
        action="create"
        content={this.state.content}
        documents={this.state.documents}
        handleChange={this.handleChange}
        handleChangeDoc={this.handleChangeDoc}
        handleSubmit={this.handleSubmit}
        uploading={this.state.uploading}
        user={this.props.user}
      />
    );
  }
}
