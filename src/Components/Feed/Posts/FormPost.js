import React, { Component, Fragment } from "react";
import { FormattedMessage } from "react-intl";
import BtnUploadFile from "Components/Tools/BtnUploadFile";
import PostInputMentions from "Components/Tools/Forms/PostInputMentions";
import defaultImg from "assets/img/default/default-user.png";
import "./PostCreate.scss";

export default class FormPost extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  static get defaultProps() {
    return {
      content: "",
      action: "create",
      user: "", // @TODO utiliser context
      uploading: false,
      handleChange: () => console.log("Missing function"),
      handleSubmit: () => console.log("Missing function"),
      handleChangeDoc: () => console.log("Missing function"),
      documents: [],
    };
  }

  handleChange(content) {
    this.props.handleChange(content);
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.handleSubmit();
  }

  handleChangeDoc(documents) {
    this.props.handleChangeDoc(documents);
  }

  attachDocuments(documents) {
    // console.log("Documents reçus : ", documents);
    var arrayDocuments = this.props.documents;
    for (var i = 0; i < documents.length; i++) {
      arrayDocuments.push(documents[i]);
    }
    this.props.handleChangeDoc(arrayDocuments);
  }

  deleteDocument(document) {
    var documents = this.props.documents;
    documents.forEach((documentToInspect, index) => {
      if (documentToInspect === document) {
        documents.splice(index, 1);
      }
    });
    this.props.handleChangeDoc(documents);
  }

  renderPreviewDocuments(documents) {
    if (documents.length !== 0) {
      return (
        <div className="preview">
          <ul className="listDocuments">
            {documents.map((document, index) => {
              return (
                <li key={index}>
                  {document.name}
                  <button
                    type="button"
                    className="close"
                    aria-label="Close"
                    onClick={() => this.deleteDocument(document)}
                  >
                    <span aria-hidden="true">&times;</span>
                  </button>
                </li>
              );
            })}
          </ul>
        </div>
      );
    } else {
      return null;
    }
  }

  render() {
    const { action, content, documents, uploading, user } = this.props;
    var submitBtnText = action === "create" ? "Publish" : "Update";
    var postTypeClass = action === "create" ? "postCreate" : "postUpdate";
    const userImg = user.logo_url_sm ? user.logo_url_sm : defaultImg;
    const userImgStyle = { backgroundImage: "url(" + userImg + ")" };
    return (
      <div className={postTypeClass}>
        <form onSubmit={this.handleSubmit}>
          <div className="inputBox">
            {action === "create" && ( // if post action is create, display image on the left
              <div className="userImgContainer">
                <div className="userImg" style={userImgStyle}></div>
              </div>
            )}
            <PostInputMentions
              content={content}
              onChange={this.handleChange}
              placeholder={[["post"], ["What's on your mind?"]]}
            />
          </div>
          <div className="actionBar">
            <BtnUploadFile setListFiles={this.attachDocuments.bind(this)} />
            <button type="submit" className="btn btn-primary" disabled={uploading ? true : false}>
              {uploading && (
                <Fragment>
                  <span
                    className="spinner-border spinner-border-sm text-center"
                    role="status"
                    aria-hidden="true"
                  ></span>
                  &nbsp;
                </Fragment>
              )}
              <FormattedMessage id={"post.create.btn" + submitBtnText} defaultMessage={submitBtnText} />
            </button>
          </div>
        </form>
        {documents && this.renderPreviewDocuments(documents)}
      </div>
    );
  }
}
