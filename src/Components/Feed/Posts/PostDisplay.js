import defaultImgUser from "assets/img/default/default-user.png";
import defaultImgProject from "assets/img/default/default-project.jpg";
// import defaultImgGroup from "assets/img/default/default-group.jpg";
import CommentDisplay from "Components/Feed/Comments/CommentDisplay";
import BtnClap from "Components/Tools/BtnClap";
import Api from "Api";
import DocumentsList from "Components/Tools/Documents/DocumentsList.js";
import React, { Component, Fragment } from "react";
import { FormattedMessage, injectIntl } from "react-intl";
import { Link } from "react-router-dom";
import Alert from "Components/Tools/Alert";
import PostDelete from "./PostDelete";
import PeopleList from "Components/People/PeopleList";
import Loading from "Components/Tools/Loading";
import "./PostDisplay.scss";
import PostUpdate from "./PostUpdate";
import { linkify, returnFirstLink, copyLink, reportContent } from "Components/Tools/utils/utils.js";
import { UserContext } from "UserProvider";
import $ from "jquery";
// import Slider from "react-slick";

class PostDisplay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showComments: true,
      edit: false,
      clappedUsers: [],
      gotClappers: false,
      viewMore: false,
      post: this.props.post,
      loading: true,
    };
  }

  static get defaultProps() {
    return {
      post: undefined,
      user: "",
      isHomeFeed: false,
      feedId: "",
    };
  }
  changeDisplayComments() {
    this.setState({ showComments: !this.state.showComments });
  }

  isEdit() {
    this.setState({ edit: !this.state.edit });
  }

  getuserInfo(userId, index, clappersNb) {
    Api.get("/api/users/" + userId)
      .then((res) => {
        var arr = this.state.clappedUsers;
        arr.push(res.data); // push new data to array
        if (index + 1 == clappersNb) {
          // when getting last user
          this.forceUpdate(); // force render update
          setTimeout(() => {
            $(`.post-${this.state.post.id} .postStats span:nth-child(1)`).click(); // force click on claps
          }, 200);
        }
      })
      .catch((error) => {});
  }

  showClapModal() {
    var post = this.state.post;
    var clappersNb = post.clappers.length; // get number of clappers
    if (post.clappers.length != 0) {
      const results = post.clappers.map(async (clapper, index) => {
        if (!this.state.gotClappers) {
          // do this only once
          this.getuserInfo(clapper.user_id, index, clappersNb);
        }
      });
      Promise.all(results).then(() => {
        $(`#clappersModal${post.id}`).modal("show"); // show modal
        this.setState({ gotClappers: true });
      });
    }
  }

  viewMore() {
    this.setState({ viewMore: !this.state.viewMore });
  }

  renderClappersModal() {
    var post = this.state.post;
    return (
      <div
        className="modal fade clappersModal"
        id={`clappersModal${post.id}`}
        tabIndex="-1"
        role="dialog"
        aria-labelledby="clappersModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="clappersModalLabel">
                <i className="fa fa-sign-language" />
              </h5>
              <button type="button" id="closeModal" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              {/* Show modal content only if there are clappers*/}
              {this.state.clappedUsers.length != 0 && (
                <PeopleList listPeople={this.state.clappedUsers} searchBar={false} itemType="people" colMax={1} />
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }

  componentDidMount() {
    if (!this.props.post) {
      Api.get("/api/posts/" + this.props.match.params.id).then((res) => {
        this.setState({ post: res.data, loading: false });
        document.querySelector(".post").className += " singlePost";
      });
    } else this.setState({ loading: false });
  }

  reloadPostApi() {
    Api.get("/api/posts/" + this.props.match.params.id)
      .then((res) => {
        this.setState({ post: res.data });
      })
      .catch((error) => {
        // console.log(error);
      });
  }

  refresh() {
    // on refresh, do another post api call to refresh items
    this.reloadPostApi();
  }
  // enf of code to make single post component to work

  render() {
    const { loading } = this.state;
    if (loading) {
      return <Loading height="300px" active={loading}></Loading>;
    }
    // var settings = {
    // 	dots: true,
    // 	arrows: true,
    // 	infinite: true,
    // 	fade: true,
    // 	lazyload: true,
    //   speed: 500,
    // 	swipeToSlide: true,
    // 	draggable: true,
    //   slidesToShow: 1,
    // 	slidesToScroll: 1,
    //   afterChange: current => this.setState({ activeSlide: current })
    // };
    // const { data } = this.state

    const { intl, isHomeFeed, feedId } = this.props;
    const post = this.props.post !== undefined ? this.props.post : this.state.post;
    const user = this.props.post !== undefined || !this.context.user ? this.props.user : this.context.user;
    const refresh = this.props.post !== undefined ? this.props.refresh : this.refresh.bind(this);
    if (post !== undefined) {
      const postId = post.id;
      const creatorId = post.creator.id;
      const objImg = post.creator.logo_url ? post.creator.logo_url : defaultImgUser;
      // var objImg;
      var postCreatorName = post.creator.first_name + " " + post.creator.last_name;
      var postFromName =
        post.from.object_type === "need" // if post is a need
          ? intl.formatMessage({ id: "post.need" }) + post.from.object_name
          : post.from.object_name;
      var postCreatorLink = "/user/" + creatorId;
      var postFromLink =
        post.from.object_type === "need" // if post is a need
          ? "/project/" + post.from.object_need_proj_id + "#needs" // link to the /project not /need because it doesn't exist
          : "/" + post.from.object_type + "/" + post.from.object_id; // else it's the default link

      var defaultImg = post.from.object_type === "project" ? defaultImgProject : defaultImgUser;
      // var defaultImg =(post.from.object_type === "project" || post.from.object_type === "need") ? defaultImgProject : post.from.object_type === "community" ? defaultImgGroup : defaultImgUser;

      // if(isHomeFeed) { // if feed is homepage feed
      // 	if(post.from.object_type !== "user") { // check if it comes from any object aside from user
      // 		objImg = post.from.object_image ? post.from.object_image : defaultImg;
      // 	}
      // 	else objImg = post.creator.logo_url ? post.creator.logo_url : defaultImgUser;
      // }
      // else { // else it's from a user's feed
      // 	objImg = post.creator.logo_url ? post.creator.logo_url : defaultImg;
      // }
      var moment = require("moment");
      const lang = localStorage.getItem("language");
      if (lang === "fr") require("moment/locale/fr.js");
      if (lang === "de") require("moment/locale/de.js");
      var commentsCount = post.comments.length;
      // console.log(moment().format('X'));
      // console.log(moment());
      // console.log(moment(post.created_at));
      // console.log(moment(post.created_at).format("X"));
      var postDate =
        moment().format("X") - moment(post.created_at).format("X") > 600000
          ? moment(post.created_at).calendar()
          : moment(post.created_at).startOf("hour").fromNow();

      // const objImgStyle = {backgroundImage: "url(" + objImg + ")"}
      const userImgStyle = { backgroundImage: "url(" + objImg + ")" };

      var maxChar = 280; // maximum character in a post to be displayed by default;
      var isLongText = !this.state.viewMore && post.content.length > maxChar;

      // // instead of characters, we could use line height (if more than 3 lines..)
      // var divHeight = document.querySelector('.text.extra').offsetHeight
      // var lineHeight = 24
      // var lines = divHeight / lineHeight;
      // console.log(lines);
      // var postFirstLinkMeta = returnFirstLink(post.content)
      // const urlMetadata = require('url-metadata')

      // if(postFirstLinkMeta) {
      // 	urlMetadata("https://cors-anywhere.herokuapp.com/"+postFirstLinkMeta).then(
      // 	function (metadata) { // success handler
      // 		// this.setState({
      // 		// 	site_title: metadata.title,
      // 		// 	site_url: metadata.url,
      // 		// 	site_source: metadata.source,
      // 		// 	site_image: metadata.image
      // 		// })
      // 		document.querySelector(`.post-${postId} .postLinkMeta`).innerHTML =
      // 		metadata.image ?
      // 		`<a href=${postFirstLinkMeta} target="_blank">
      // 			<img class="postLinkMeta--image" src="${metadata.image}">
      // 			<div class="postLinkMeta--infos">
      // 				<h4>${metadata.title}</h4>
      // 				<p>${metadata.description}</p>
      // 			</div>
      // 		</a>`
      // 		: metadata.title ? // if no image
      // 		`<a href=${postFirstLinkMeta} target="_blank">
      // 			<div class="postLinkMeta--infos">
      // 				<h4>${metadata.title}</h4>
      // 				<p>${metadata.description}</p>
      // 			</div>
      // 		</a>`
      // 		: "" // if no title
      // 	})
      // }
      var post_images = post.documents.filter(
        (document) => document.content_type === "image/jpeg" || document.content_type === "image/png"
      );
      const contentWithLinks = linkify(post.content);
      return (
        <div className={`post post-${postId}`}>
          <div className="topContent">
            <div className="topBar">
              <div className="left">
                <div className="userImgContainer">
                  <Link to={"/user/" + creatorId}>
                    <div className="userImg" style={userImgStyle}></div>
                  </Link>
                  {/* <Link to={isHomeFeed ? postFromLink : postCreatorLink}>
										<div className="userImg" style={objImgStyle}></div>
									</Link> */}
                </div>
                <div className="topInfo">
                  <Link to={"/user/" + creatorId}>{post.creator.first_name + " " + post.creator.last_name}</Link>
                  {/* <Link to={isHomeFeed ? postFromLink : postCreatorLink}>
										{isHomeFeed ? postFromName : postCreatorName}
									</Link> */}
                  <div className="date">{postDate}</div>
                  <div className="from">
                    <FormattedMessage id="post.from" defaultMessage="From" />
                    <Link to={postFromLink}>{postFromName}</Link>
                    {/* <Link to={isHomeFeed ? postCreatorLink : postFromLink}>
											{isHomeFeed ? postCreatorName : postFromName}
										</Link> */}
                  </div>
                </div>
              </div>
              <div className="post-manage right d-flex flex-row">
                <div className="btn-group dropright">
                  <button
                    type="button"
                    className="btn btn-secondary dropdown-toggle"
                    data-display="static"
                    data-flip="false"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    •••
                  </button>
                  <div className="dropdown-menu dropdown-menu-right">
                    {user.id === creatorId && (
                      <Fragment>
                        {!this.state.edit ? (
                          <div onClick={this.isEdit.bind(this)}>
                            <i className="fa fa-edit postUpdate" />
                            <FormattedMessage id="feed.object.update" defaultMessage="Update" />
                          </div>
                        ) : (
                          <div onClick={this.isEdit.bind(this)}>
                            <FormattedMessage id="entity.form.btnCancel" defaultMessage="Cancel" />
                          </div>
                        )}
                      </Fragment>
                    )}
                    {user.id === creatorId && ( // if it's user's post
                      <PostDelete postId={postId} type="post" origin="self" refresh={refresh} />
                    ) // set origin to self (it's your post)
                    }
                    {user.id !== creatorId &&
                    this.props.isAdmin && ( // if user is admin and it's NOT his post
                        <PostDelete postId={postId} type="post" origin="other" feedId={feedId} refresh={refresh} />
                      ) // set origin to other (not your post)
                    }
                    {user.id !== creatorId && (
                      <div onClick={() => reportContent("post", postId)}>
                        {" "}
                        {/* on click, launch report function (hide if user is creator */}
                        <i className="fa fa-flag postFlag" />
                        <FormattedMessage id="feed.object.report" defaultMessage="Report" />
                      </div>
                    )}
                    <div onClick={() => copyLink(postId, "post")}>
                      {" "}
                      {/* on click, launch copyLink function */}
                      <i className="fa fa-link" />
                      <FormattedMessage id="feed.object.copyLink" defaultMessage="Copy post Link" />
                    </div>
                    {/* <div onClick={() => this.integratePopup(postId)}><i className="fa fa-code"/> <FormattedMessage id="feed.object.integrate" defaultMessage="Integrate" /></div> */}
                  </div>
                </div>
              </div>
            </div>
            {!this.state.edit ? ( // if post is not being edited, show post content
              <div className={`postTextContainer ${isLongText && "hideText"}`}>
                {" "}
                {/* add hideText class to hide text if it's too long */}
                <div className="text extra" dangerouslySetInnerHTML={{ __html: contentWithLinks }} />
                {isLongText && ( // show "view more" link if text is too long
                  <div className="viewMore" onClick={() => this.viewMore()}>
                    ...
                    <FormattedMessage id="general.showmore" defaultMessage="Show More" />
                  </div>
                )}
              </div>
            ) : (
              // else show post edition component
              <PostUpdate
                postId={postId}
                content={post.content}
                isEdit={this.isEdit.bind(this)}
                refresh={refresh}
                user={user}
              />
            )}
            {post_images.length > 0 /* If post has image, display it */ && (
              <img className="postImage" alt="preview" src={post_images[0].url} />
            )}
            {/* {post_images.length == 0 && postFirstLinkMeta && /* If post has a link and no image, display link metaData
							<div className="postLinkMeta">
								<a href={site_url} target="_blank">
									<img className="postLinkMeta--image" src={site_image}/>
									<div className="postLinkMeta--infos">
										<h4>{site_title}</h4>
										<p>{site_source}</p>
									</div>
								</a>
							</div> */}

            {/* <Slider ref={c => (this.slider = c)} {...settings}>
							{post_images.map((image, index) => (
								<img className="postImage" src={image.url} key={index}/>
			      	))}
		     		</Slider> */}

            <DocumentsList
              documents={post.documents}
              cardtype="feed"
              refresh={refresh}
              postObj={post}
              user={user}
              isEditing={this.state.edit}
            />
            <div className="postStats">
              {/* on clap icon click, show modal */}
              <span onClick={() => this.showClapModal()}>
                <i className="fa fa-sign-language" /> {post.claps_count}
              </span>
              <span onClick={this.changeDisplayComments.bind(this)}>
                <i className="fa fa-comment" /> {commentsCount}{" "}
                <FormattedMessage id="post.comments" defaultMessage="Comment" />
                {commentsCount > 1 ? "s" : ""}
              </span>
            </div>
          </div>
          <div className="actionBar">
            <div className="meta">
              <BtnClap itemType="posts" itemId={post.id} type="text" clapState={post.has_clapped} refresh={refresh} />
              <button className="btn-postcard btn" onClick={this.changeDisplayComments.bind(this)}>
                <i className="fa fa-comments" /> <FormattedMessage id="post.comment" defaultMessage="Comment" />
              </button>
            </div>
            {this.state.showComments && (
              <CommentDisplay comments={post.comments} postId={postId} refresh={refresh} user={user} />
            )}
          </div>
          {this.state.gotClappers && this.renderClappersModal()} {/* Post clappers modal (hidden by default) */}
          {/* "Report" and "CopyLink" confirmation alerts (hidden by default) */}
          <Alert
            id="copyConfirmation"
            postId={postId}
            type="success"
            message={<FormattedMessage id="feed.object.copyLink.conf" defaultMessage="The post link has been copied" />}
          />
          <Alert
            id="reportPostConfirmation"
            postId={postId}
            type="success"
            message={<FormattedMessage id="feed.object.reportPost.conf" defaultMessage="The post has been reported" />}
          />
        </div>
      );
    } else return null;
  }
}
export default injectIntl(PostDisplay);
PostDisplay.contextType = UserContext;
