import React, { Component, Fragment } from "react";
import { FormattedMessage } from "react-intl";
import Api from "Api";
import PostDisplay from "Components/Feed/Posts/PostDisplay";
import PostCreate from "./Posts/PostCreate";
import { UserContext } from "UserProvider";
import { infiniteLoader } from "Components/Tools/utils/utils.js";
import "./Feed.scss";

export default class Feed extends Component {
  constructor(props) {
    super(props);

    this.state = {
      posts: [],
      loadingBtn: false,
      loadBtnCount: 1,
      hideLoadBtn: false,
    };
  }

  static get defaultProps() {
    return {
      displayCreate: false,
      feedId: undefined,
      isAdmin: false,
    };
  }

  loadPosts(currentPage) {
    var itemsPerQuery = 5; // number of users per query calls (load more btn click)
    if (currentPage > 1) this.setState({ loadingBtn: true });
    Api.get(`/api/feeds/${this.props.feedId}?items=${itemsPerQuery}&page=${currentPage}`)
      .then((res) => {
        const totalPages = res.headers["total-pages"]; // get total pages from response headers
        var posts = this.state.posts;
        var newPosts = res.data;
        newPosts.map((post) => {
          // map
          return posts.push(post);
        });
        this.setState({ posts, loading: false, loadingBtn: false, loadBtnCount: (currentPage += 1) });
        currentPage <= totalPages && infiniteLoader("feed"); // relaunch function at each call, if we haven't reached last page
        if (currentPage == totalPages) this.state.hideLoadBtn = true; // hide btn when the last "page" has been called
      })
      .catch((error) => {
        // console.log(error);
        this.setState({ loading: false, loadingBtn: false });
      });
  }

  getFeedApi() {
    Api.get("/api/feeds/" + this.props.feedId + "?items=15")
      .then((res) => {
        this.setState({ posts: res.data });
      })
      .catch((error) => {
        // console.log(error);
      });
  }

  componentDidMount() {
    this.loadPosts(this.state.loadBtnCount);
  }

  refresh() {
    // on refresh, reset feed and get only 5 latest posts
    this.getFeedApi();
  }

  displayPosts(posts) {
    if (posts.length !== 0) {
      return posts.map((post, index) => (
        <PostDisplay
          post={post}
          key={index}
          feedId={this.props.feedId}
          refresh={this.refresh.bind(this)}
          user={this.context.user}
          isAdmin={this.props.isAdmin}
        />
      ));
    } else {
      // return(
      //   <FormattedMessage id="feed.empty" defaultMessage="Be the first to post something"/>
      // )
    }
  }

  render() {
    const { posts, loadingBtn, hideLoadBtn } = this.state;
    var btnHiddenClass = hideLoadBtn ? "d-none" : "";
    return (
      <div className="feed">
        {this.props.displayCreate && (
          <PostCreate
            feedId={this.props.feedId}
            type="post"
            refresh={this.refresh.bind(this)}
            user={this.context.user}
          />
        )}
        {this.displayPosts(posts)}
        {posts.length >= 5 && (
          <button
            type="button"
            className={`btn btn-primary loadBtn ${btnHiddenClass}`}
            onClick={() => this.loadPosts(this.state.loadBtnCount)}
          >
            {loadingBtn && (
              <Fragment>
                <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true"></span>
                &nbsp;
              </Fragment>
            )}
            <FormattedMessage id="general.load" defaultMessage="Load more" />
          </button>
        )}
      </div>
    );
  }
}
Feed.contextType = UserContext;
