import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import Api from "Api";
import ProgramForm from "./ProgramForm";
import MembersList from "../Members/MembersList";
import ChallengesAttachedList from "Components/Challenges/ChallengesAttachedList";
import { UserContext } from "UserProvider";
import { stickyTabNav, scrollToActiveTab } from "Components/Tools/utils/utils.js";

export default class ProgramEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      program: this.props.program,
      programUpdated: false,
      failedLoad: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  static get defaultProps() {
    return {
      program: {
        id: 0,
        title: "",
        short_title: "",
        banner_url: "",
        description: "",
        claps_count: 0,
        follower_count: 0,
        members_count: 0,
        launch_date: null,
        end_date: null,
      },
    };
  }

  componentDidMount() {
    Api.get("/api/programs/getid/" + this.props.match.params.short_title)
      .then((res) => {
        Api.get("/api/programs/" + res.data.id)
          .then((res) => {
            this.setState({ program: res.data, gotChallenges: true });
          })
          .catch((error) => {
            // console.log(error);
          });
      })
      .catch((error) => {
        // console.log(error);
        this.setState({ failedLoad: true });
      });
  }

  componentWillReceiveProps(nextProps) {
    const program = nextProps.program;
    this.setState({ program });
  }

  handleChange(key, content) {
    // console.log("Edit handleChange : " + key + " " + content);
    var newProgram = this.state.program;
    newProgram[key] = content;
    // console.log(newProgram);
    this.setState({ program: newProgram });
  }

  handleSubmit() {
    const program = this.state.program;
    Api.patch("/api/programs/" + this.state.program.id, { program })
      .then((res) => {
        // console.log(res);
        this.setState({ programUpdated: true });
      })
      .catch((error) => {
        // console.log(error);
      });
  }

  render() {
    if (!this.state.gotChallenges) {
      return null;
    }
    const { failedLoad, program, programUpdated } = this.state;
    if (programUpdated) {
      return <Redirect to={"/program/" + program.short_title} />;
    }
    let userContext = this.context;
    if (program || (!program && failedLoad)) {
      // check if program exist, or if it doesn't
      if (failedLoad || !userContext.user) {
        // redirect to programs page if not connected, not admin, if program doesn't exist, or was just deleted
        return <Redirect to="/" />;
      }
    }
    setTimeout(function () {
      stickyTabNav("isEdit"); // make the tab navigation bar sticky on top when we reach its scroll position
      scrollToActiveTab(); // if there is a hash in the url and the tab exists, click and scroll to the tab
    }, 700); // had to add setTimeout for the function to work
    return (
      <div className="programEdit container-fluid">
        <h1>
          <FormattedMessage id="program.edit.title" defaultMessage="Edit the program" />
        </h1>
        <a href={"/program/" + program.short_title}>
          {" "}
          {/* go back link*/}
          <i className="fa fa-arrow-left" />
          <FormattedMessage id="program.edit.back" defaultMessage="Go back" />
        </a>
        <nav className="nav nav-tabs container-fluid">
          <a className="nav-item nav-link active" href="#basic_info" data-toggle="tab">
            <FormattedMessage id="entity.tab.basic_info" defaultMessage="Basic information" />
          </a>
          <a className="nav-item nav-link" href="#members" data-toggle="tab">
            <FormattedMessage id="general.members" defaultMessage="Members" />
          </a>
          <a className="nav-item nav-link" href="#challenges" data-toggle="tab">
            <FormattedMessage id="challenges" defaultMessage="Challenges" />
          </a>
        </nav>
        <div className="tabContainer">
          <div className="tab-content justify-content-center container-fluid">
            <div className="tab-pane active" id="basic_info">
              <ProgramForm
                mode="edit"
                program={program}
                handleChange={this.handleChange}
                handleSubmit={this.handleSubmit}
              />
            </div>
            <div className="tab-pane" id="members">
              {program.id ? <MembersList itemType="programs" itemId={program.id} /> : null}
            </div>
            <div className="tab-pane" id="challenges">
              <ChallengesAttachedList itemType="programs" actionBar={true} itemId={program.id} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
ProgramEdit.contextType = UserContext;
