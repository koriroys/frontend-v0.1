import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
// import { Link } from "react-router-dom";

export default class ProgramsHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listPeople: [],
    };
  }

  componentWillMount() {
    /* Api.get("/api/programs/").then(res=>{
      var listUser = [];
      res.data.map((user) => {
        listUser.push(JSON.stringify(user));
      });
      this.setState({listPeople: listUser});
    }).catch(error=>{
      console.log(error);
    }); */
  }

  render() {
    /* const defaultHref = "#"; */
    return (
      <div className="programsHeader">
        {/*<div className="col-12 text-right">
          <button className="btn btn-sm btnSearch">
            <FormattedMessage id="searchBar.mainBtn" defaultMessage="Search & Filter & Sort" />
          </button>
          <Link to="/program/create">
            <button className="btn btn-sm btnAddProgram">
              <FormattedMessage id="searchBar.btnAddProgram" defaultMessage="Add a program" />
            </button>
          </Link>
        </div>*/}
        <h1>
          <FormattedMessage id="programs.header.title" defaultMessage="Explore programs" />
        </h1>
        <p>
          <FormattedMessage
            id="programs.header.description"
            defaultMessage="Discover awesome programs around the world and join teams to make the world a better place."
          />
        </p>
        {/*<div className="dropdown">
          <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Filter</button>
          <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a className="dropdown-item" href={defaultHref}>Action</a>
            <a className="dropdown-item" href={defaultHref}>Another action</a>
            <a className="dropdown-item" href={defaultHref}>Something else here</a>
          </div>
        </div>*/}
        <hr />
      </div>
    );
  }
}
