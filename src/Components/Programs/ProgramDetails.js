import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { Redirect } from "react-router-dom";
import { UserContext } from "UserProvider";
import Api from "Api";
import Feed from "../Feed/Feed";
import InfoHtmlComponent from "Components/Tools/Info/InfoHtmlComponent";
// import ListFollowers from "../Tools/ListFollowers";
import Loading from "Components/Tools/Loading";
import Needs from "Components/Needs/Needs";
import ProgramHeader from "./ProgramHeader";
import ListChallengesAttached from "../Tools/ListChallengesAttached";
import ListProjectsAttached from "../Tools/ListProjectsAttached";
import { stickyTabNav, scrollToActiveTab } from "Components/Tools/utils/utils.js";
import "Components/Main/Similar.scss";

export default class ProgramDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      program: this.props.program,
      failedLoad: false,
      loading: true,
    };
  }

  getProgram(program_id) {
    Api.get("/api/programs/" + program_id)
      .then((res) => {
        // console.log("Program got : ", res.data);
        if (res.data) {
          this.setState({ program: res.data, loading: false });
        } else {
          this.setState({ failedLoad: true, loading: false });
        }
      })
      .catch((error) => {
        // console.log("ERR : ", error);
        this.setState({ failedLoad: true, loading: false });
      });
  }

  componentWillMount() {
    if (!this.props.program) {
      if (isNaN(this.props.match.params.short_title)) {
        Api.get("/api/programs/getid/" + this.props.match.params.short_title)
          .then((res) => {
            this.getProgram(res.data.id);
          })
          .catch((error) => {
            // console.log("ERR : ", error);
            this.setState({ failedLoad: true, loading: false });
          });
      } else {
        this.getProgram(this.props.match.params.short_title);
      }
    } else {
      // console.log("Program RECU");
      this.setState({ loading: false });
    }
  }

  render() {
    const { program, failedLoad, loading } = this.state;
    var urlParams = new URLSearchParams(window.location.search);
    var userLang = navigator.language || navigator.userLanguage;
    const lang =
      (localStorage.getItem("language") && localStorage.getItem("language") === "fr") ||
      (!localStorage.getItem("language") && (userLang === "fr-FR" || userLang === "fr"))
        ? "fr"
        : "en";
    if (failedLoad) {
      return <Redirect to="/programs" />;
    }

    // make different tab active depending if user is member or not
    var newsTabClasses, aboutTabClasses, newsPaneClasses, aboutPaneClasses;
    if (program) {
      if (program.is_member || urlParams.get("tab") === "feed") {
        newsTabClasses = "nav-item nav-link active";
        aboutTabClasses = "nav-item nav-link";
        newsPaneClasses = "tab-pane active";
        aboutPaneClasses = "tab-pane inside";
      } else {
        newsTabClasses = "nav-item nav-link";
        aboutTabClasses = "nav-item nav-link active";
        newsPaneClasses = "tab-pane";
        aboutPaneClasses = "tab-pane inside active";
      }
      setTimeout(function () {
        stickyTabNav(); // make the tab navigation bar sticky on top when we reach its scroll position
        scrollToActiveTab(); // if there is a hash in the url and the tab exists, click and scroll to the tab
      }, 700); // had to add setTimeout to make it work
      return (
        <Loading active={loading}>
          <div className="programDetails container-fluid">
            <ProgramHeader program={program} lang={lang} />
            <nav className="nav nav-tabs container-fluid">
              <a className={newsTabClasses} href="#news" data-toggle="tab">
                <FormattedMessage id="entity.tab.news" defaultMessage="News & Updates" />
              </a>
              <a className={aboutTabClasses} href="#about" data-toggle="tab">
                <FormattedMessage id="general.about" defaultMessage="About the Program" />
              </a>
              <a className="nav-item nav-link challenges" href="#challenges" data-toggle="tab">
                <FormattedMessage id="challenges" defaultMessage="Challenges" />
              </a>
              <a className="nav-item nav-link projects" href="#projects" data-toggle="tab">
                <FormattedMessage id="general.projects" defaultMessage="Projects" />
              </a>
              <a className="nav-item nav-link needs" href="#needs" data-toggle="tab">
                <FormattedMessage id="entity.card.needs" defaultMessage="Needs" />
              </a>
              <a className="nav-item nav-link" href="#faq" data-toggle="tab">
                <FormattedMessage id="entity.info.faq" defaultMessage="FAQ" />
              </a>
              <a className="nav-item nav-link" href="#ressources" data-toggle="tab">
                <FormattedMessage id="program.tab.ressources" defaultMessage="ressources" />
              </a>
              {/* <a className="nav-item nav-link" href="#enablers" data-toggle="tab"><FormattedMessage id="program.tab.enablers" defaultMessage="facilitators" /></a> */}
              {/* <a className="nav-item nav-link" href="#followers" data-toggle="tab"><FormattedMessage id="entity.tab.followers" defaultMessage="Follower" /></a> */}
            </nav>
            <div className="tabContainer">
              <div className="tab-content justify-content-center container-fluid">
                <div className={newsPaneClasses} id="news">
                  {/* Show feed, and pass DisplayCreate to all connected users */}
                  {program.feed_id && (
                    <Feed
                      feedId={program.feed_id}
                      displayCreate={this.context.connected ? true : false}
                      isAdmin={program.is_admin}
                    />
                  )}
                </div>
                <div className={aboutPaneClasses} id="about">
                  <InfoHtmlComponent
                    title=""
                    content={lang === "fr" && program.description_fr ? program.description_fr : program.description}
                  />
                </div>
                <div className="tab-pane" id="challenges">
                  <div className="alert alert-info" role="alert">
                    <FormattedMessage
                      id="program.challenges.text"
                      defaultMessage="To participate to this challenge, participate to as many of its projects. Check out the projects already submitted, contribute to them or create your own!"
                    />
                  </div>
                  <ListChallengesAttached itemId={program.id} itemType="programs" />
                </div>
                <div className="tab-pane" id="projects">
                  <div className="alert alert-info" role="alert">
                    <FormattedMessage id="program.projects.text" />
                  </div>
                  <ListProjectsAttached itemId={program.id} itemType="programs" />
                </div>
                <div className="tab-pane" id="needs">
                  <Needs displayCreate={false} itemId={program.id} itemType="programs" />
                </div>
                <div className="tab-pane" id="faq">
                  <InfoHtmlComponent
                    title=""
                    content={lang === "fr" && program.faq_fr ? program.faq_fr : program.faq}
                  />
                </div>
                <div className="tab-pane" id="ressources">
                  <InfoHtmlComponent title="" content={program.ressources} />
                </div>
                {/* <div className="tab-pane" id="enablers">
									<InfoHtmlComponent title="" content={lang === "fr" && program.enablers_fr ? program.enablers_fr : program.enablers} />
                </div> */}
                {/* <div className="tab-pane" id="followers">
                  <ListFollowers itemId={program.id} itemType="programs" />
                </div> */}
              </div>
            </div>
          </div>
        </Loading>
      );
    } else {
      return <Loading active={loading} />;
    }
  }
}
ProgramDetails.contextType = UserContext;
