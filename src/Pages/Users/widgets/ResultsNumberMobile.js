import React from "react";
import { connectStats } from "react-instantsearch-dom";
import { formatNumber } from "Components/Tools/utils/utils.js";
import { FormattedMessage } from "react-intl";

const ResultsNumberMobile = ({ nbHits }) => (
  <div>
    <strong>{formatNumber(nbHits)}</strong> <FormattedMessage id="users.members" defaultMessage="members" />
  </div>
);

export default connectStats(ResultsNumberMobile);
