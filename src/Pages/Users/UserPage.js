import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import UserProfile from "Components/User/UserProfile";
import UserProfileEdit from "Components/User/UserProfileEdit";
import UserSettings from "Components/User/Settings/UserSettings";

export default class UserPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultURL: "/user",
    };
  }

  render() {
    return (
      <div>
        <Switch>
          <Route path={this.state.defaultURL + "/:id"} exact={true} component={UserProfile} />
          <Route path={this.state.defaultURL + "/:id/edit"} exact={true} component={UserProfileEdit} />
          <Route path={this.state.defaultURL + "/:id/settings"} exact={true} component={UserSettings} />
        </Switch>
      </div>
    );
  }
}
