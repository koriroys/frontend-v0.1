import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import ProgramCreate from "Components/Programs/ProgramCreate";
import ProgramDetails from "Components/Programs/ProgramDetails";
import ProgramEdit from "Components/Programs/ProgramEdit";

export default class ProgramPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultURL: "/program",
    };
  }

  render() {
    return (
      <div>
        <Switch>
          <Route path={this.state.defaultURL + "/create"} exact={true} component={ProgramCreate} />
          <Route path={this.state.defaultURL + "/:short_title"} exact={true} component={ProgramDetails} />
          <Route path={this.state.defaultURL + "/:short_title/edit"} exact={true} component={ProgramEdit} />
        </Switch>
      </div>
    );
  }
}
