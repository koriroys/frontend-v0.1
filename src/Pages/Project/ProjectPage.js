import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import ProjectCreate from "Components/Projects/ProjectCreate";
import ProjectDetails from "Components/Projects/ProjectDetails";
import ProjectEdit from "Components/Projects/ProjectEdit";

export default class ProjectPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultURL: "/project",
    };
  }

  render() {
    return (
      <div>
        <Switch>
          <Route path={this.state.defaultURL + "/create"} exact={true} component={ProjectCreate} />
          <Route path={this.state.defaultURL + "/:id"} exact={true} component={ProjectDetails} />
          <Route path={this.state.defaultURL + "/:id/edit"} exact={true} component={ProjectEdit} />
        </Switch>
      </div>
    );
  }
}
