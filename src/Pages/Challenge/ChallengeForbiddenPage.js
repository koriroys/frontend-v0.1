import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { FormattedMessage } from "react-intl";
//import "assets/css/ChallengePage.scss";

export default class ChallengeForbiddenPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
    };
  }

  goToChallenges() {
    this.setState({ redirect: true });
  }

  render() {
    if (this.state.redirect) {
      return <Redirect push to={"/challenges"} />;
    }
    return (
      <div className="container-fluid ChallengePage">
        <div className="row">
          <div className="col-12 text-center">
            <br />
            <br />
            <FormattedMessage
              id="challenge.info.forbidden"
              defaultMessage="Please contact the JOGL team to obtain rights to create a challenge"
            />
            <br />
            <a href="mailto:hello@jogl.io">hello@jogl.io</a>
            <br />
            <br />
            <br />
            <button className="btn btn-primary" onClick={() => this.goToChallenges()}>
              <FormattedMessage id="challenge.info.forbiddenBtn" defaultMessage="Return to challenges" />
            </button>
          </div>
        </div>
      </div>
    );
  }
}
