import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { Link } from "react-router-dom";
import "./NotFound.scss";

export default class NotFound extends Component {
  render() {
    return (
      <div className="notFound">
        <p>
          <FormattedMessage id="err-404.title" defaultMessage="Sorry, that page doesn’t exist!" />
        </p>
        <Link to="/">
          <button className="btn btn-primary">
            <FormattedMessage id="err-404.btn" defaultMessage="Back to Home page" redirectTo="/" />
          </button>
        </Link>
      </div>
    );
  }
}
